<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Student; 
use App\Grade;

class SamplesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Sample Kelas
        Grade::create(['name'=>'7A']);
        Grade::create(['name'=>'7B']);
        Grade::create(['name'=>'7C']);
        Grade::create(['name'=>'7D']);
        Grade::create(['name'=>'7E']);
        Grade::create(['name'=>'7F']);
        Grade::create(['name'=>'7G']);
        Grade::create(['name'=>'7H']);
        Grade::create(['name'=>'7I']);
        Grade::create(['name'=>'8A']);
        Grade::create(['name'=>'8B']);
        Grade::create(['name'=>'8C']);
        Grade::create(['name'=>'8D']);
        Grade::create(['name'=>'8E']);
        Grade::create(['name'=>'8F']);
        Grade::create(['name'=>'8G']);
        Grade::create(['name'=>'8H']);
        Grade::create(['name'=>'8I']);
        Grade::create(['name'=>'9A']);
        Grade::create(['name'=>'9B']);
        Grade::create(['name'=>'9C']);
        Grade::create(['name'=>'9D']);
        Grade::create(['name'=>'9E']);
        Grade::create(['name'=>'9F']);
        Grade::create(['name'=>'9G']);
        Grade::create(['name'=>'9H']);
        Grade::create(['name'=>'9I']);

        // $faker  = Faker::create();
        // foreach(range(1,10) as $i)
        // {
        //     Student::create([
        //         'nis'       =>  rand(1000000,9999999),
        //         'name'      =>  $faker->name,
        //         'grade_id'  =>  rand(1,9),
        //         'pin'       =>  $i,
        //         'kelamin'   =>  'Laki-laki',
        //         'alamat'    =>  $faker->address,
        //         'tmplahir'  =>  $faker->city,
        //         'tgllahir'  =>  $faker->date($format = 'Y-m-d'),
        //         'foto'      =>  'laravel.jpg',
        //         'kota'      =>  $faker->city,
        //         'tahun_masuk' =>  date('Y'),
        //         'nohp'      =>  '081324712041',
        //         'nmortu'    =>  $faker->name,
        //         'nohp_ortu' =>  '081324712041',
        //         'nowa'      =>  '081324712041'
        //     ]);
        // }

    }
}