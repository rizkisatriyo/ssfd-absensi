<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(
            [
                SamplesSeeder::class,
                FingerprintTableSeeder::class
            ]
        );

        factory(App\User::class, 3)->create()->each(function($u)
        {
            factory(App\Role::class)->create();
        });

    }
}
