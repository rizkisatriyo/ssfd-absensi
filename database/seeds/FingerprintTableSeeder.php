<?php

use Illuminate\Database\Seeder;
use App\FingerprintModel;

class FingerprintTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i = 1; $i <= 1; $i++)
    	{
    		$data = [
                'name'          =>  'Mesin '.$i,
                'user_id'       =>  '10000',
                'password'      =>  '1',
                'ip_address'    =>  '192.168.137.1'.$i,
                'com_key'       =>  0
            ];
    		FingerprintModel::create($data);
    	}
    }
}
