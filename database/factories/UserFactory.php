<?php

//use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => 'password',
        'username' => $faker->unique()->username,
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Role::class,function(Faker\Generator $faker) {
	$roles = ['superadmin','middleadmin','lowadmin'];
	return [
		'name' => $faker->unique()->randomElement($array = $roles),
		'user_id' => $faker->unique()->randomElement(App\User::pluck('id')->toArray()),
		'description' => $faker->text
	];
});

//FAKER DEFAULT
//$factory->define(App\User::class, function (Faker $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->unique()->safeEmail,
//        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
//        'remember_token' => str_random(10),
//    ];
//});
