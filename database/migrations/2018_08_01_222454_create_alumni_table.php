<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumni', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('nis')->unique();
            $table->string('name');
            $table->integer('grade_id')->unsigned(); 
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade')->onUpdate('cascade');
            $table->enum('kelamin', ['Laki-Laki','Perempuan']);
            $table->string('alamat');
            $table->string('tmplahir')->nullable();
            $table->date('tgllahir')->nullable();
            $table->string('foto')->default('noimage.jpg');
            $table->string('kota');
            $table->string('tahun_masuk');
            $table->string('tahun_keluar');
            $table->string('nohp')->nullable();
            $table->string('nmortu')->nullable();
            $table->string('nohp_ortu')->nullable();
            $table->string('nowa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumni');
    }
}
