<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fingerprint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fingerprint', function (Blueprint $table)
        {

            $table->increments('id');
            $table->string('name');
            $table->string('user_id');
            $table->string('password');
            $table->ipAddress('ip_address')->unique();
            $table->smallInteger('com_key');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('fingerprint');
    }
}
