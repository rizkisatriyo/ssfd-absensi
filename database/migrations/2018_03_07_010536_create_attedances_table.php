<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttedancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attedances', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('id')
                    ->on('students')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->string('kode')->unique();
            $table->time('jam_masuk');
            $table->time('jam_keluar')->nullable();
            $table->boolean('hadir');
            $table->boolean('sakit');
            $table->boolean('terlambat');
            $table->boolean('alfa');
            $table->boolean('izin');
            $table->boolean('pulang');
            $table->boolean('nsm')->comment = "Notifikasi SMS ketika masuk TRUE OR FALSE";
            $table->boolean('nsp')->comment = "Notifikasi SMS ketika pulang / keluar TRUE OR FALSE";
            $table->time('kjm')->comment = "Ketentuan jam masuk";
            $table->time('kjp')->comment = "Ketentuan jam pulang";
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attedances');
    }
}
