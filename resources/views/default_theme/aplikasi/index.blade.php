@extends('default_theme.main')
@section('content')
<div class="row no-m-t no-m-b">
    <div class="col s12 m12 l12">
        <div class="card invoices-card">
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3">
                                <a class="active" href="#mf">
                                    Pengaturan Aplikasi
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a  href="#help">
                                    Bantuan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="mf" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <div class="row">
                            {{ Form::open(['id' => 'form_update_aplikasi']) }}
                                {{ Form::hidden('short_name', 'null', ['class'=>'validate short_name']) }}
                                {{ Form::hidden('created_by', 'null', ['class'=>'validate created_by']) }}
                                {{ Form::hidden('background_color', 'null', ['class'=>'validate background_color']) }}
                                {{ Form::hidden('authors', 'null', ['class'=>'validate authors']) }}
                                {{ Form::hidden('favicon', 'null', ['class'=>'validate favicon']) }}
                                {{ Form::hidden('sms_flog', 'null', ['class'=>'validate sms_flog']) }}
                                {{ Form::hidden('read_finger_in', 'null', ['class'=>'validate read_finger_in']) }}
                                {{ Form::hidden('read_finger_out', 'null', ['class'=>'validate read_finger_out']) }}
                                {{ Form::hidden('counter_notifier', 'null', ['class'=>'validate counter_notifier']) }}

                                <div class="input-field col s4">
                                    {{ Form::text('first_name', 'null', ['class'=>'validate first_name']) }}
                                    {{ Form::label('first_name', 'Nama Awal', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s4">
                                    {{ Form::text('middle_name', 'null', ['class'=>'validate middle_name']) }}
                                    {{ Form::label('middle_name', 'Nama Tengah', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s4">
                                    {{ Form::text('last_name', 'null', ['class'=>'validate last_name']) }}
                                    {{ Form::label('last_name', 'Nama Akhir', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s6">
                                    {{ Form::text('limit', 'null', ['class'=>'validate limit']) }}
                                    {{ Form::label('limit', 'Batas Pengecekan Per Siswa', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s6">
                                    {{ Form::text('sms_notification', 'null', ['class'=>'validate sms_notification']) }}
                                    <label for="sms_notification" class="active">
                                        Notifikasi SMS Ke Orang Tua Siswa <em class="red-text">(Di isi "Y" & "N") </em>
                                    </label>
                                </div>
                                <div class="input-field col s6">
                                    {{ Form::text('in_clock', 'null', ['class'=>'validate in_clock']) }}
                                    {{ Form::label('in_clock', 'Jam Masuk', ['class' => 'active']) }}
                                </div>
                               <div class="input-field col s6">
                                    {{ Form::text('time_off_schedular_in', 'null', ['class'=>'validate tosi']) }}
                                    {{ Form::label('time_off_schedular_in', 'Batas Waktu Absen Masuk', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s6">
                                    {{ Form::text('out_clock', 'null', ['class'=>'validate out_clock']) }}
                                    {{ Form::label('out_clock', 'Jam Keluar', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s6">
                                    {{ Form::text('time_off_schedular_out', 'null', ['class'=>'validate toso']) }}
                                    {{ Form::label('time_off_schedular_out', 'Batas Waktu Absen Keluar', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s12">
                                    {{ Form::text('message_format_in', 'null', ['class'=>'validate message_format_in']) }}
                                    {{ Form::label('message_format_in', 'Pesan SMS siswa saat masuk', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s12">
                                    {{ Form::text('message_format_out', 'null', ['class'=>'validate message_format_out']) }}
                                    {{ Form::label('message_format_out', 'Pesan SMS siswa saat pulang', ['class' => 'active']) }}
                                </div>
                                <div class="input-field col s12">
                                    {{ Form::button('Simpan', ['class'=>'btn btn-primary btn-small waves-effect waves-light m-b-xs','id' => 'button_update_aplikasi']) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                    <div id="help" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <h6>
                            <b>
                                Penjelasan Tombol / Button Dan Cara Kerja Fitur Pengaturan :
                            </b>
                        </h6> 
                        <ol>
                            @include('default_theme.bantuan.button')
                            <li>Nama awal adalah nama awal dari aplikasi</li>
                            <li>Nama tengah adalah nama tengah dari aplikasi</li>
                            <li>Nama akhir adalah nama akhir dari aplikasi</li>
                        </ol>
                        <h6><b>Penjelasan Mengenai Jam Masuk Dan Jam Pulang</b></h6>
                        <ol>
                            <li>
                                Jam masuk di set jika ada perubahan jam masuk sekolah, contoh jika jam masuk pada bulan suci ramadhan masuk lebih siang pada jam 08:00 maka setting jam masuk pada jam 08:00, jika terdapat konsekuensi waktu 15 menit maka perlu ditambahkan menit di belakang nya maka jam masuk menjadi 08:15
                            </li>
                            <li>
                                Jam keluar di set jika ada perubahan jam keluar sekolah, contoh jika jam keluar pada saat ujian sekolah maka jam pulang lebih awal sehingga bisa ditentukan jam minimal pulang sekolah
                            </li>
                        </ol>
                        <h6><b>Rincian penjelasan mengenai schedular : </b></h6>
                        <ol>
                            <li>
                                Jika kurang dari <b class="red-text text-accent-3">in_clock (Jam Masuk) </b> maka status absen siswa adalah <b class="red-text text-accent-3">HADIR</b>;
                            </li>
                            <li>
                                Jika Melebihi dari <b class="red-text text-accent-3">in_clock (Jam Masuk) </b> dan kurang dari <b class="red-text text-accent-3">time_off_schedular_in</b> dan jika siswa mengabsen maka status <b class="red-text text-accent-3">TERLAMBAT</b>, jika belum mengabsen maka absen siswa belum masuk ke dalam database;
                            </li>
                            <li>
                                Jika melebihi dari <b class="red-text text-accent-3">time_off_schedular_in</b> maka status absen siswa dinyatakan <b class="red-text text-accent-3">ALFA</b>;
                            </li>
                            <li>
                                Jika waktu lebih dari <b class="red-text text-accent-3">out_clock</b> maka schedular untuk mengecek jam pulang telah berjalan, jika kurang maka schedular jam pulang belum berjalan;
                            </li>
                            <li>
                                Jika terdapat siswa pulang sebelum ketentuan jam pulang, maka siswa harus menunggu waktu jam pulang, untuk melakukan <b class="red-text text-accent-3">TAP SIDIK JARI</b> pada mesin finger, jika tidak menunggu dan mengabsen sebelum waktunya pulang, maka absen pulang sekolah tidak akan terupdate pada database;
                            </li>
                            <li>
                                <b class="red-text text-accent-3">time_off_schedular_out</b> adalah batas waktu dimana berhentinya pengecekan jam pulang dan semua schedular tidak ada yang berjalan;
                            </li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/aplikasi.js') }}"></script>
@endsection