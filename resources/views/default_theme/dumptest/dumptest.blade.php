@extends('default_theme.main')

@section('content')
<div class="row no-m-t no-m-b">
    <div class="col s12 m12 l12">
        <div class="card invoices-card">
            <div class="card-content">
                <div class="card-options">
                    <input type="text" class="expand-search" placeholder="Search" autocomplete="off">
                </div>
                <span class="card-title">Fingerprint</span>
                <div class="test_pusher"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        //instantiate a Pusher object with our Credential's key

        // Pusher.logToConsole = true;

        // var pusher = new Pusher('593a18374af6cda615d4',
        // {
        //     cluster: 'ap1',
        //     encrypted: false
        // });

        // var channel = pusher.subscribe('sms-channel');
        // var event   = pusher.subscribe('sms-event');

        // channel.bind('sms-event', function(e)
        // {
        //   console.log(e);
        // });

        // channel.bind('App\\Events\\ReceiveMessages', function(e)
        // {
        //     jQuery('.test_pusher').append('<p>' + e.message + '</p>');
        // });
    </script>
@endsection