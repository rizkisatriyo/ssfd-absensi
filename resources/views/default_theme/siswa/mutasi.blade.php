<div class="col s6">
    {{ Form::label('kelas', 'Pilih Kelas', ['class' => 'active']) }}
    <select id="f_class" class="browser-default" style="width:100%;">
        <option value="" selected>- Pilih Kelas -</option>
    </select>
</div>
<div class="col s6">
    {{ Form::label('kelas', 'Mutasi Ke Kelas', ['class' => 'active']) }}
    <select id="t_class" class="browser-default" style="width:100%;">
        <option value="" selected>- Pilih Kelas -</option>
        <option value="alumni">Alumni</option>
    </select>
</div>
<div class="col s12">
    <br>
    {{ Form::button('Mutasi', ['class'=>'waves-effect waves-light btn green btn-mutasi']) }}
</div>