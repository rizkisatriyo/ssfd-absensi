<div class="input-field col s6 m6 l6 xl6 m6 l6 xl6">
    {{ Form::text('nis', null, ['class'=>'validate']) }}
    {{ Form::label('nis', 'NIS', ['class' => 'active']) }}
</div>
<div class="input-field col s12 l12 m12 xl12">
    {{ Form::text('name', null, ['class'=>'validate']) }}
    {{ Form::label('name', 'Nama Siswa', ['class' => 'active']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::text('tmplahir', null, ['class'=>'validate','length' => '120']) }}
    {{ Form::label('tmplahir', 'Tempat Lahir', ['class' => 'active']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::date('tgllahir', \Carbon\Carbon::now()) }}
    {{ Form::label('tgllahir', 'Tanggal Lahir', ['class' => 'active']) }}
</div>
<div class="input-field col s12 l12 m12 xl12">
    {{ Form::text('tahun_masuk', null, ['class'=>'validate']) }}
    {{ Form::label('tahun_masuk', 'Tahun Masuk', ['class' => 'active']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::label('kelamin', 'Jenis Kelamin', ['class' => 'active']) }}
    {{ Form::select('kelamin', ['Laki-Laki'=>'Laki-Laki','Perempuan'=>'Perempuan'], null, ['placeholder' => ' - Pilih jenis kelamin -','class' => 'browser-default']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::label('grade_id', 'Kelas', ['class' => 'active']) }}
    {{ Form::select('grade_id', App\Grade::pluck('name','id')->all(), null, ['placeholder' => 'Pilih Kelas','class' => 'browser-default'])  }}
</div>
<div class="input-field col s12 l12 m12 xl12"></div>
<div class="input-field col s12 l12 m12 xl12">
    {{ Form::text('alamat', null, ['class'=>'validate','length' => '120']) }}
    {{ Form::label('alamat', 'Alamat Rumah', ['class' => 'active']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::text('kota', null, ['class'=>'validate']) }}
    {{ Form::label('kota', 'Kota', ['class' => 'active']) }}
</div>
<div class="input-field col s6 m6 l6 xl6">
    {{ Form::text('nohp_ortu', null, ['class'=>'validate']) }}
    {{ Form::label('nohp_ortu', 'Nomor Handphone', ['class' => 'active']) }}
</div>
<div class="input-field col s12 l12 m12 xl12">
    <div class="file-field input-field">
        <a data-fancybox data-type="iframe" href="{{ asset('assets/js/filemanager/dialog.php?type=1&field_id=foto&relative_url=1') }}" class="btn btn-block teal lighten-1" type="button">
            Foto Siswa
        </a>
        <div class="file-path-wrapper">
            {{ Form::text('foto', null, ['class'=>'validate','id' => 'foto']) }}
        </div>
    </div>
</div>