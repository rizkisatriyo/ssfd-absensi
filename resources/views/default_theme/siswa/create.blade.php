<div class="row">
    @php
        $q = App\Student::all()->last();
        if(isset($q->pin)) {
            $q = $q->pin + 1;
        } else {
            $q = 1;
        }
    @endphp
    {{ Form::open(['id' => 'form_add_siswa']) }}
	    <div class="input-field col s6 m6 l6 xl6 m6 l6 xl6">
	        {{ Form::text('pin', $q, ['class'=>'validate']) }}
	        {{ Form::label('pin', 'PIN', ['class' => 'active']) }}
	    </div>
        @include('default_theme.siswa.form')
        <div class="input-field col s12 right-align">
            {{ Form::button('Simpan', ['class'=>'waves-effect waves-light btn green','id' => 'btn_add_siswa']) }}
        </div>
    {{ Form::close() }}
</div>