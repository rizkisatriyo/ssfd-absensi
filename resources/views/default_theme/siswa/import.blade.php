<div class="col s12">
    <div class="file-field input-field">
        <div class="btn teal lighten-1" id="btn_upload_file_xls">
            <span>Import File</span>
            <input type="file" id="file_xls" name="file_xlsx">
        </div>
        <div class="file-path-wrapper">
            <input class="file-path validate valid" placeholder="Import File Excel" type="text">
        </div>
    </div>
</div>
<div class="col s12">
    <table class="highlight error_log" style="display:none;">
        <thead>
            <tr>
                <th>NO</th>
                <th>NAMA SISWA</th>
                <th>RESPONSE</th>
            </tr>
        </thead>
        <tbody class="error_tbody_log"></tbody>
    </table>
</div>
