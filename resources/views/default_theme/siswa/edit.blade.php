<div class="row">
	{{ Form::model($student, ['id' => 'form_update_siswa']) }}
		{{ Form::hidden('id', null, ['id' => 'id_siswa']) }}
	    <div class="input-field col s6 m6 l6 xl6 m6 l6 xl6">
	        {{ Form::text('pin', null, ['class'=>'validate', 'disabled' => 'disabled']) }}
	        {{ Form::label('pin', 'PIN', ['class' => 'active']) }}
	    </div>
        @include('default_theme.siswa.form')
        <div class="input-field col s12 right-align">
            {{ Form::button('Simpan', ['class'=>'waves-effect waves-light btn green', 'id' => 'btn_update_siswa']) }}
        </div>
	{{ Form::close() }}
</div>