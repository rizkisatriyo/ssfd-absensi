@extends('default_theme.main')
@section('content')
<div class="row no-m-t no-m-b">
    <div class="col s12 m12 l12">
        <div class="card invoices-card">
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3">
                                <a class="active" href="#mf">
                                    Siswa
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a href="#export" id="tab_export">
                                    Export
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a href="#import" id="tab_import">
                                    Import
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a href="#mutasi" id="tab_mutasi">
                                    Mutasi
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a  href="#help">
                                    Bantuan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="mf" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <div class="responsive-table">
                            {{ $html->table(['class' => 'highlight centered','style' => 'border:none;']) }}
                        </div>
                    </div>
                    <div id="export" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        @include('default_theme.siswa.export')
                    </div>
                    <div id="mutasi" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        @include('default_theme.siswa.mutasi')
                    </div>
                    <div id="import" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        @include('default_theme.siswa.import')
                    </div>
                    <div id="help" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <h6><b>Penjelasan Tombol / Button :</b></h6> 
                        <ol>
                            @include('default_theme.bantuan.button')
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {{ $html->scripts() }}
    <script src="{{ asset('assets/js/student-datatable-injector.js') }}"></script>
    <script src="{{ asset('assets/js/export_to_fingerprint.js') }}"></script>
    <script src="{{ asset('assets/js/import_data_siswa.js') }}"></script>
    <script src="{{ asset('assets/js/mutasi_siswa.js') }}"></script>
    <script src="{{ asset('assets/js/add_siswa.js') }}"></script>
    <script src="{{ asset('assets/js/update_siswa.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).on('click','#btn_add_siswa', function(e) {
            var param = {
                url : "{{Route('student.store')}}",
                senData : serializer(e, '#form_add_siswa')
            };
            add_siswa(param);
        });
        jQuery(document).on('click','#btn_update_siswa', function(e) {
            var param = {
                url : "{{Route('student.update','')}}/" + jQuery('#id_siswa').val(),
                senData : serializer(e, '#form_update_siswa')
            };
            update_siswa(param);
        });
    </script>
@endsection