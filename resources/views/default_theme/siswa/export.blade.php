<div class="input-field col s6 m6 xl6 l6">
    {{ Form::label('kelas', 'Pilih Kelas', ['class' => 'active']) }}
    <select name="kelas" id="kelas" class="browser-default" style="width:100%;">
        <option value="" selected>- Pilih Kelas - </option>
	</select>
</div>
<div class="input-field col s6 m6 xl6 l6">
    {{ Form::label('mesin', 'Pilih Mesin', ['class' => 'active']) }}
    <select name="mesin" id="mesin" class="browser-default" style="width:100%;">
    	<option value="" selected>- Pilih Mesin -</option>
	</select>
</div>
<div class="col s12">
	<br>
 	{{ Form::button('Export', ['class'=>'waves-effect waves-light btn green export_to_finger']) }}
    {{ Form::button('Abort', ['class'=>'waves-effect waves-light btn red btn_abort']) }}
</div>
<div class="col s12"><br>
	<textarea id="list_student" class="materialize-textarea" style="display:none;"></textarea>
    <div class="responsive-table">
        <table class="highlight striped">
            <thead>
                <tr>
                    <th class="center-align">No.</th>
                    <th class="center-align">Nama</th>
                </tr>
            </thead>
            <tbody class="tbody_siswa"></tbody>
        </table>
    </div>
</div>
