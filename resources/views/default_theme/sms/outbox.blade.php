<div class="row">

    <div style="padding-bottom: 30px;"></div>

    {!! Form::open(['id' => 'form_get_outbox']) !!}

    <div class="input-field col s5">
        {{ Form::date('outbox_datefrom', \Carbon\Carbon::now()), ['class' => 'outbox_datefrom'] }}
        {{ Form::label('outbox_datefrom', 'Dari Tanggal', ['class' => 'active']) }}
    </div>
    <div class="input-field col s5">
        {{ Form::date('outbox_dateto', \Carbon\Carbon::now()), ['class' => 'outbox_dateto'] }}
        {{ Form::label('outbox_dateto', 'Sampai Tanggal', ['class' => 'active']) }}
    </div>
    <div class="input-field col s2">
        {{ Form::button('<i class="material-icons">search</i>',['class' => 'waves-effect waves-light btn BtnShowOutbox'] ) }}
    </div>

    {!! Form::close() !!}

    <div class="col s12">
        <div style="padding-bottom: 30px;"></div>
        <b>
            Kotak keluar yang dipilih berdasarkan dari tanggal
            <mark class="outbox_dateFromMark">yyyy-mm-dd</mark> sampai dengan tanggal <mark class="outbox_dateToMark">yyyy-mm-dd</mark>
        </b>
        <div class="responsive-table">
            <table class="highlight">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nomor Tujuan</th>
                        <th>Pesan</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody class="outbox_tbody"></tbody>
            </table>
        </div>
    </div>

</div>