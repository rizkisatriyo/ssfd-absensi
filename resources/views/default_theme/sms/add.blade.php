<div class="row">
	<div style="padding-bottom: 30px;"></div>
	{{ csrf_field() }}
	<div class="input-field col s6" id="option_set">
	    <select class="js-example-tokenizer js-states browser-default" multiple="multiple" style="width: 100%">
	        <optgroup label="Silakan ketik nomor tujuan. Contoh : 081xxxxxxxxx [Enter] untuk nomor hp tujuan selanjutnya" id="optgroup"></optgroup>
	    </select>
	</div>
	<div class="input-field col s12">
	    <textarea id="pesan" class="materialize-textarea" maxlength="500" length="500"></textarea>
	    <label for="pesan" class="">Pesan</label>
	</div>
	<div class="input-field col s12">
	    <button class="waves-effect waves-light btn" id="SendSMSBtn" type="button">
	        Kirim
	    </button>
	</div>
</div>