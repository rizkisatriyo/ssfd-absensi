@extends('default_theme.main')

@section('content')
<div class="middle-content">

    <div class="row no-m-t no-m-b">
        <div class="col s12 m12 l4">
            <div class="card stats-card">
                <div class="card-content">
                    <span class="card-title">SMS Credit</span>
                    <span class="stats-counter">
                        <span class="counter" id="credit-sms-counter"></span>
                        <small>Credit</small>
                    </span>
                </div>
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </div>
            <div class="col s12 m12 l4">
            <div class="card stats-card">
                <div class="card-content">
                    <span class="card-title">Expire Credit</span>
                    <span class="stats-counter">
                        <span class="counter" id="credit-sms-expire"></span>
                        <small></small>
                    </span>
                </div>
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </div>
        <div class="col s12 m12 l4">
            <div class="card stats-card">
                <div class="card-content">
                    <span class="card-title">Pesan Keluar</span>
                    <span class="stats-counter">
                        <span class="counter">16</span>
                        <small>Pesan</small>
                    </span>
                </div>
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row no-m-t no-m-b">
        <div class="col s12 m12 l12">
            <div class="card invoices-card">
                <div class="card-content">

                    <div class="row">

                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s3">
                                    <a class="active" href="#send">
                                        Kirim Pesan
                                    </a>
                                </li>
                                <li class="tab col s3">
                                    <a  href="#inbox">
                                        Kotak Masuk
                                    </a>
                                </li>
                                <li class="tab col s3">
                                    <a  href="#outbox">
                                        Kotak Keluar
                                    </a>
                                </li>
                                <li class="tab col s3">
                                    <a  href="#help">
                                        Bantuan
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="send" class="col s12">
                            @include('default_theme.sms.add')
                        </div>
                        <div id="inbox" class="col s12">
                            @include('default_theme.sms.inbox')
                        </div>
                        <div id="outbox" class="col s12">
                            @include('default_theme.sms.outbox')
                        </div>
                        <div id="help" class="col s12">
                           <div style="padding-bottom: 30px;"></div>
                            <h6><b>Penjelasan Tombol / Button :</b></h6> 
                            <ol>
                                @include('default_theme.bantuan.button')
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="inner-sidebar">
    <span class="inner-sidebar-title">
        Pesan Terbaru
    </span>
    <div class="message-list">
        <div class="info-item message-item">
            <img class="circle" src="{{ asset('assets/images/profile-image-2.png') }}" alt="">
            <div class="message-info">
                <div class="message-author">Maman Abdul</div>
                <small>3 hours ago</small>
            </div>
        </div>
    </div>
    <div class="inner-sidebar-divider"></div>

</div> -->

@endsection

@section('scripts')
    <script type="text/javascript">
        jQuery(function()
        {
            /*
            *   === [ GET SMS CREDIT ] ===
            */
            get_credit();
            /*
            *   === [ ADD STYLE CSSATTRIBUTES ] ===
            */
            jQuery('.loaded').attr('style','overflow: hidden; width: 1350px;');
            jQuery('#chat-sidebar').attr('style','transform: translateX(0px);');
        });
    </script>

@endsection