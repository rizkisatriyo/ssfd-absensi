<div class="row">

    <div style="padding-bottom: 30px;"></div>

    {{ Form::open(['id' => 'form_get_inbox']) }}

    <div class="input-field col s5">
        {{ Form::date('inbox_datefrom', \Carbon\Carbon::now()), ['class' => 'inbox_datefrom'] }}
        {{ Form::label('inbox_datefrom', 'Dari Tanggal', ['class' => 'active']) }}
    </div>
    <div class="input-field col s5">
        {{ Form::date('inbox_dateto', \Carbon\Carbon::now()), ['class' => 'inbox_dateto'] }}
        {{ Form::label('inbox_dateto', 'Sampai Tanggal', ['class' => 'active']) }}
    </div>
    <div class="input-field col s2">
        {{ Form::button('<i class="material-icons">search</i>',['class' => 'waves-effect waves-light btn BtnShowInbox'] ) }}
    </div>

    {{ Form::close() }}

    <div class="col s12">
        <div style="padding-bottom: 30px;"></div>
        <b>
            Kotak masuk yang dipilih berdasarkan dari tanggal
            <mark class="inbox_dateFromMark">yyyy-mm-dd</mark> sampai dengan tanggal <mark class="inbox_dateToMark">yyyy-mm-dd</mark>
        </b>
        <div class="responsive-table">
            <table class="highlight">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Pengirim</th>
                        <th>Pesan</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody class="inbox_tbody"></tbody>
            </table>
        </div>
    </div>

</div>