<div class="row">

	{{ Form::model($finger, ['id' => 'form_update_fingerprint']) }}
		{{ Form::hidden('id', null, ['id' => 'id']) }}
		<div class="input-field col s12">
		    {{ Form::text('ip_address', null, ['class'=>'validate']) }}
		    {{ Form::label('ip_address', 'IP Address', ['class' => 'active']) }}
		</div>
		<div class="input-field col s6">
			{{ Form::text('user_id', null, ['class'=>'validate']) }}
			{{ Form::label('user_id', 'User ID', ['class' => 'active']) }}
		</div>
		<div class="input-field col s6">
			{{ Form::text('password', null, ['class'=>'validate']) }}
			{{ Form::label('password', 'Password', ['class' => 'active']) }}
		</div>
		<div class="input-field col s10">
			{{ Form::text('name', null, ['class'=>'validate']) }}
			{{ Form::label('name', 'Nama Mesin', ['class' => 'active']) }}
		</div>
		<div class="input-field col s2">
			{{ Form::text('com_key', null, ['class'=>'validate']) }}
			{{ Form::label('com_key', 'Com Key', ['class' => 'active']) }}
		</div>
		<div class="input-field col s12">
			{{ Form::button('Simpan', ['class'=>'btn btn-primary btn-small waves-effect waves-light m-b-xs','id' => 'button_update_fingerprint']) }}
		</div>
	{{ Form::close() }}

</div>