<div class="row" id="preview" data-id="{{ $show->id }}">
	<center>
		@if($response['code'] == 200)
		<table class="highlight">
			<tbody>
				<tr>
					<td><b>Nama Device</b></td>
					<td>{{ $response['results']['device_name'] }}</td>
				</tr>
				<tr>
					<td><b>Nomor Seri</b></td>
					<td>{{ $response['results']['serial_number'] }}</td>
				</tr>
				<tr>
					<td><b>Tanggal Produksi</b></td>
					<td>{{ $response['results']['produce_date'] }}</td>
				</tr>
				<tr>
					<td><b>Ip Address</b></td>
					<td>{{ $response['results']['ip_address'] }}</td>
				</tr>
				<tr>
					<td><b>User ID</b></td>
					<td>{{ $response['results']['user_id'] }}</td>
				</tr>
				<tr>
					<td><b>Password</b></td>
					<td>{{ $response['results']['password'] }}</td>
				</tr>
				<tr>
					<td><b>ComKey</b></td>
					<td>{{ $response['results']['com_key'] }}</td>
				</tr>
				<tr>
					<td><b>Kapasitas Pengguna</b></td>
					<td>{{ $response['results']['user_capacity'] }}</td>
				</tr>
				<tr>
					<td><b>Kapasitas Transaksi</b></td>
					<td>{{ $response['results']['trans_capacity'] }}</td>
				</tr>
				<tr>
					<td><b>Kapasitas Sidik Jari</b></td>
					<td>{{ $response['results']['finger_capacity'] }}</td>
				</tr>
				<tr>
					<td><b>Kunci</b></td>
					<td>{{ $response['results']['lock'] }}</td>
				</tr>
				<tr>
					<td><b>Kartu ID</b></td>
					<td>{{ $response['results']['rf_card'] }}</td>
				</tr>
				<tr>
					<td><b>Pesan Singkat</b></td>
					<td>{{ $response['results']['short_message'] }}</td>
				</tr>
				<tr>
					<td><b>USB Disk</b></td>
					<td>{{ $response['results']['usb_disk'] }}</td>
				</tr>
				<tr>
					<td><b>USB Client</b></td>
					<td>{{ $response['results']['usb_client'] }}</td>
				</tr>
				<tr>
					<td><b>Ditambahakan tanggal</b></td>
					<td>{{ $show->created_at }}</td>
				</tr>
				<tr>
					<td><b>Diubah tanggal</b></td>
					<td>{{ $show->updated_at }}</td>
				</tr>
				<tr>
					<td><b>Status</b></td>
					<td>Connected</td>
				</tr>
			</tbody>
		</table>
		@else
			{{ $response['results'] }}
		@endif

		<h6>klik pada tombol dibawah jika ingin menghapus data ini...!</h6>
		<button class="btn red m-b-xs waves-effect waves-light" id="button_delete_fingerprint"> 
			Hapus
		</button>

	</center>
</div>