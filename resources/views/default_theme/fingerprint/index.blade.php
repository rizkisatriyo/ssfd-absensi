@extends('default_theme.main')
@section('content')
<div class="row no-m-t no-m-b">
    <div class="col s12 m12 l12">
        <div class="card invoices-card">
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3">
                                <a class="active" href="#mf">
                                    Pengaturan Fingerprint
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a  href="#help">
                                    Bantuan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="mf" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <div class="responsive-table">
                            <table class="dataTableFingerprint highlight centered" style="border:none;width:100%;">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nama Mesin</th>
                                        <th>Ip Address</th>
                                        <th>Com Key</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div id="help" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <h6><b>Penjelasan Tombol / Button :</b></h6> 
                        <ol>
                            @include('default_theme.bantuan.button')
                             <li>
                                Button <small><i class="material-icons">check</i></small> untuk cek koneksi ke mesin sidik jari.
                            </li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('assets/js/fingerprint.js') }}"></script>
@endsection