<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <title>
           Dashboard - Administrator
        </title>
        @include('default_theme.loaders.meta_loader')
        @include('default_theme.loaders.css_loader')
    </head>

<body>

    <div class="loader-bg"></div>

    <!-- LOADER  -->
        @include('default_theme.loaders.progress_loader')
    <!-- ./END LOADER  -->

    <!-- MN-CONTENT  -->
        <div class="mn-content fixed-sidebar">

            <!-- HEADER  -->
                <header class="mn-header navbar-fixed">
                    <nav class="cyan darken-1">
                        @include('default_theme.loaders.header_loader')
                    </nav>
                </header>
            <!-- ./END HEADER -->

            <!-- ASIDE  -->
                <aside id="chat-sidebar" class="side-nav white">
                    <div class="side-nav-wrapper">
                        @include('default_theme.loaders.chatsidebar_loader')
                    </div>
                </aside>
                <aside id="chat-messages" class="side-nav white">
                    @include('default_theme.loaders.chatmessage_loader')
                </aside>
                <aside id="slide-out" class="side-nav white fixed">
                    <div class="side-nav-wrapper">
                        @include('default_theme.loaders.slideout_loader')
                    </div>
                </aside>
            <!-- ./END ASIDE  -->

            <!-- MAIN  -->
                <main class="mn-inner inner-active-sidebar">

                    <!-- MIDDLE CONTENT  -->
                        <!-- <div class="middle-content"> -->
                           @yield('content')
                    <!-- ./END MIDDLE CONTENT  -->

                    <!-- INNDER  -->
<!--                         <div class="inner-sidebar">
                            @include('default_theme.loaders.innersidebar_loader')
                        </div> -->
                    <!-- ./END INNER -->

                </main>
            <!-- ./END MAIN  -->

            <!-- FOOTER  -->
                <div class="page-footer">
                    @include('default_theme.loaders.footer_loader')
                </div>
            <!-- ./END FOOTER  -->
            @include('default_theme.modal.modal')
        </div>
    <!-- ./END MN-CONTENT  -->

        <div class="left-sidebar-hover"></div>

    <!-- JS LOADER  -->
        @include('default_theme.loaders.js_loader')
    <!-- ./END JS LOADER  -->

</body>
</html>