<div class="input-field col s12 m12 l6 xl6">
    {{ Form::label('status', 'Status Kehadiran', ['class' => 'active']) }}
    <select class="browser-default" name="status">
        @if($attendance->hadir == 1)
            <option value="hadir" selected="selected">Hadir</option>
        @else
            <option value="hadir">Hadir</option>
        @endif

        @if($attendance->terlambat == 1)
            <option value="terlambat" selected="selected">Terlambat</option>
        @else
            <option value="terlambat">Terlambat</option>
        @endif

        @if($attendance->alfa == 1)
            <option value="alfa" selected="selected">Alfa</option>
        @else
            <option value="alfa">Alfa</option>
        @endif

        @if($attendance->sakit == 1)
            <option value="sakit" selected="selected">Sakit</option>
        @else
            <option value="sakit">Sakit</option>
        @endif

        @if($attendance->izin == 1)
            <option value="izin" selected="selected">Izin</option>
        @else
            <option value="izin">Izin</option>
        @endif
    </select>
</div>
<div class="input-field col s12 m12 l6 xl6">
    {{ Form::text('jam_masuk', $attendance->jam_masuk, ['class'=>'validate']) }}
    {{ Form::label('jam_masuk', 'Jam Masuk', ['class' => 'active']) }}
</div>
<div class="input-field col s12 m12 l12 xl12 right-align">
    {{ Form::button('Simpan', ['class'=>'waves-effect waves-light btn green', 'id' => 'btn_update_absensi']) }}
</div>