@extends('default_theme.main')
@section('content')
    <div class="row no-m-t no-m-b">
        <div class="col s12 m12 l12">
            <div class="card invoices-card">
                <div class="card-content">
                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs">
                                <li class="tab col s3">
                                    <a class="active" href="#tab_absensi">
                                        Absensi
                                    </a>
                                </li>
                                <li class="tab col s3">
                                    <a href="#help">
                                        Bantuan
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div id="tab_absensi" class="col s12">
                            <div style="padding-bottom: 30px;"></div>
                            <div class="responsive-table">
                                <table class="dataTableAttendances highlight" style="border:none;width:100%;">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th style="width:30%;">Nama</th>
                                            <th style="width:10%;">Jam Masuk</th>
                                            <th style="width:10%;">Jam Keluar</th>
                                            <th style="width:10%;">Pulang</th>
                                            <th style="width:10%;">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div id="help" class="col s12">
                            <div style="padding-bottom: 30px;"></div>
                            <h6><b>Penjelasan Tombol / Button :</b></h6> 
                            <ol>
                                @include('default_theme.bantuan.button')
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    var table = jQuery('.dataTableAttendances').DataTable({
        dom         : 'Bfrtip',
        select      : true,
        ordering    : false,
        processing  : true,
        serverSide  : true,
        buttons     : [
            {
                text        : '<i class="material-icons">edit</i>',
                className   : 'btn-floating btn-primary btn-small waves-effect waves-light update_title',
                action      : function(e, dt, node, config)
                {
                    var res    =   dt.row( { selected: true } ).data();
                    if(res != undefined)
                    {
                        // Ganti title pada modal
                        jQuery('#modal_header').html('Update data');
                        var route = '{{Route("attedances.edit","ID")}}';
                        // Ambil form edit data dengan xhr request
                        jQuery.get(route.replace("ID",res.id), function(response)
                        {
                            jQuery('#modal_content').html(response);
                        });
                        // Open modal
                        jQuery('.modal').openModal();
                    } else
                    {
                        notify('Silakan pilih data terlebih dahulu!',4000);
                    }
                }
            },
        ],
        columns     :
        [
            {
                data        : 'id',
                name        : 'id',
                visible     : false
            },    
            {
                data        : 'nama_siswa',
                name        : 'nama_siswa',
            },
            {
                data        : 'jam_masuk',
                name        : 'jam_masuk',
            },
            {
                data        : 'jam_keluar',
                name        : 'jam_keluar',
            },
            {
                data        : 'pulang',
                name        : 'pulang',
            },
            {
                data        : 'status',
                name        : 'status',
            }
        ],
        ajax        : { url : '{{Route("attedances.index")}}' }
    });

    jQuery(document).on('click','#btn_update_absensi', function(e) {
        var FormData    = serializer(e, '#form_update_absensi');
        var id          = jQuery('#id_absensi').val();
        jQuery.ajax({
            url         : base_url + segment + 'attedances/' + id,
            dataType    : 'JSON',
            type        : 'PUT',
            data        : FormData,
            headers     : _token(),
            beforeSend  : function()
            {
                notify('Memproses...', 500);
            },
            error       : function()
            {
               notify('Error silakan coba lagi!',500);
            },
            success     : function(responses)
            {
                table.ajax.reload();

                if(responses.error == 1)
                {
                    for(var i = 0; i < responses.results.length; i++) {
                        notify(responses.results[i],4000);
                    }
                } else
                {
                    jQuery('.modal').closeModal();
                    notify('Data berhasil diperbarui!',4000);
                }
            }
        });
    });
</script>
@endsection