{!! Form::open(['url' => route('report.api'), 'method' => 'post']) !!}
<div class="input-field col s3">
    {{ Form::label('Kelas', 'Pilih Kelas', ['class' => 'active']) }}
    {{ Form::select(
    	'name[]',
    	App\Grade::pluck('name','name')->all(),
    	null,
    	[
    		'placeholder' => '- Pilih Kelas -',
    		'class' => 'browser-default',
    		'required' => 'required'
    	]
    )}}
</div>
<div class="input-field col s3">
	{{ Form::date('f_date', \Carbon\Carbon::now()) }}
	{{ Form::label('f_date', 'Dari Tanggal', ['class' => 'active']) }}
</div>
<div class="input-field col s3">
	{{ Form::date('t_date', \Carbon\Carbon::now()) }}
	{{ Form::label('t_date', 'Sampai Tanggal', ['class' => 'active']) }}
</div>
<div class="input-field col s3 right-align">
	<div style="padding-bottom: 5px;"></div>
    {{ Form::submit('Cetak Rekap', ['class'=>'waves-effect waves-light btn green']) }}
</div>
{!! Form::close() !!}