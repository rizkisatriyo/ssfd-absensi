{{ Form::model($model, ['url' => $hapus, 'method' => 'delete' , 'class' => 'data-inject','data-id' => $model->id, 'route-edit' => $edit]) }} 

	{{ Form::button('<i class="material-icons" style="color:#fff;">edit</i>', ['class'=>'btn-floating btn-primary waves-effect waves-light','id' => 'editData']) }}

	{{ Form::button('<i class="material-icons" style="color:#fff;">delete</i>', ['class'=>'btn-floating btn red waves-effect waves-light', 'type' => 'submit']) }}

{{ Form::close() }}