@extends('default_theme.main')

@section('content')

    <div class="row">
        <div class="col s12 m12 l12">
            <div class="card invoices-card">
                <div class="card-content">
                    <span class="card-title">Grafik Data Master</span>
                    <div class="row">
                        <div class="col m12 xl10 offset-xl10 offset-l1 l10 s12">
                            <canvas id="myChart" class="responsive-img"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-m-t no-m-b">
        <div class="col s12 m12 l12">
            <div class="card invoices-card">
                <div class="card-content">
                    <span class="card-title">Data Ringkas Absensi Hari Ini</span>
                    <table class="table dataTableAttendances highlight" style="border:none;width:100%;">
                        <thead>
                            <tr>
                                <th style="width:30%;">NAMA</th>
                                <th style="width:10%;">KELAS</th>
                                <th style="width:5%;">H</th>
                                <th style="width:5%;">S</th>
                                <th style="width:5%;">T</th>
                                <th style="width:5%;">A</th>
                                <th style="width:5%;">I</th>
                                <th style="width:5%;">P</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/js/attendances.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript">
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx,
        {
            type: 'bar',
            data: {
                labels : [
                    "Data Kelas",
                    "Data Siswa",
                    "Data Absensi",
                    "Data Mesin Finger",
                ],
                datasets: [{
                    label: "Grafik Data Master",
                    backgroundColor: ['#1abc9c','#3498db','#9b59b6','#f1c40f'],
                    data: [ "{{ $kelas }}", "{{$siswa}}" , "{{$attedances}}" , "{{ $fingerprint }}" ],
                }]
            },

            // Configuration options go here
            options: {}
        });
    </script>
@endsection