<!DOCTYPE html>
<html>
<head>
        
 <!-- Title -->
<title>Login Administrator</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta charset="UTF-8">
<meta id="token" name="token" content="{{ csrf_token() }}">
        
 <!-- Styles -->
<link type="text/css" rel="stylesheet" href="{{ asset('assets/plugins/materialize/css/materialize.min.css') }}"/>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="{{ asset('assets/plugins/material-preloader/css/materialPreloader.min.css') }}" rel="stylesheet">

 <!-- Theme Styles -->
<link href="{{ asset('assets/css/alpha.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css"/>
 
</head>
    <body class="signin-page">
        <div class="loader-bg"></div>
            <!--PARTIAL VIEW -->
          @include('default_theme.loaders.progress_loader') 

        <div class="mn-content valign-wrapper">
            <main class="mn-inner container">
                <div class="valign">
                      <div class="row">
                          <div class="col s12 m6 l4 offset-l4 offset-m3">
                              <div class="card white darken-1">
                                  <div class="card-content">
                                      <span class="card-title">Sign In</span>
                                       <div class="row">
                                        <form class="col s12" role="form" method="post" action="{{ url('/login') }}">
                                         {{ csrf_field() }}

                                          <div class="input-field col s12">
                                              <input id="username" type="text" class="validate" name="username" value="{{ old('username') }}" required autofocus>
                                              <label for="username">Username</label>
                                          </div>

                                          <div class="input-field col s12">
                                              <input id="password" type="password" class="validate" name="password" required>
                                              <label for="password">Password</label>
                                          </div>

                                          <div class="col s12 m-t-md">
                                            <button type="submit" class="btn btn-primary">Masuk</button>
                                          </div>

                                           </form>
                                            
                                      </div>
                                  </div>
                              </div>
                          </div>
                    </div>
                </div>
            </main>
        </div>
        
<!-- JAVASCRIPTS -->
<script src="{{ asset('assets/plugins/jquery/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/plugins/materialize/js/materialize.min.js') }}"></script>
<script src="{{ asset('assets/plugins/material-preloader/js/materialPreloader.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
<script src="{{ asset('assets/js/alpha.min.js') }}"></script>

  @if(session('warning'))
    <script type="text/javascript">
      Materialize.toast("{{ session('warning') }}", 4000)
    </script>
  @endif
</body>
</html>