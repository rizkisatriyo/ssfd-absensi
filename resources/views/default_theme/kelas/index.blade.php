@extends('default_theme.main')
@section('content')
<div class="row no-m-t no-m-b">
    <div class="col s12 m12 l12">
        <div class="card invoices-card">
            <div class="card-content">

                <div class="row">
                    <div class="col s12">
                        <ul class="tabs">
                            <li class="tab col s3">
                                <a class="active" href="#mf">
                                    Data Kelas
                                </a>
                            </li>
                            <li class="tab col s3">
                                <a  href="#help">
                                    Bantuan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="mf" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <div class="responsive-table">
                            {{ $html->table(['class' => 'highlight centered','style' => 'border:none;']) }}
                        </div>
                    </div>
                    <div id="help" class="col s12">
                        <div style="padding-bottom: 30px;"></div>
                        <h6><b>Penjelasan Tombol / Button :</b></h6> 
                        <ol>
                            @include('default_theme.bantuan.button')
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    {{ $html->scripts() }}
    <script src="{{ asset('assets/js/grade-datatable-injector.js') }}"></script>
@endsection