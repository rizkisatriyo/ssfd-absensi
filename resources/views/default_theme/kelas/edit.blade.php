<div class="row">

    {{ Form::model($grade, ['url' => Route('grade.update', $grade->id), 'method' => 'put', 'class' => 'form-horizontal']) }}

        <div class="input-field col s12">
            {{ Form::text('name', null, ['class' => 'validate']) }}
            {{ Form::label('name', 'Nama Kelas', ['class' => 'active']) }}
        </div>
        <div class="input-field col s12">
            {{ Form::submit('Simpan', ['class' => 'btn btn-primary btn-small waves-effect waves-light m-b-xs']) }}
        </div>

    {{ Form::close() }}

</div>