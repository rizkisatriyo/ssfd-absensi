<div class="modal" id="modal-form" style="z-index: 2000; display: none; opacity: 0; transform: scaleX(1; top: 600.516304347826px;">
    <div class="modal-content">
        <div class="card-content">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <form method="POST" id="example-form" action="#">
                            {{ csrf_field() }} {{ method_field('POST') }}
                            <span class="card-title">Tambah Kelas</span><br>
                                <div class="row">
                                    <div class="col m6">
                                        <div class="row">
                                            <input type="hidden" id="id" name="id">
                                            <div class="input-field col s12">
                                                <label for="name">Nama Kelas</label>
                                                <input id="name" name="name" type="text" class="{{ $errors->has('name') ? 'has-error' : '' }}" value="{{ old('name') }}" class="required validate">
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-light btn cyan darken-1">Simpan</a>
            <a href="#!" class="modal-action modal-close waves-effect waves-blue btn-flat ">Batal</a>
        </div>
    </div>
</div>