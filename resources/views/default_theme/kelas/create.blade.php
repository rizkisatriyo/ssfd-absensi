<div class="row">

    {{ Form::open(['url' => Route('grade.store'), 'method' => 'post']) }}

        <div class="input-field col s12">
            {{ Form::text('name', null, ['class' => 'validate']) }}
            {{ Form::label('name', 'Nama Kelas', ['class' => 'active']) }}
        </div>
        <div class="input-field col s12">
            {{ Form::submit('Simpan', ['class' => 'btn btn-primary btn-small waves-effect waves-light m-b-xs']) }}
        </div>

    {{ Form::close() }}

</div>