<div class="sidebar-profile">
    <div class="sidebar-profile-image">
        <img src="{{ asset('assets/images/profile-image.png') }}" class="circle" alt="">
    </div>
    <div class="sidebar-profile-info">

        <a>
            <p>{{ Auth::user()->name }}</p>
            <span>
                {{ Auth::user()->email }}
            </span>
        </a>

    </div>
</div>

<ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion" >
    <li class="no-padding {{ Request::segment(2) == '' ? 'active' : '' }}">
        <a class="waves-effect waves-grey active" href="{{ route('dashboard.index') }}">
            <i class="material-icons">home</i> Dashboard
        </a>
    </li>
    <li class="no-padding {{ Request::segment(2) == 'grade' || Request::segment(2) == 'student' ? 'active' : '' }}">
        <a class="collapsible-header waves-effect waves-grey {{ Request::segment(2) == 'grade' || Request::segment(2) == 'student' || Request::segment(2) == 'attedances' ? 'active' : '' }}">
            <i class="material-icons">dns</i>Data Master
            <i class="nav-drop-icon material-icons">keyboard_arrow_right</i>
        </a>
        <div class="collapsible-body">
            <ul>
                <li>
                    <a href="{{ Route('grade.index') }}">Data Kelas</a>
                </li>
                <li>
                    <a href="{{ route('student.index') }}">Data Siswa</a>
                </li>
            </ul>
        </div>
    </li>
    <li class="no-padding {{ Request::segment(2) == 'sms' ? 'active' : '' }}">
        <a class="waves-effect waves-grey active" href="{{ route('sms.index') }}">
            <i class="material-icons">email</i> Pesan
        </a>
    </li>
<!-- 
    <li class="no-padding {{ Request::segment(2) == 'attedances' ? 'active' : '' }}">
        <a class="waves-effect waves-grey active" href="{{ Route('attedances.index') }}">
            <i class="material-icons">fingerprint</i> Absensi
        </a>
    </li> -->

    <li class="no-padding {{ Request::segment(2) == 'report' ? 'active' : '' }}">
        <a href="{{ Route('attedances.index') }}">
            <i class="material-icons">fingerprint</i> Data Absensi
        </a>
    </li>

    <li class="no-padding {{ Request::segment(2) == 'report' ? 'active' : '' }}">
        <a class="waves-effect waves-grey active" href="{{ Route('report.index') }}">
            <i class="material-icons">receipt</i>Rekap Absensi
        </a>
    </li>

    <li class="no-padding {{ Request::segment(2) == 'pengaturan' ? 'active' : '' }}">
        <a class="collapsible-header waves-effect waves-grey {{ Request::segment(2) == 'pengaturan' ? 'active' : '' }}">
            <i class="material-icons">settings</i> Pengaturan
            <i class="nav-drop-icon material-icons">keyboard_arrow_right</i>
        </a>
        <div class="collapsible-body">
            <ul>
                <li>
                    <a href="{{ route('aplikasi.index') }}">Aplikasi</a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="{{ route('fingerprint.index') }}">Fingerprint</a>
                </li>
            </ul>
        </div>
    </li>
    <li class="no-padding">
        <a href="{{ Route('logout') }}" class="collapsible-header waves-effect waves-grey">
            <i class="material-icons">exit_to_app</i> Keluar
        </a>
    </li>
</ul>
<div class="footer">
    <p class="copyright"></p>
</div>