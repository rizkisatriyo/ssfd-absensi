<link rel="stylesheet" href="{{ asset('assets/plugins/materialize/css/materialize.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/material-preloader/css/materialPreloader.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/css/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables/css/dataTables.material.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/alpha.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/jquery.fancybox.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
<link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
<link href="//use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="">