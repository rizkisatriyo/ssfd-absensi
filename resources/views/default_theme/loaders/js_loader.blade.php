<!--
*
*	Javascript From Theme
*
-->
<script src="{{ asset('assets/plugins/jquery/jquery-2.2.0.min.js') }}"></script>
<script src="{{ asset('assets/plugins/materialize/js/materialize.min.js') }}"></script>
<script src="{{ asset('assets/plugins/material-preloader/js/materialPreloader.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-blockui/jquery.blockui.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/peity/jquery.peity.min.js') }}"></script>
<script src="{{ asset('assets/js/alpha.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('assets/js/pages/form-select2.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/Select-1.2.5/js/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/Buttons-1.5.1/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/Buttons-1.5.1/js/buttons.html5.min.js') }}"></script>
<!--
*
*	External Javascript
*
-->
<script src="//js.pusher.com/4.1/pusher.min.js"></script>
<!--
*
*	Javascript Custom
*
-->
<script type="text/javascript">
	var base_url 		= "{{ url('/') }}";					// Menetapkan Base URL
	var segment			= "/{{ Request::segment(1) }}/";	// Menetapkan Path Segment ke 1
</script>
<script src="{{ asset('assets/js/serializer.js') }}"></script>
<script src="{{ asset('assets/js/token_parser.js') }}"></script>
<script src="{{ asset('assets/js/notify.js') }}"></script>
<script src="{{ asset('assets/js/get_manifest.js') }}"></script>
<script src="{{ asset('assets/js/pusher-config.js') }}"></script>
<script src="{{ asset('assets/js/sms.js') }}"></script>
<script type="text/javascript">
	var ajaxCall;
</script>
@yield('scripts')