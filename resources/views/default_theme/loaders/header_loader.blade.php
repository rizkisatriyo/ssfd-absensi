<div class="nav-wrapper row">

    <section class="material-design-hamburger navigation-toggle">
        <a href="javascript:void(0)" data-activates="slide-out" class="show-on-large material-design-hamburger__icon reverse-icon">
            <span class="material-design-hamburger__layer material-design-hamburger__icon--from-arrow">
            </span>
        </a>
    </section>

    <div class="header-title col s3 m3">      
        <span class="chapter-title hide-on-small-only"></span>
    </div>

    <ul class="right col s9 m3 nav-right-menu">
        <li class="hide-on-small-and-down">
            <a href="#" id="chat-button" data-activates="chat-sidebar" class="chat-button show-on-large">
                <i class="material-icons">message</i>
                <span class="badge" id="badge-pesan">0</span>
            </a>
        </li>
<!--         <li class="hide-on-small-and-down">
            <a href="javascript:void(0)" data-activates="dropdown1" class="dropdown-button dropdown-right show-on-large" style="">
                <i class="material-icons">notifications_none</i>
                <span class="badge">4</span>
            </a>
            <ul id="dropdown1" class="dropdown-content notifications-dropdown" style="width: 46px; position: absolute; top: 0px; left: 972.75px; opacity: 1; display: none;">
                <li class="notificatoins-dropdown-container">
                    <ul>
                        <li class="notification-drop-title">Today</li>
                        <li>
                            <a href="#!">
                                <div class="notification">
                                    <div class="notification-icon circle cyan">
                                        <i class="material-icons">done</i>
                                    </div>
                                    <div class="notification-text">
                                        <p><b>Alan Grey</b> uploaded new theme</p>
                                        <span>7 min ago</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li> -->
    </ul>
</div>