<li>
    Button <small><i class="material-icons">add</i></small> untuk menambahkan data baru.
</li>
 <li>
    Button <small><i class="material-icons">edit</i></small> untuk merubah / update data.
</li>
 <li>
    Button <small><i class="material-icons">info</i></small> untuk melihat detail data.
</li>
<li>
	Button <small><i class="material-icons">delete</i></small> untuk menghapus data.
</li>