var table = jQuery('.dataTableAttendances').DataTable({
    dom         : 'Bfrtip',
    select      : true,
    ordering    : false,
    processing  : true,
    serverSide  : true,
    buttons     : [],
    columns     :
    [
        {
            data        : 'name',
            name        : 'name',
        },
        {
            data        : 'kelas',
            name        : 'kelas',
        },
        {
            data        : 'hadir',
            name        : 'hadir',
        },
        {
            data        : 'sakit',
            name        : 'sakit',
        },
        {
            data        : 'terlambat',
            name        : 'terlambat',
        },
        {
            data        : 'alfa',
            name        : 'alfa',
        },
        {
            data        : 'izin',
            name        : 'izin',
        },
        {
            data        : 'pulang',
            name        : 'pulang',
        }
    ],
    ajax        :
    {
        url 	: base_url + segment + 'api/attedances'
    }
});

attendances_new_channel.bind('App\\Events\\AttendancesReceive', function(e)
{
    table.ajax.reload();
});