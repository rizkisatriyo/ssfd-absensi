Pusher.logToConsole = true;
var pusher = new Pusher('593a18374af6cda615d4',
{
    cluster: 'ap1',
    encrypted: false
}),
sms_inbox_channel 	= pusher.subscribe('sms_inbox_channel'),
sms_inbox_event   	= pusher.subscribe('sms_inbox_event'),
sms_new_channel   	= pusher.subscribe('sms_new_channel'),
sms_new_event   	= pusher.subscribe('sms_new_event'),

attendances_new_channel = pusher.subscribe('attendances_new_channel'),
attendances_new_event	= pusher.subscribe('attendances_new_event');