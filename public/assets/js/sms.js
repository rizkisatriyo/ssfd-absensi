var ajaxCall;
var	msgLoading = '<i class="fa fa-spinner fa-spin left"></i> Mengirim pesan ...';
/*
* 	=== [ REAPLCE HTNL MESSAGE WITH REGEX ] ===
*/
function fck_html(str)
{
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
/*
* 	=== [ GET CREDIT SMS ] ===
*/
function get_credit()
{
	jQuery.ajax(
	{

		url 		: base_url + segment + 'api/sms.credit',
		dataType	: 'html',
		beforeSend	: function()
		{
			jQuery('#credit-sms-counter').html('<i class="fa fa-spinner fa-spin"></i>');
		},
		success		: function(responses)
		{

			var parser = jQuery.parseJSON(responses);
			jQuery('#credit-sms-counter').html(parser.credit);
			jQuery('#credit-sms-expire').html(parser.expired);

		},
		error 		: function()
		{
			jQuery('#credit-sms-counter').html('Error mengambil data.');
		}

	});
}
/*
* 	=== [ MASS SEND SMS WITHOUT LOOPING ] ===
*	parameter to disertai dengan number karena tipe data ini dalam bentuk array
* 	parameter message adalah parameter pesan yang dikirim
*	parameter record adalah jumlah nomor tujuan
* 	parameter number adalah nomor bagian pengiriman dan akan bertambah setelah pengiriman selesai dan tidak
	boleh melebihi atau sama dengan jumlah (record parameter)
*/
function sendSms(to,message,record,number)
{
	ajaxCall = jQuery.ajax(
	{
		url 		: base_url + segment + 'api/sms.send',
		dataType	: 'HTML',
		type 		: 'POST',
		data 		: 'to=' + to[number]  +'&message=' + message,
		headers		: _token(),
		beforeSend	: function()
		{
			notify('Mengirim ke nomor ' + to[number], 1000);
		},
		error 		: function()
		{
			notify('Gagal mengirim ke nomor ' + to[number], 1000);
		},
		success		: function(responses)
		{

			var parser = jQuery.parseJSON(responses);

			if(parser.status == 1)
			{
				notify('Berhasil mengirim sms ke nomor ' + parser.to, 4000);
			} else
			{
				notify(parser.text, 4000);
			}

			number++;

			if(number < record)
			{

				setTimeout(function()
				{
					sendSms(to,message,record,number);
				}, 4000);

			} else
			{
				jQuery('#SendSMSBtn').removeAttr('disabled');
				jQuery('#SendSMSBtn').html('Kirim');
			}

		}
	});

}
/*
* 	=== [ EVENT BUTTON KIRIM SMS ] ===
*/
jQuery('#SendSMSBtn').click(function()
{

	var to 			= jQuery('.js-example-tokenizer').val(), /* Tipe data array */
		message 	= jQuery('#pesan').val(); /* Mengambil isi pesan */

	if(to != null) /* Penegcekan jika nomor tujuan kosong atau tidak di isi */
	{
		jQuery('#SendSMSBtn').attr('disabled','disabled'); /* Menambahakan atribut disable ke button kirim sms */
		jQuery('#SendSMSBtn').html(msgLoading); /* Ubah text button */
		sendSms(to,message,to.length,0); /* Kirim prameter ke fungsi kirim sms */
	} else
	{
		notify('Masukan nomor hp!', 4000); /* Munculkan notifikasi jika tidak mengisi nomor hp */
	}

});
/*
* 	=== [ EVENT BUTTON MENAMPILKAN KOTAK MASUK ] ===
*/
jQuery('.BtnShowInbox').click(function(e)
{

	var datefrom 	= jQuery('input[name=inbox_datefrom]').val(),
		dateto 		= jQuery('input[name=inbox_dateto]').val(),
		FormData 	= serializer(e, '#form_get_inbox');

	jQuery('.inbox_dateFromMark').html(datefrom);
	jQuery('.inbox_dateToMark').html(dateto);

	if(datefrom != null && dateto != null)
	{

		jQuery.ajax(
		{
			url 		: base_url + segment + 'api/sms.inbox',
			dataType	: 'HTML',
			type 		: 'POST',
			data 		: FormData,
			headers		: _token(),
			beforeSend	: function()
			{
				notify('Mengambil data ... ', 1000);
				jQuery(".inbox_tbody").html('<tr><td colspan="4"><center>Mengambil data ...</center></td></tr>');
			},
			error 		: function()
			{
				notify('Gagal mengambil data ', 1000);
				jQuery(".inbox_tbody").html('<tr><td colspan="4"><center>Gagal mengambil data ...</center></td></tr>');
			},
			success		: function(responses)
			{
				jQuery(".inbox_tbody").html('');
				var parser = jQuery.parseJSON(responses);
				if(parser.status == undefined)
				{
					var no = 1;
					for(var i = 0; i < parser.msg.length; i++)
					{
						var x = jQuery('<tr>')
								.html(
									'<td>' + no + '</td>' +
									'<td>' + parser.msg[i].dari + '</td>' +
									'<td>' + fck_html(parser.msg[i].isiPesan) + '</td>' +
									'<td>' + parser.msg[i].date + '</td>');
						jQuery(".inbox_tbody").append(x);
					no++;
					}
				} else
				{
					jQuery(".inbox_tbody").html('<tr><td colspan="4"><center>Pesan kosong.</center></td></tr>');
				}

			}
		});

	} else
	{
		notify('Pilih tanggal terlebih dahulu!', 4000); /* Munculkan notifikasi */
	}

});
/*
* 	=== [ EVENT BUTTON MENAMPILKAN KOTAK KELUAR ] ===
*/
jQuery('.BtnShowOutbox').click(function(e)
{

	var datefrom 	= jQuery('input[name=outbox_datefrom]').val(),
		dateto 		= jQuery('input[name=outbox_dateto]').val(),
		FormData 	= serializer(e, '#form_get_outbox');

	jQuery('.outbox_dateFromMark').html(datefrom);
	jQuery('.outbox_dateToMark').html(dateto);

	if(datefrom != null && dateto != null)
	{

		jQuery.ajax(
		{
			url 		: base_url + segment + 'api/sms.outbox',
			dataType	: 'HTML',
			type 		: 'POST',
			data 		: FormData,
			headers		: _token(),
			beforeSend	: function()
			{
				notify('Mengambil data ... ', 1000);
				jQuery(".outbox_tbody").html('<tr><td colspan="5"><center>Mengambil data ...</center></td></tr>');
			},
			error 		: function()
			{
				notify('Gagal mengambil data ', 1000);
				jQuery(".outbox_tbody").html('<tr><td colspan="5"><center>Gagal mengambil data ...</center></td></tr>');
			},
			success		: function(responses)
			{
				jQuery(".outbox_tbody").html('');
				var parser = jQuery.parseJSON(responses);
				if(parser.status == undefined)
				{
					var no = 1;
					for(var i = 0; i < parser.msg.length; i++)
					{
						var x = jQuery('<tr>')
								.html(
									'<td>' + no + '</td>' +
									'<td>' + parser.msg[i].noTujuan + '</td>' +
									'<td>' + fck_html(parser.msg[i].isiPesan) + '</td>' +
									'<td>' + parser.msg[i].date + '</td>' +
									'<td>' + parser.msg[i].msg_status + '</td>');
						jQuery(".outbox_tbody").append(x);
					no++;
					}
				} else
				{
					jQuery(".outbox_tbody").html('<tr><td colspan="5"><center>Pesan kosong.</center></td></tr>');
				}

			}
		});

	} else
	{
		notify('Pilih tanggal terlebih dahulu!', 4000); /* Munculkan notifikasi */
	}

});
/*
* 	=== [ GET NEW MESSAGE ] ===
*/
function get_new_message()
{
	jQuery.get(base_url + segment + 'api/sms.new', function(responses)
	{
		jQuery('.chat-list').html('');
		jQuery('#badge-pesan').html(responses.length);
		for(var i = 0; i < responses.length; i++)
		{
			var rsplit = responses[i].split('|'),
				x = jQuery('<a href="javascript:void(0)" class="chat-message">')
				.html(
					'<div class="chat-item">' +
						'<div class="chat-item-image">' +
							'<img src="' + base_url + '/assets/images/profile-image-2.png" class="circle" alt="">' +
						'</div>' +
						'<div class="chat-item-info">' +
							'<p class="chat-name">' + rsplit[1] + '</p>' +
							'<small>' + rsplit[3] + '</small><p>' +
							'<span class="chat-message">' + rsplit[2] + '</span>' +
						'</div>' +
					'</div>'
				);
			jQuery(".chat-list").append(x);
		}
	});
}
/*
* 	=== [ CLEAR SMS TEMPORARY ] ===
*/
jQuery("#clear_temp_sms").click(function()
{
	jQuery.get(base_url + segment + 'api/sms.del_temp_sms', function(responses)
	{
		get_new_message();
		notify('Temporary pesan telah dihapus ', 1000);
	});
});
/*
* 	=== [ GET LOG UNREAD SMS WITH PUSHER & SCHEDULE EVENT App\\Events\\ReceiveMessagesEvent ] ===
*/
sms_new_channel.bind('App\\Events\\ReceiveMessagesEvent', function(e)
{
	get_new_message();
    console.log(e.message);
});
get_new_message();