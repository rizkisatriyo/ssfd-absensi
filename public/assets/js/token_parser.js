function _token()
{
	return { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') };
}