var btn_mutasi = jQuery('.btn-mutasi');

jQuery('#tab_mutasi').click(function()
{
    f_class.html('<option value="" selected>- Pilih Kelas -</option>');
    t_class.html('<option value="" selected>- Pilih Kelas -</option><option value="alumni">Alumni</option>');
    get_class();
});

btn_mutasi.click(function()
{
    if(f_class.val() != '' && t_class.val() != '')
    {
        jQuery.ajax(
        {
            url         : base_url + segment + 'student/mutasi.student',
            dataType    : 'JSON',
            type        : 'POST',
            data        : 'f_class='+ f_class.val() +
                          '&t_class=' + t_class.val(),
            headers     : _token(),
            beforeSend  : function()
            {
                notify('Memproses...', 2000);
                btn_mutasi.attr('disabled','disabled');
                btn_mutasi.html('<i class="fa fa-spinner fa-spin left"></i> Memproses...');
            },
            error       : function()
            {
                notify('Error silakan coba lagi!',2000);
                btn_mutasi.removeAttr('disabled');
                btn_mutasi.html('Mutasi');
            },
            success     : function(responses)
            {
                btn_mutasi.removeAttr('disabled');
                btn_mutasi.html('Mutasi');
                notify(responses.msg,2000);
            }
        });
    } else
    {
        notify('Silakan tentukan kelas terlebih dahulu!',2000);
    }
});