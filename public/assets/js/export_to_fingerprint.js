/*
*
*
*
*   INIT VARIABLES
* 
*
*
*/ 
var kelas           = jQuery('#kelas'),
    mesin           = jQuery('#mesin'),
    list_student    = jQuery('#list_student'),
    tab             = jQuery('#export'),
    btn_export      = jQuery('.export_to_finger'),
    tab_export      = jQuery('#tab_export'),
    f_class         = jQuery('#f_class'),
    t_class         = jQuery('#t_class'),
    btn_abort       = jQuery('.btn_abort'),
    x;
/*
*
*
*
*   MENGAMBIL DATA KELAS
* 
*
*
*/ 
function get_class()
{
    jQuery.ajax(
    {
        url         : base_url + segment + 'api/grade',
        dataType    : 'JSON',
        type        : 'GET',
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            kelas.html('<option value="" selected>- Pilih Kelas - </option><option value="all">Semua Kelas</option>');
            for (i = 0; i < responses.length; i++)
            {
                kelas.append(jQuery('<option value="' + responses[i].id + '">').html(responses[i].name));
                f_class.append(jQuery('<option value="' + responses[i].id + '">').html(responses[i].name));
                t_class.append(jQuery('<option value="' + responses[i].id + '">').html(responses[i].name));
            }
        }
    });
}
/*
*
*
*
*   MENGAMBIL DATA DAFTAR MESIN
* 
*
*
*/ 
function get_machines()
{
    jQuery.ajax(
    {
        url         : base_url + segment + 'api/fingerprint',
        dataType    : 'JSON',
        type        : 'GET',
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {

            mesin.html('<option value="" selected>- Pilih Mesin -</option>');

            for (i = 0; i < responses.data.length; i++)
            {
                x = jQuery('<option value="' + responses.data[i].user_id + '|' +
                            responses.data[i].password + '|' +
                            responses.data[i].ip_address + '">').html(responses.data[i].name);
                mesin.append(x);
            }
        }
    });
}
/*
*
*
*
*   CLICK EVENT TAB EXPORT
* 
*
*
*/ 
tab_export.click( function()
{
    if(tab.attr('style') == 'display: none;')
    {
        setTimeout(function()
        {
            get_class();
        }, 1000);
        setTimeout(function()
        {
            get_machines();
        }, 500);
    }

});
/*
*
*
*
*   SELECT OR CHANGE EVENT KELAS
* 
*
*
*/ 
kelas.change(function()
{
    list_student.html('');
     jQuery('.tbody_siswa').html('');
    jQuery.ajax(
    {
        url         : base_url + segment + 'api/student.grade/' + kelas.val(),
        dataType    : 'JSON',
        type        : 'GET',
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            no = 1;
            for (i = 0; i < responses.length; i++)
            {
                list_student.append(responses[i].pin + '|' + responses[i].name + "\n");
                var x = jQuery('<tr>').html('<td class="center-align">' + no + '</td><td>' + responses[i].name + '</td>');
                jQuery('.tbody_siswa').append(x);
            no++;
            }
        }
    });
});
/*
*
*
*
*   FUNCTION EXPORT
* 
*
*
*/
function exports_to_finger(data_siswa,data_mesin,number,record)
{

    var ds = data_siswa[number].split('|'),
        dm = data_mesin.split('|');

    ajaxCall = jQuery.ajax(
    {
        url         : base_url + segment + 'export.student',
        dataType    : 'JSON',
        type        : 'POST',
        data        : 'id_siswa='+ ds[0] +
                      '&nama_siswa=' + ds[1] +
                      '&username=' + dm[0] +
                      '&userpwd=' + dm[1] +
                      '&ip_address=' + dm[2],
        headers     : _token(),
        error       : function()
        {
           notify('Error silakan coba lagi!',2000);
           btn_export.removeAttr('disabled');
           btn_export.html('Export');
        },
        success     : function(responses)
        {

            notify(responses.message, 2000);
            number++;

            if(number < record)
            {

                setTimeout(function()
                {
                    exports_to_finger(data_siswa,data_mesin,number,record);
                },100);

            } else
            {
                btn_export.removeAttr('disabled');
                btn_export.html('Export');
            }

        }
    });
}
/*
*
*
*
*   BUTTON EVENT EXPORT
* 
*
*
*/
btn_export.click(function()
{

    if(mesin.val() != '' || list_student.val() != '')
    {
        list    = list_student.val().split("\n");
        total   = list.length - 1;
        btn_export.attr('disabled','disabled');
        btn_export.html('<i class="fa fa-spinner fa-spin left"></i> Memproses...');
        notify('Memproses...', 4000);
        exports_to_finger(list,mesin.val(),0,total);
    } else
    {
        notify('Tidak ada data yang dikirim.', 4000);
    }
});
/*
*
*
*
*   BUTTON ABORT EVENT
* 
*
*
*/
btn_abort.click(function(){
    ajaxCall.abort();
})