var page = 1;
var url = "{{ Route('grade.index') }}";
var current_page = 1;
var total_page = 0;
var is_ajax_fire = 0;
//manageData();


/* manage data list 
function manageData() {
    $.ajax({
        dataType: 'json',
        url: url,
        data: {page:page}
    }).done(function(data){
        total_page = data.last_page;
        current_page = data.current_page;
        $('#pagination').twbsPagination({
           totalPages: total_page,
            visiblePages: current_page,
            onPageClick: function (event, pageL) {
                page = pageL;
                if(is_ajax_fire != 0){
                  getPageData();
                }
            }
        });
        manageRow(data.data);
        is_ajax_fire = 1;
    });
}
$.ajaxSetup({
    headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});*/

/* Get Page Data*/
function getPageData() {
    $.ajax({
        dataType: 'json',
        url: url,
        data: {page:page}
    }).done(function(data){
        manageRow(data.data);
    });
}

/* Add new Post table row */
function manageRow(data) {
    var rows = '';
    $.each( data, function( key, value ) {
        rows = rows + '<tr>';
        rows = rows + '<td>'+value.name+'</td>';
        rows = rows + '<td data-id="'+value.id+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-kelas" class="cyan darken-1 modal-trigger edit-kelas">Edit</button> ';
        rows = rows + '<a class="waves-effect waves-red btn-flat remove-kelas">Delete</button>';
        rows = rows + '</td>';
        rows = rows + '</tr>';
    });
    $("tbody").html(rows);
}

/* Create new Kelas (Tambah data)*/
$(".crud").click(function(e){
    e.preventDefault();
    var form_action = $("#create-kelas").find("form").attr("action");
    var name = $("#create-kelas").find("input[name='name']").val();
    //var details = $("#create-kelas").find("textarea[name='details']").val();
    $.ajax({
        dataType: 'json',
        type:'POST',
        url: form_action,
        data:{name:name}
    }).done(function(data){
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Post Created Successfully.', 'Success Alert', {timeOut: 5000});
    });
});

/* Remove Kelas (Hapus) */
$("body").on("click",".remove-kelas",function(){
    var id = $(this).parent("td").data('id');
    var c_obj = $(this).parents("tr");
    $.ajax({
        dataType: 'json',
        type:'delete',
        url: url + '/' + id,
    }).done(function(data){
        c_obj.remove();
        toastr.success('Class Deleted Successfully.', 'Success Alert', {timeOut: 5000});
        getPageData();
    });
});

/* Edit Kelas */
$("body").on("click",".edit-kelas",function(){
    var id = $(this).parent("td").data('id');
    var name = $(this).parent("td").prev("td").prev("td").text();
    //var details = $(this).parent("td").prev("td").text();
    $("#edit-kelas").find("input[name='name']").val(name);
    //$("#edit-kelas").find("textarea[name='details']").val(details);
    $("#edit-kelas").find("form").attr("action",url + '/' + id);

});

/* Updated  Kelas baru (Updated new Kelas) */
$(".crud-edit").click(function(e){
    e.preventDefault();
    var form_action = $("#edit-item").find("form").attr("action");
    var title = $("#edit-item").find("input[name='title']").val();
    var details = $("#edit-item").find("textarea[name='details']").val();
    $.ajax({
        dataType: 'json',
        type:'PUT',
        url: form_action,
        data:{title:title, details:details}
    }).done(function(data){
        getPageData();
        $(".modal").modal('hide');
        toastr.success('Post Updated Successfully.', 'Success Alert', {timeOut: 5000});
    });
});