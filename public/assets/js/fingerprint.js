var table = jQuery('.dataTableFingerprint').DataTable({
    dom         : 'Bfrtip',
    select      : true,
    ordering    : false,
    processing  : true,
    serverSide  : true,
    buttons     :
    [
        {
            text        : '<i class="material-icons">add</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light add_title',
            action      : function()
            {
                // Ganti title pada modal
                jQuery('#modal_header').html('Tambah data');
                // Ambil form tambah data dengan xhr request
                jQuery.get(base_url + segment +  'pengaturan/fingerprint/create', function(response)
                {
                    jQuery('#modal_content').html(response);
                });
                // Open modal
                jQuery('.modal').openModal();
            }
        },
        {
            text        : '<i class="material-icons">edit</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light update_title',
            action      : function(e, dt, node, config)
            {
                var res    =   dt.row( { selected: true } ).data();
                if(res != undefined)
                {
                    // Ganti title pada modal
                    jQuery('#modal_header').html('Update data');
                    // Ambil form edit data dengan xhr request
                    jQuery.get(base_url + segment +  'pengaturan/fingerprint/' + res.id + '/edit', function(response)
                    {
                        jQuery('#modal_content').html(response);
                    });
                    // Open modal
                    jQuery('.modal').openModal();
                } else
                {
                    notify('Silakan pilih data terlebih dahulu!',4000);
                }
            }
        },
        {
            text        : '<i class="material-icons">info</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light info_title',
            action      : function(e, dt, node, config)
            {
                var res    =   dt.row( { selected: true } ).data();
                if(res != undefined)
                {
                    // Ganti title pada modal
                    jQuery('#modal_header').html('Info Lengkap');
                    jQuery('#modal_content').html('<center><i class="fa fa-spinner fa-spin"></i> Memproses ...</center>');
                    // Ambil form show data dengan xhr request
                    jQuery.get(base_url + segment +  'check/fingerprint/' + res.id, function(response)
                    {
                        jQuery('#modal_content').html(response);
                    });
                    // Open modal
                    jQuery('.modal').openModal();
                } else
                {
                    notify('Silakan pilih data terlebih dahulu!',4000);
                }
            }
        },
        {
            text        : '<i class="material-icons">history</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light delete_title',
            action      : function(e, dt, node, config)
            {
                var res    =   dt.row( { selected: true } ).data();

                if(res != undefined)
                {
                    // Ganti title pada modal
                    jQuery('#modal_header').html('Hapus log transaksi');
                    var tpl =   '<center data-id="' + res.id + '" id="conf">Apakah anda yakin, semua log akan dihapus?<br><br>' +
                                '<button class="btn btn-primary btn-small waves-effect waves-light" id="delete_log">' +
                                    'Hapus' +
                                '</button></center>';
                    jQuery('#modal_content').html(tpl);
                    // Open modal
                    jQuery('.modal').openModal();

                } else
                {
                    notify('Silakan pilih data terlebih dahulu!',4000);
                }
            }
        },
        {
            text        : '<i class="material-icons">storage</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light backup_title',
            action      : function(e, dt, node, config)
            {
                var res    =   dt.row( { selected: true } ).data();
                if(res != undefined)
                {
                    window.open(base_url + segment + 'backup.finger/' + res.id, '_blank');
                } else
                {
                    notify('Silakan pilih data terlebih dahulu!',4000);
                }
            }
        },
        {
            text        : '<i class="material-icons">power_settings_new</i>',
            className   : 'btn-floating btn-primary btn-small waves-effect waves-light reboot_title',
            action      : function(e, dt, node, config)
            {
                var res    =   dt.row( { selected: true } ).data();
                if(res != undefined)
                {
                    reboot_finger(res.id);
                } else
                {
                    notify('Silakan pilih data terlebih dahulu!',4000);
                }
            }
        }
    ],
    columns     :
    [
        {
            data        : 'id',
            name        : 'id',
            visible     :  false
        },
        {
            data        : 'name',
            name        : 'name',
            className   : 'centered'
        },
        {
            data        : 'ip_address',
            name        : 'ip_address',
        },
        {
            data        : 'com_key',
            name        : 'com_key',
        }
    ],
    ajax        :
    {
            url         : base_url + segment + 'api/fingerprint'
    }
});
table.on('select', function ()
{
    var selectedRows = table.rows( { selected: true } ).count();
    table.button( 1 ).enable( selectedRows == 1);
    table.button( 2 ).enable( selectedRows == 1);
    table.button( 3 ).enable( selectedRows == 1);
    table.button( 4 ).enable( selectedRows == 1);
});
table.on('draw.dt', function ()
{
    jQuery('.backup_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Backup data user pada mesin'
    });
    jQuery('.delete_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Hapus semua log transaksi'
    });
    jQuery('.info_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Lihat informasi mesin finger'
    });
    jQuery('.update_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Update mesin finger'
    });
    jQuery('.add_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Tambah mesin finger baru'
    });
    jQuery('.reboot_title').tooltip(
    {
        delay   : 50,
        position : 'top',
        tooltip : 'Boot ulang'
    });

});
/*
*
*
*
*   BUTTON ADD EVENT
* 
*
*
*/ 
jQuery(document).on('click','#button_add_fingerprint', function(e)
{
    var FormData = serializer(e, '#form_add_fingerprint');
    jQuery.ajax(
    {
        url         : base_url + segment + 'pengaturan/fingerprint',
        dataType    : 'HTML',
        type        : 'POST',
        data        : FormData,
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
            notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            table.ajax.reload();
            var parser = jQuery.parseJSON(responses);
            if(parser.error == 1)
            {
                for(var i = 0; i < parser.results.length; i++)
                {
                    notify(parser.results[i],4000);
                }
            } else
            {
                jQuery('.modal').closeModal();
                notify('Data berhasil disimpan!',4000);
            }

        }
    });

});
/*
*
*
*
*   BUTTON UPDATE EVENT
* 
*
*
*/ 
jQuery(document).on('click','#button_update_fingerprint', function(e)
{
    var FormData    = serializer(e, '#form_update_fingerprint');
    var id          = jQuery('#id').val();
    jQuery.ajax(
    {
        url         : base_url + segment + 'pengaturan/fingerprint/' + id,
        dataType    : 'HTML',
        type        : 'PUT',
        data        : FormData,
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            table.ajax.reload();
            var parser = jQuery.parseJSON(responses);
            if(parser.error == 1)
            {
                for(var i = 0; i < parser.results.length; i++)
                {
                    notify(parser.results[i],4000);
                }
            } else
            {
                jQuery('.modal').closeModal();
                notify('Data berhasil diperbarui!',4000);
            }
        }
    });

});
/*
*
*
*
*   BUTTON DELETE EVENT
* 
*
*
*/ 
jQuery(document).on('click','#button_delete_fingerprint', function(e)
{
    var id = jQuery('#preview').attr('data-id');
    jQuery.ajax(
    {
        url         : base_url + segment + 'pengaturan/fingerprint/' + parseInt(id),
        dataType    : 'HTML',
        type        : 'DELETE',
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            jQuery('.modal').closeModal();
            table.ajax.reload();
            var parser = jQuery.parseJSON(responses);
            if(parser.error == 1)
            {
                notify(parser.results,4000);
            } else
            {
                notify('Data berhasil dihapus!',4000);
            }

        }
    });

});
/*
*
*
*
*   BUTTON DELETE LOG EVENT
* 
*
*
*/
jQuery(document).on('click','#delete_log', function(e)
{
    var id = jQuery('#conf').attr('data-id');
    jQuery.ajax(
    {
        url         : base_url + segment + 'delete.log/' + parseInt(id),
        dataType    : 'JSON',
        type        : 'GET',
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            jQuery('.modal').closeModal();
            msg = responses.message;
            if(msg == '')
            {
                msg = 'kunci mesin salah, data log gagal dihapus.';
            }
            notify(msg, 4000);
        }
    });

});
/*
*
*
*
*   BUTTON REBOOT FINGERPRINT EVENT
* 
*
*
*/
function reboot_finger(param)
{
    jQuery.ajax(
    {
        url         : base_url + segment + 'reboot.finger/' + param,
        dataType    : 'JSON',
        type        : 'GET',
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
           notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            notify(responses.message, 4000);
        }
    });
}