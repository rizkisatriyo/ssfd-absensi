/*
*
*   === [ INJECT TO DATATABLE ELEMENT ] ===
*/
var addData  =  '<button class="btn-floating btn-primary btn-small waves-effect waves-light" id="GradeAdd">' +
                    '<i class="material-icons">add</i>' +
                '</button>';

jQuery('.dataTables_length').html(addData);
jQuery('#GradeAdd').click(function()
{
    // Ganti title pada modal
    jQuery('#modal_header').html('Tambah data');

    // Ambil form edit data dengan xhr request
    jQuery.get(base_url + segment + 'student/create', function(response)
    {
        jQuery('#modal_content').html(response);
    });

    // Open modal
    jQuery('.modal').openModal();
});

jQuery(document.body).on('click', '#editData', function()
{

    var data_inject =   jQuery(this).parents('.data-inject'),
        valueData   =   data_inject.attr('data-id'),
        route       =   data_inject.attr('route-edit');

    // Ganti title pada modal
    jQuery('#modal_header').html('Update Data');

    // Ambil form edit data dengan xhr request
    jQuery.get(route, function(response)
    {
        jQuery('#modal_content').html(response);
    });

    // // Open modal
    jQuery('.modal').openModal();

});