var first_name          =   jQuery('.first_name'),
    middle_name         =   jQuery('.middle_name'),
    last_name           =   jQuery('.last_name'),
    in_clock            =   jQuery('.in_clock'),
    out_clock           =   jQuery('.out_clock'),
    short_name          =   jQuery('.short_name'),
    created_by          =   jQuery('.created_by'),
    bg                  =   jQuery('.background_color'),
    authors             =   jQuery('.authors'),
    favicon             =   jQuery('.favicon'),
    tosi                =   jQuery('.tosi'),
    toso                =   jQuery('.toso'),
    limit               =   jQuery('.limit'),
    sms_flog            =   jQuery('.sms_flog'),
    sms_notification    =   jQuery('.sms_notification'),
    read_finger_in      =   jQuery('.read_finger_in'),
    read_finger_out     =   jQuery('.read_finger_out'),
    message_format_out  =   jQuery('.message_format_out'),
    counter_notifier    =   jQuery('.counter_notifier'),
    message_format_in   =   jQuery('.message_format_in');

jQuery.ajax(
{
    url         : base_url + '/manifest.json',
    dataType    : 'JSON',
    type        : 'GET',
    beforeSend  : function()
    {
        notify('Memproses...', 500);
    },
    error       : function()
    {
       notify('Error silakan coba lagi!',500);
    },
    success     : function(responses)
    {
        first_name.attr('value',responses.first_name);
        middle_name.attr('value',responses.middle_name);
        last_name.attr('value',responses.last_name);
        in_clock.attr('value',responses.in_clock);
        out_clock.attr('value',responses.out_clock);
        short_name.attr('value',responses.short_name);
        created_by.attr('value',responses.created_by);
        bg.attr('value',responses.background_color);
        authors.attr('value',responses.authors);
        favicon.attr('value',responses.favicon);
        sms_flog.attr('value',responses.sms_flog);
        tosi.attr('value',responses.time_off_schedular_in);
        toso.attr('value',responses.time_off_schedular_out);
        limit.attr('value',responses.limit);
        sms_notification.attr('value',responses.sms_notification);
        read_finger_in.attr('value',responses.read_finger_in);
        read_finger_out.attr('value',responses.read_finger_out);
        message_format_in.attr('value',responses.message_format_in);
        message_format_out.attr('value',responses.message_format_out);
        counter_notifier.attr('value',responses.counter_notifier);
    }
});

jQuery('#button_update_aplikasi').click(function(e)
{
    var FormData = serializer(e, '#form_update_aplikasi');
    jQuery.ajax(
    {
        url         : base_url + segment + 'pengaturan/aplikasi',
        dataType    : 'JSON',
        type        : 'POST',
        data        : FormData,
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 500);
        },
        error       : function()
        {
            notify('Error silakan coba lagi!',500);
        },
        success     : function(responses)
        {
            notify(responses.msg,500);
            get_manifest();
        }
    });
})