function update_siswa(param)
{
    jQuery.ajax(
    {
        url         : param.url,
        dataType    : 'JSON',
        type        : 'PUT',
        data        : param.senData,
        headers     : _token(),
        beforeSend  : function()
        {
            notify('Memproses...', 2000);
        },
        error       : function()
        {
            notify('Error silakan coba lagi!', 2000);
        },
        success     : function(responses)
        {
            if(responses.error == 1)
            {
                for(var i = 0; i < responses.results.length; i++) {
                    notify(responses.results[i],4000);
                }
            } else
            {
                jQuery('.modal').closeModal();
                notify('Data berhasil diperbarui!',4000);
            }
        }
    });
}