function get_manifest()
{
	jQuery.ajax(
	{
		url 		: base_url +'/manifest.json',
		dataType	: 'html',
		success		: function(responses)
		{

			var parser = jQuery.parseJSON(responses);
			jQuery('.chapter-title').html(parser.first_name + ' ' + parser.middle_name);
			jQuery('.direction').html(parser.first_name + ' ' + parser.middle_name + ' ' + parser.last_name);
			jQuery('.copyright').html(parser.created_by);

			jQuery('meta[name="author"]').attr('content', parser.authors);
			jQuery('link[rel="shortcut icon"]').attr('href', base_url + '/' + parser.favicon);

		}
	});
}
get_manifest();