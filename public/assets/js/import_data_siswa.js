var file_xls 	= 	jQuery('#file_xls');


function logger()
{
	jQuery('.error_log').attr('style','display:none;');
	jQuery('.error_tbody_log').html('');
}

function read_temp_file(param)
{
	logger();
	jQuery.get(base_url + '/' + param, function(res)
	{
		jQuery('.error_log').removeAttr('style');
		multiple_insert_data_siswa(res,0,res.length);
	});
}

function multiple_insert_data_siswa(data,number,record)
{
    jQuery.ajax(
    {
        url         : base_url + segment + 'student/import.student',
        dataType    : 'JSON',
        type        : 'POST',
        data        : data[number],
        headers     : _token(),
        error       : function()
        {
           notify('Error silakan coba lagi!',2000);
        },
        success     : function(res)
        {

        	number++;

    	    setTimeout(function()
    	    {

				if(res.error == 1)
	    		{
	    			var tr = jQuery('<tr>').html('<td>' + number + '</td><td>' + res.data_siswa + '</td><td>' + res.error_msg + '</td>');
	    			
	    		} else
	    		{
	    			var tr = jQuery('<tr>').html('<td>' + number + '</td><td>' + res.data_siswa + '</td><td>' + res.msg + '</td>');
	    		}

	    		jQuery('.error_tbody_log').append(tr);

    	    }, 100);

        	if(number < record)
        	{
        	    setTimeout(function()
        	    {
        	        multiple_insert_data_siswa(data,number,record);
        	    },100);

        	} else
        	{
        		notify('Proses imnport selesai.',2000);
        	}
        }
    });
}

jQuery(document.body).on('change','#file_xls', function(e)
{
	e.preventDefault();
	extension	=	file_xls.val().split('.').pop().toLowerCase();

	if(jQuery.inArray(extension, ['xls', 'xlsx']) == -1)
	{
		notify('Upload file hanya di ijinkan dari file excel.', 2000);
	} else
	{

		file_data	= 	file_xls.prop('files')[0];

		form_data	= 	new FormData();
		form_data.append('file', file_data);

		jQuery.ajax(
		{
		    url         : base_url + segment + 'student/render.file',
		    type        : 'POST',
		    data 		: form_data,
		    processData : false,
		    contentType : false,
		    dataType    : 'JSON',
		    headers     : _token(),
		    beforeSend  : function()
		    {
		        notify('Memproses...', 500);
		    },
		    error       : function()
		    {
		       notify('Error silakan coba lagi!',500);
		    },
		    success     : function(res)
		    {
		    	notify(res.msg,500);
		    	if(res.error == 0)
		    	{
		    		read_temp_file(res.temp_file);
		    	}
		    }
		});
	}

});

jQuery('#tab_import').click(function() { logger(); })