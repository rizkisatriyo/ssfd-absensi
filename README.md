<p>- Running Schedule With Artisan</p>
    php artisan schedule:run

<p>- Script Bat File For Cronjob On Windows</p>

	D:
	D:\xampp\php\php.exe artisan schedule:run 1>> NUL 2>&1

	<!-- Or -->

	START /min cmd.exe /k 
	D: && cd D:\xampp\htdocs\laravel\ssfd-absensi && php artisan schedule:run 1>> NUL 2>&1

	<!-- Or -->

	START /min cmd.exe /k "D: && cd D:\xampp\htdocs\laravel\ssfd-absensi && php artisan schedule:run" 1>> NUL 2>&1

<p>- Set Crontab On Linux</p>
	
	<!-- Create On Terminal -->
	crontab -e
	<!-- With Log File, This cron running every minute -->
	* * * * * php /opt/lampp/htdocs/ssfd-absensi/artisan schedule:run 1>> /opt/lampp/htdocs/ssfd-absensi/file.log

<p>- Listing Channel And Event Pusher</p>

	1. 	For Get Inbox
		- CHANNEL 	: 	sms_inbox_channel
		- EVENT 	:	sms_inbox_event
		- RUN 		: 	-

	2. 	For Get New SMS
		- CHANNEL 	: 	sms_new_channel
		- EVENT 	:	sms_new_event
		- RUN 		: 	php artisan command:receive_messages

	3. 	For Get SMS Today
		- CHANNEL 	: 	sms_today_channel
		- EVENT 	:	sms_today_channel
		- RUN 		: 	-

	4. 	For Get Absensi
		- CHANNEL 	: 	absensi_new_channel
		- EVENT 	:	absensi_new_event
		- RUN 		: 	-

<p>- If pusher error message response : <br>
	<code>Timestamp expired: Given timestamp (2018-02-17T15:39:38Z) not within 600s of server time (2018-02-17T17:09:21Z)</code>
</p>

<b>Mac/Linux/Unix :</b><br>

	To check the current machine time on your system, please run the date command from the console. Is the output correct?

	If not, you can correct the time on your machine by going to your console and using date to modify it. In most cases, you will need to give yourself sudo access on your machine.

	$ sudo date MMddhhmmyyyy

	Where:

	MM - Month (two-digit numeric month)
	dd - Day (two-digit numeric day i.e. day of month)
	hh - Hour
	mm - Minutes
	yy - Year

	So, for example, if we wanted to change the current time to 3:13pm on Sunday 16th June 2013, we would type the following:

	$ sudo date 061615132013

<b>Windows :</b><br>

	1. Set Date And Time
	2. Turn of computer

<p>- Pusher Binding From Javascript Example Script :</p>

	Pusher.logToConsole = true;
	var pusher = new Pusher('593a18374af6cda615d4',
	{
	    cluster: 'ap1',
	    encrypted: false
	}),
	sms_inbox_channel 	= pusher.subscribe('sms_inbox_channel'),
	sms_inbox_event   	= pusher.subscribe('sms_inbox_event'),
	sms_new_channel   	= pusher.subscribe('sms_new_channel'),
	sms_new_event   	= pusher.subscribe('sms_new_event'),
	absensi_new_channel = pusher.subscribe('absensi_new_channel'),
	absensi_new_event 	= pusher.subscribe('absensi_new_event');

	<!--
	* 		Event Binding
	*		" App\\Events\\ReceiveMessagesEvent " this is event name
	 -->
	sms_new_channel.bind('App\\Events\\ReceiveMessagesEvent', function(e)
	{
		get_new_message();
	    // console.log(e.message);
	});

	<!-- Channel Binding -->
	sms_new_channel.bind('sms_new_channel', function(e)
	{
		console.log(e);
	});

<p>- Remove Package :</p>

    Running the below command (most recommended way to remove your package without updating your other packages)

    $ composer remove vendor/package

    Go to your composer.json file and then run command like below it will remove your package (but it will also update your other packages)

    $ composer update

    Reference : https://stackoverflow.com/questions/23126562/how-to-remove-a-package-from-laravel-using-composer

<p>- Format Excel Import Data Siswa :</p>
<img src="https://i.imgur.com/Fxt66Cz.png" alt="Format Excel Import Data Siswa">

<p>- Contoh response mengambil data sidik jari</p>
	Array
	(
	    [0] => 
	    [1] => <Row><PIN>10000</PIN><FingerID>5</FingerID><Size>1222</Size><Valid>1</Valid><Template>TYNTUzIxAAAEwMcECAUHCc7QAAAcwWkBAAAAhG0qXsBBAI4PtQCFAJbPMwBiAHcPqQBgwIkPFwBqADIPvcB3AI8PFwBMAGvPIQCKAPcPQwCmwIgP8wCqAN0PisCuAIYP4wB7AJ7POgDAAPwPjwDGwHgPIQDEAKMPmMDJAIkPvQAJAJDPUwDSAGkPxgHWwB4PcgDiAKwPXcDiAGEPowA3AIvPRwD6AFYPsAD4wFUPAAH8AOUPecAFATwPDQDMAd7PRwANAUsPowAKwT0PeAAbAegPHcAdAVIPkADZASHPSgApATsP/wAvwUYPdgA1AeIOgsA9AawObQD4ATjOaQBIAcEO7wBPwUAPiwBMAdsOW8BYATQOnQCeAaTOIwpXGV+b6gZbz3IHfYNeg6YIoj56EysDfYMPiLrAsv5XC08L0XrWuOr4DIo+AJuDJcQjA0cjxYNf+9dHpPwl/xP2BIf6vlsKiYT9m2SXibw0ETYZFW/f7iM9pAf+9DL/twzaPkYExfFJCXBv/kanAxILlQqQF0rt0Pkd+n4Mp1eqy8r8bQ0K9IceSNuIGunvJgPWi/9P5euNErUCFIrHVqqS9fVtE/8QWdIg6jUC7fLY8lHK2HpCDSMIGPoGKKt5wfT98+MCMcwL/I4JvO+sfLGqCJBlhFVy7ReQv0SSkIcVjVyeEj0iCPPxFXREggI4IHUi+CYPuIaONoJ6T3jSbnIAJIIAAqQjZQXFqRDP/MP9CwBv1BBTAFlPCQCMEdX/xT7BVgkATBfVwFwAYQYAchwQkcEUwEcgD8H9e5lAd9IBLCIJ//8F/nsA/sH/wFHBwAAi7Ac4BQAOPDH9Pc0BXkIMRWAFQsTHAblDE1VU0AAWhPv+Qv9DVjr9xpL8wg4ACkjWxjKGwVb/BwASn/crggUALl+AxAUFBPZj/f7AwA3FcGLJQ1T/VcMXxQtwNEL/Pv9VTwVTYdYBD370RDj7wvr+wD5zEQAbQ/A88lA2VAgAJEz3xAD8/1UJAIZICTYA/sDBCwCCZInEtXRxCQABpxv9NAAxEABwjgn9SsQARcDAwBgAwqnpj0r+wf/9aztiO7gKAIqvhsC0wm4BFQA/u/o9BTtmPUvAwMDAGcUHuin+Tz5D+8IF/sQ/wP/A///BBQYEjsD6QMANAOHC7wD8OP/AwP2ABgT3w22HwwUAIsIakwYARsZ0wwTAh8MBIMhkwAfFTtW0xcB+EgBWFPf6PsMoOErAwJwKBJDXbcJ+ScHUAFkh8fvB/f7/gf9hAlELAFXiZwfAerD+BQBt4m0GmA7AofKWlcLDVw4Eufj9+8D9SgXAxKAGAHH6YMJi/w3AQPxawv/COnsNwEf+U/9+Z8AQcMBWxo8MEIHHCfj+wMH+cxgQyQXSgcD9/v9AS4RXYMoRfwgxwf8B/sYAw/8LEIcJ58FBok4HEEgRSaxvANBnEkOUGRDZGN6b/v7+Kf/+hf9XAMFbCBB8HuzAar0FEBsgU33MEJXgI8H+wH/B0BBX7MfA/v39/jn8+z/+/sDAwMAE/8Q9BBBJLUDBQAUUjiw3WQUQNutMxVQEEDwuRoTAEFzvO8GHBRB1/TDHPsIEEHBBLagDFKtNK8JSQgDOQwXAAQtFUgAAAAAAAA==</Template></Row>
	    [2] => 
	)

<p>- Delete presensi only</p>
	Pada mesin absensi fingerprint
	1. Kelola Data
	2. Hapus Data
	3. Hapus Data Presensi
	4. Hapus Semua / Berdasarkan Rentang Waktu

<p>- Yang belum :</p>
	1. Mutasi Siswa (Naik peringkat kelas berdasarkan kelas sebelumnya)
	2. Balas SMS ke orang tua siswa
	3. Waktu masuk & Keluar

<p>- Ubah Format Waktu / Time menjadi waktu 24 Jam pada komputer dengan format sebagai berikut :</p>
	Short Time : H:mm tt
	Long Time : H:mm:ss tt

	File Cookie di folder storage/logs/cookies.txt Jangan Di hapus
<code>
# Netscape HTTP Cookie File
# https://curl.haxx.se/docs/http-cookies.html
# This file was generated by libcurl! Edit at your own risk.

192.168.137.12	FALSE	/	FALSE	0	SessionID	1527182842	
</code>

# Backup data to fdisk harus format FAT32
