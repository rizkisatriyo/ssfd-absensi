<?php

Route::get('/', function ()
{

    return view('default_theme.login.login');

})->name('login')->middleware('guest');
/*
*
*   LOGOUT ROUTE
*
*/
Route::get('/logout',function()
{
    Auth::logout();
    return redirect('/');
})->name('logout');
/*
*
*   HOME / DASHBOARD ROUTE
*
*/
Route::get('/home','DashboardCtrl@index')->middleware('auth')->name('home');
/*
*
*   POST LOGIN ROUTE
*
*/
Route::post('/login','LoginCtrl@checkLogin')->name('login');
/*
*
*   SUPERADMIN GROUP ROUTE
*
*/
Route::group(['middleware' => ['auth','role:superadmin'],'prefix' => 'superadmin'], function() {
/*
*
*   ROUTE YANG BERHUBUNGAN DENGAN SMS CENTER
*
*/
    Route::resource('sms','Sms\Sms');
    Route::get('api/sms.del_temp_sms','Sms\Sms@del_temp_sms');
    Route::get('api/sms.credit','Sms\Sms@credit')->name('api.sms.credit');
    Route::post('api/sms.send','Sms\Sms@kirim_sms')->name('api.sms.send');
    Route::get('api/sms.new','Sms\Sms@new')->name('api.sms.new');
    Route::post('api/sms.inbox','Sms\Sms@inbox')->name('api.sms.inbox');
    Route::post('api/sms.outbox','Sms\Sms@outbox')->name('api.sms.outbox');
/*
*
*   ROUTE YANG BERHUBUNGAN DENGAN PENGATURAN MESIN FINGERPRINT
*
*/
    Route::resource('/pengaturan/fingerprint','Fingerprint\Fingerprint');
    Route::get('api/fingerprint','Fingerprint\Fingerprint@api')->name('api.fingerprint');
    Route::get('check/fingerprint/{id}','Fingerprint\FingerprintStatus@index');
    Route::get('delete.log/{id}','Fingerprint\DeleteLog@index');
    Route::get('backup.finger/{id}','Fingerprint\BackupData@index');
    Route::get('reboot.finger/{id}','Fingerprint\Reboot@index');
/*
*
*   ROUTE YANG BERHUBUNGAN DENGAN SISWA
*
*/
    Route::resource('student','StudentCtrl');
    Route::get('api/student.grade/{grade_id}','Siswa\ShowByGrade@index');
    Route::post('student/render.file','Siswa\RenderFile@index');
    Route::post('student/import.student','Siswa\RenderFile@import_to_db');
    Route::post('student/mutasi.student','Siswa\MutasiSiswa@index');
/*
*
*   ROUTE YANG BERHUBUNGAN DENGAN / KELAS
*
*/
    Route::resource('grade','GradeCtrl');
    Route::post('export.student','Siswa\ExportSiswaToFinger@index');
    Route::get('api/grade','GradeCtrl@apiGrade')->name('api.grade');

/*
*
*   ROUTE ABSENSI
*
*/
    Route::resource('attedances', 'Attedance');
    Route::get('api/attedances', 'Attedance@api');

/*
*
*   ROUTE REPORT
*
*/
    Route::get('/report', 'Report@index')->name('report.index');
    Route::post('/api/report', 'Report@api')->name('report.api');
    
/*
*
*   ROUTE INDEX / DASHBOARD
*
*/
    Route::get('/','DashboardCtrl@index')->name('dashboard.index');
/*
*
*   ROUTE YANG BERHUBUNGAN DENGAN SETTING APLIKASI
*
*/
    Route::resource('/pengaturan/aplikasi','Pengaturan\Aplikasi');
});
/*
*
*   DUMPTEST ROUTE
*
*/
Route::prefix('dumptest')->group(function()
{

	Route::get('/GetAttLog', 'DumpTest@GetAttLog');
	Route::get('/ClearData', 'DumpTest@ClearData');
	Route::get('/RestartMachines', 'DumpTest@RestartMachines');
	Route::get('/GetReport', 'DumpTest@GetReport');
	Route::get('/AddUser', 'DumpTest@AddUser');
	Route::get('/UserInfo', 'DumpTest@UserInfo');
    Route::get('/check', 'DumpTest@check');
    Route::get('/get_template', 'DumpTest@FingerTemplate');
    Route::get('/AddFinger', 'DumpTest@AddFinger');
    Route::get('/DeleteUser', 'DumpTest@DeleteUser');
    Route::get('/between_query_test', 'DumpTest@between_query_test');

});