-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2018 at 06:45 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ssfd_absensi`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade_id` int(10) UNSIGNED NOT NULL,
  `kelamin` enum('Laki-Laki','Perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tmplahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noimage.jpg',
  `kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_masuk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_keluar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmortu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp_ortu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nowa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id`, `nis`, `name`, `grade_id`, `kelamin`, `alamat`, `tmplahir`, `tgllahir`, `foto`, `kota`, `tahun_masuk`, `tahun_keluar`, `nohp`, `nmortu`, `nohp_ortu`, `nowa`, `created_at`, `updated_at`) VALUES
(1, '2782', 'ALIFAH SUKMA ASIH', 19, 'Perempuan', 'Ds. Leuwiseeng, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '0811230151', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(2, '2784', 'ALYA AULIA NUR MUHAMAD', 19, 'Perempuan', 'Ds. Jatiwangi, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221346983', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(3, '2785', 'ALYSHA KHAIRUNNISA', 19, 'Perempuan', 'Jl. Siti Armilah, Majalengka Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082130036452', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(4, '2801', 'CRISNA SATRIA FEBRIANA', 19, 'Laki-Laki', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082127173611', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(5, '2802', 'DAFFA FASYA SUMAWIJAYA', 19, 'Laki-Laki', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081380981152', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(6, '2827', 'FADLAN RESTU PRATAMA', 19, 'Laki-Laki', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081319193262', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(7, '2995', 'FAHMI NURDIN', 19, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, NULL, NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(8, '2829', 'FAJRIN NABILA HAPID', 19, 'Perempuan', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '08121441131', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(9, '2830', 'FARHAN ABDILLAH', 19, 'Laki-Laki', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, NULL, NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(10, '2838', 'FERI SUTIYAWAN', 19, 'Laki-Laki', 'Lingkungan Melati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '089661173076', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(11, '2842', 'GEULIS TIARA CUCU ELIAWATI', 19, 'Perempuan', 'Ds. Jengah, Jatigede, Sumedang', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081324461757', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(12, '2844', 'GIFFASYA AUFADHIA AGISTIYAS', 19, 'Perempuan', 'Ds. Kamun, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082318792888', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(13, '2857', 'IBNU SAHIBA', 19, 'Laki-Laki', 'Majalengka Wetan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081956229762', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(14, '2863', 'IQBAL SYAHRIAL ROSADI', 19, 'Laki-Laki', 'Ds. Bojong, Dawuan', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085222734124', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(15, '2866', 'KAFKA AZZIKRA RAHMAN', 19, 'Laki-Laki', 'Perum Cijati Residence No. A10', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082116421135', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(16, '2867', 'KHALIFA ADIANI SYAM', 19, 'Perempuan', 'Mujul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082116257455', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(17, '2873', 'M ARSYAD NURSYAHID SARONI', 19, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085314344422', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(18, '2878', 'MARWA SOPHIA NATHANIA', 19, 'Perempuan', 'Ds. Karangsambung, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085311838035', NULL, '2018-10-06 02:54:14', '2018-10-06 02:54:14'),
(19, '2889', 'MUHAMAD DAFFA RIZQULLAH', 19, 'Laki-Laki', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085321190111', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(20, '2996', 'MUHAMMAD ALIF AL-FAWWAZ', 19, 'Laki-Laki', 'Jl. Pemuda, Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081324043531', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(21, '2903', 'MUHAMMAD GIBRAN M.S.', 19, 'Laki-Laki', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, NULL, NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(22, '2895', 'MUHAMMAD HAIKAL HAFID RANIDA', 19, 'Laki-Laki', 'Majalengka Wetan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085295501298', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(23, '2909', 'MUHAMMAD WILDAN THOORIQ ARRISYAD', 19, 'Laki-Laki', 'Sarijadi, Sukasari, Bandung', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '087823455586', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(24, '2938', 'RAFA NADA SITI NAJ ADILAH', 19, 'Perempuan', 'Ds. Maja Utara, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224851977', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(25, '2948', 'RIANTI AULIANI', 19, 'Perempuan', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082315403052', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(26, '2951', 'RIKA SRI ASIH HASANAH', 19, 'Perempuan', 'Ds. Cicalung, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082320031983', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(27, '2955', 'RIZQON MUSTHAPA AL NASR', 19, 'Laki-Laki', 'Ds. Babakancuyu, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085324284375', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(28, '2964', 'SALWA SAFINA NOOR ATHALA', 19, 'Perempuan', 'Ds. Sindangwangi, Sindangwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082218179200', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(29, '2973', 'SHIFA NUR AROFAH', 19, 'Perempuan', 'Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085322890410', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(30, '2978', 'SITI INDIRA KHOEROTUNNISA', 19, 'Perempuan', 'Ds. Sukahaji, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '08122391722', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(31, '2981', 'SRI NANDA HIKMATULLOH', 19, 'Perempuan', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082116518812', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(32, '2993', 'YUSAN NURUL SEPTIYANI', 19, 'Perempuan', 'Ds. Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082219076628', NULL, '2018-10-06 02:54:15', '2018-10-06 02:54:15'),
(33, '2999', 'ABDULLAH TSANI AZZUHAIR', 2, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, NULL, NULL, '2018-10-06 03:00:40', '2018-10-06 03:00:40'),
(34, '3000', 'ABID FAUZAN', 2, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082315733622', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(35, '3003', 'ADILA SALSABILA', 2, 'Perempuan', 'Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224104326', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(36, '3004', 'ADINDA SHAIRA RISKI NASTARI SYAHRIR', 2, 'Perempuan', 'Komplek Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '089662270027', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(37, '3008', 'AHMAD QOSIM KUSYAERI', 2, 'Laki-Laki', 'Panyingkiran Kananga', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081240193236', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(38, '3021', 'AMELYARIHAN PUTRI HAULIYAN', 2, 'Perempuan', 'Jl. Bojong, Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082214333088', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(39, '3026', 'ANNA AGHITSNA NURFARAS D', 2, 'Perempuan', 'Jl. Laswi, Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085317237616', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(40, '3031', 'ARVINA PUTRI RACHMAN', 2, 'Perempuan', 'Jl. Suma, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085324381111', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(41, '3032', 'ARY APRILLA TARIGAN', 2, 'Laki-Laki', 'Blok Margaharja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082218444554', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(42, '3041', 'BUNGA LAIL INSYIROH', 2, 'Perempuan', 'Blok Kenanga, Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081222155512', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(43, '3044', 'DAFFA HAIBAN MUJAKKI', 2, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224499392', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(44, '3046', 'DERIS DARMAWAN HENDARSAH', 2, 'Laki-Laki', 'Jl. Pangeran Muhammad', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224933755', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(45, '3079', 'GERA NURFAUZA', 2, 'Laki-Laki', 'Panyingkiran, BTN Andir', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '08112340076', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(46, '3081', 'GEULIS KHARISMA PUTRI AL- HARIYATI', 2, 'Perempuan', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081395642126', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(47, '3086', 'HADI AUDIANSYAH', 2, 'Laki-Laki', 'Kadipaten, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082118942727', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(48, '3090', 'HASNA NUR FAJRIYANTY', 2, 'Perempuan', 'Panyingkiran, BTN Andir', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081324339971', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(49, '3092', 'HELSA MUTIARA RAHMAWATI', 2, 'Perempuan', 'Cicenang, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081320674724', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(50, '3102', 'KANIA PUSPA KIRANI', 2, 'Perempuan', 'Perum BCA, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221279980', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(51, '3110', 'LUTHFIAH KHAIRUNNISA', 2, 'Perempuan', 'Jl. Imam Bonjol', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221143109', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(52, '3114', 'MARISA DWI LESTARI', 2, 'Perempuan', 'Kodim 0617, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085223860211', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(53, '3129', 'MUHAMMAD ISMUL ADHOM ZAKIYA FARID', 2, 'Laki-Laki', 'Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '08122215360', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(54, '3136', 'MUHAMMAD ROYHAN FARHAN HABIBI', 2, 'Laki-Laki', 'Jl. Suma, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224446057', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(55, '3148', 'NANA SEPTIANA', 2, 'Laki-Laki', 'Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221395106', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(56, '3149', 'NAURA NASHTIA KHANSA', 2, 'Perempuan', 'Perum Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085317731338', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(57, '3153', 'NINDA LESTARI', 2, 'Perempuan', 'Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224770925', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(58, '3154', 'NISA FUZIANTI RAMADHAN', 2, 'Perempuan', 'Jl. Pahlawan', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085224379905', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(59, '3158', 'NURAZIZAH WIDIASIH', 2, 'Perempuan', 'Gg. Panday, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082119621178', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(60, '3162', 'PRAMUDHITA DWI PRASASTI', 2, 'Perempuan', 'Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082130738988', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(61, '3163', 'PUTRI AZZAHRA', 2, 'Perempuan', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081312080146', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(62, '3170', 'Rd. ZAIDAN AKMAL FADHLULLOH', 2, 'Laki-Laki', 'Perum BCA, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082127310027', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(63, '3172', 'RESTU ADJI PAMUNGKAS', 2, 'Laki-Laki', 'Komplek Pusaka Indah', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081222555540', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(64, '3183', 'RIZKY ADINUL AKBAR', 2, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '089618813813', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(65, '3215', 'WILDAN ZHILAL MANAFI', 2, 'Laki-Laki', 'Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085318415779', NULL, '2018-10-06 03:00:41', '2018-10-06 03:00:41'),
(66, '3245', 'ALYA RAHMAWATI', 1, 'Perempuan', 'Jatiraga Timur RT.03 RW.03, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082118081166', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(67, '3253', 'ARDINA NUR FATIMAH', 1, 'Perempuan', 'Perum BCA RT.11 RW.06 Jl. Mangga Raya N0.72, Cikalong, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082128882517', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(68, '3254', 'ARVIN FARAND FIRJATULLAH', 1, 'Laki-Laki', 'Jl. Gerakn Koperasi RT.01 RW.04 Ling. Giri Asih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081214279919', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(69, '3256', 'ASHFA NAYLA NAJA', 1, 'Perempuan', 'Jl. Veteran No. 89 RT.01 RW.01 Blok Ahad, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081224387834', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(70, '3266', 'DARYNNATA NUGRAHA', 1, 'Laki-Laki', 'Jl. Siti Armilah No.8', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '08112401339', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(71, '3278', 'DINDA NURFELIZA SUHANDI', 1, 'Perempuan', 'Jl. Ahmad Kusuma RT.05 RW.01', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081283370449', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(72, '3279', 'DREANTAMA MISBACHUDIN YUSMAN', 1, 'Laki-Laki', 'Jl. Pejuang 45 Blok Ahad RT.007 RW.003 Ds. Kulur', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081311581054', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(73, '3296', 'FARHAN FIRJATULLAH', 1, 'Laki-Laki', 'Ling. Leuwilenggik RT.019 RW.010 Kel. Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221025952', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(74, '3305', 'GAHARA RESTU PAMUJI', 1, 'Laki-Laki', 'Ling. Sirahdayeuh RT.03 RW.01, Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '089660635555', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(75, '3315', 'GINAYASYA RIZKY FATHIHA', 1, 'Perempuan', 'Jl. Sangadipa No.46 RT.004 RW.004 Blok Sabtu Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082240140178', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(76, '3322', 'HILMY ABDILLAH MUNGGARAN', 1, 'Laki-Laki', 'Cijati RT.01 RW.04', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081320413538', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(77, '3326', 'IMA ALIMA SABILA RAHMA', 1, 'Perempuan', 'Jl. Blok Senin Gandok RT.002 RW.002 Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081386432424', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(78, '3331', 'KARTIKA MAULIDA AZZAHRA', 1, 'Perempuan', 'Ds. Kutamanggu RT.08 RW.03', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082127685165', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(79, '3349', 'MALIK BARKAH ARRAYHAN', 1, 'Laki-Laki', 'Blok Karapyak No.32 Panglayunga, RT.12 RW.06 Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085320077015', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(80, '3351', 'MAULANA YUSUF ISKANDAR', 1, 'Laki-Laki', 'Jl. Cicenang RT.02 RW.05', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082217070054', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(81, '3354', 'MOCHAMAD AZID MUSLIM', 1, 'Laki-Laki', 'Blok Sangkanhurip RT.003 Rw.001 Ds. Pasirmuncang', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085219041517', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(82, '3355', 'MOCHAMAD RIZKY MAULANA', 1, 'Laki-Laki', 'Ling. Mekarjaya RT.002 RW.001 No.67 Kel. Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085317784264', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(83, '3367', 'MUHAMMAD HASBI HARDIAN', 1, 'Laki-Laki', 'Jl. Jatisampay Gg. Misna No.18', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085280123782', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(84, '3369', 'MUHAMMAD KAISAR WIBAWA UTAMA', 1, 'Laki-Laki', 'Blok Senin RT.13 RW.05 Ds. Tarikolot, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085322070585', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(85, '3372', 'MUHAMMAD QALBI', 1, 'Laki-Laki', 'Jl. Abdul Gani No.9', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081320089512', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(86, '3377', 'NABILA AZZAHRA', 1, 'Perempuan', 'Ds. Cibulan Kec. Lemahsugih', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085221421000', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(87, '3382', 'NAISA SYAHRA SALSABILA', 1, 'Perempuan', 'Blok Ciandeu RT.02 RW.14 Ds. Sidamukti', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082137962886', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(88, '3384', 'NASYA NAZIBA RAHMADYANA', 1, 'Perempuan', 'Blok Cirahayu RT.01 RW.01 Ds. Baribis, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '087761543050', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(89, '3385', 'NASYA ZASKYA HAFIZHA', 1, 'Perempuan', 'Blok Sabtu Rt.01 RW.01 Ds. Heuleut', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081324756586', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(90, '3396', 'OKTAVIA REGITA GAYANTRI', 1, 'Perempuan', 'Jl. Margaraharja RT.09 RW.03 Kel. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082127895585', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(91, '3403', 'RAFA ABIYASA DARMAJATI', 1, 'Laki-Laki', 'Jl. K.H. Abdul Halim No.238', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085320625279', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(92, '3425', 'SANDRINA FADYA DIZA', 1, 'Perempuan', 'Ling. Sirahdayeuh RT.05 RW.02, Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085295511487', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(93, '3427', 'SANI NAFILAH MARDANI', 1, 'Perempuan', 'Gg. Anggur No.442 Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085321591216', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(94, '3458', 'ZACKY ALFAUZA', 1, 'Laki-Laki', 'Jl. Kartini Gg. Rahayu 1 No.262', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085810189879', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(95, '3459', 'ZAFIRA LEXA IRAWAN', 1, 'Perempuan', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, NULL, NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(96, '3461', 'ZAHRA SAFINATUN NAJA', 1, 'Perempuan', 'Jl. Letkol Abdul Gani Gg. Haji Oman No.62', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '081222092579', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(97, '3462', 'ZAKI IMAMUL UMAM', 1, 'Laki-Laki', 'Ling. Mekarjaya RT.002 RW.001 No.43 Kel. Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '085295896737', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35'),
(98, '3464', 'ZASKIA AMELIA NURUDIN', 1, 'Perempuan', 'Blok Babakan Jatimulya RT.08 RW.04 Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, '2018', NULL, NULL, '082240899688', NULL, '2018-10-06 03:15:35', '2018-10-06 03:15:35');

-- --------------------------------------------------------

--
-- Table structure for table `attedances`
--

CREATE TABLE `attedances` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam_masuk` time NOT NULL,
  `jam_keluar` time DEFAULT NULL,
  `hadir` tinyint(1) NOT NULL,
  `sakit` tinyint(1) NOT NULL,
  `terlambat` tinyint(1) NOT NULL,
  `alfa` tinyint(1) NOT NULL,
  `izin` tinyint(1) NOT NULL,
  `pulang` tinyint(1) NOT NULL,
  `nsm` tinyint(1) NOT NULL COMMENT 'Notifikasi SMS ketika masuk TRUE OR FALSE',
  `nsp` tinyint(1) NOT NULL COMMENT 'Notifikasi SMS ketika pulang / keluar TRUE OR FALSE',
  `kjm` time NOT NULL COMMENT 'Ketentuan jam masuk',
  `kjp` time NOT NULL COMMENT 'Ketentuan jam pulang',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fingerprint`
--

CREATE TABLE `fingerprint` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `com_key` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fingerprint`
--

INSERT INTO `fingerprint` (`id`, `name`, `user_id`, `password`, `ip_address`, `com_key`, `created_at`, `updated_at`) VALUES
(1, 'Mesin 1', '10000', '1', '192.168.137.11', 0, '2018-10-06 01:49:55', '2018-10-06 01:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, '7A', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(2, '7B', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(3, '7C', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(4, '7D', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(5, '7E', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(6, '7F', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(7, '7G', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(8, '7H', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(9, '7I', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(10, '8A', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(11, '8B', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(12, '8C', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(13, '8D', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(14, '8E', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(15, '8F', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(16, '8G', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(17, '8H', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(18, '8I', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(19, '9A', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(20, '9B', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(21, '9C', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(22, '9D', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(23, '9E', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(24, '9F', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(25, '9G', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(26, '9H', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(27, '9I', '2018-10-06 01:49:55', '2018-10-06 01:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_07_011358_create_roles_table', 1),
(4, '2018_02_07_163124_create_grades_table', 1),
(5, '2018_02_07_163426_create_students_table', 1),
(6, '2018_02_10_021900_fingerprint', 1),
(7, '2018_03_07_010536_create_attedances_table', 1),
(8, '2018_08_01_222454_create_alumni_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `user_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 1, 'Consequatur neque non illum. Molestiae sed deserunt impedit est placeat. Sunt nam officia incidunt omnis libero natus quibusdam quia. Facere cumque non facere nostrum quia cumque totam.', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(2, 'middleadmin', 2, 'Fugiat accusantium itaque quod temporibus ut earum recusandae. Maxime nihil atque quas rerum quia. Cupiditate odit sed doloremque et at. Repellendus facilis illum nobis sint quis dolore.', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(3, 'lowadmin', 3, 'Placeat dolorem ducimus reiciendis atque et repellat. Molestias consequatur non tenetur earum dolore. Porro et aperiam officiis.', '2018-10-06 01:49:55', '2018-10-06 01:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `nis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grade_id` int(10) UNSIGNED NOT NULL,
  `pin` int(11) NOT NULL,
  `kelamin` enum('Laki-Laki','Perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tmplahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'noimage.jpg',
  `kota` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun_masuk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nmortu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nohp_ortu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nowa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `nis`, `name`, `grade_id`, `pin`, `kelamin`, `alamat`, `tmplahir`, `tgllahir`, `foto`, `kota`, `tahun_masuk`, `nohp`, `nmortu`, `nohp_ortu`, `nowa`, `created_at`, `updated_at`) VALUES
(1, '3025', 'ANIQ MUFLIHAH', 10, 1, 'Perempuan', 'Jl. KH Abdul Halim', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224771199', NULL, '2018-10-06 01:52:19', '2018-10-06 01:52:19'),
(2, '3034', 'AULIA KHOERUNNISA', 10, 2, 'Perempuan', 'Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224141222', NULL, '2018-10-06 01:52:19', '2018-10-06 01:52:19'),
(3, '3037', 'BAMBANG SUGIARTO', 10, 3, 'Laki-Laki', 'Ds. Jatisawit', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085351429274', NULL, '2018-10-06 01:52:20', '2018-10-06 01:52:20'),
(4, '3038', 'BAYU RIZKI ROHMANUDIN', 10, 4, 'Laki-Laki', 'Jl. Imam Bonjol', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321065072', NULL, '2018-10-06 01:52:20', '2018-10-06 01:52:20'),
(5, '3042', 'CANTIKA RATU AVELIA', 10, 5, 'Perempuan', 'Jl. Olahraga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224764411', NULL, '2018-10-06 01:52:21', '2018-10-06 01:52:21'),
(6, '3043', 'D. ANGGIA PUTRI ROHENDA', 10, 6, 'Perempuan', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324574757', NULL, '2018-10-06 01:52:21', '2018-10-06 01:52:21'),
(7, '3047', 'DHINI AZ-ZAHRA', 10, 7, 'Perempuan', 'Ds. Heuleut', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085250534500', NULL, '2018-10-06 01:52:22', '2018-10-06 01:52:22'),
(8, '3048', 'DHIYA ALFIYYAH RAMADHANI', 10, 8, 'Perempuan', 'Jl. Kartini', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224415900', NULL, '2018-10-06 01:52:22', '2018-10-06 01:52:22'),
(9, '3054', 'DIO SHEGY RAFFAEL', 10, 9, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320161509', NULL, '2018-10-06 01:52:23', '2018-10-06 01:52:23'),
(10, '3056', 'DJATI IHSAN FARDANA', 10, 10, 'Laki-Laki', 'Jl. Pemuda', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222378044', NULL, '2018-10-06 01:52:23', '2018-10-06 01:52:23'),
(11, '3059', 'ERLANGGA FAWAZ SOFYAN', 10, 11, 'Laki-Laki', 'Ds. Kertabasuki', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122110705', NULL, '2018-10-06 01:52:24', '2018-10-06 01:52:24'),
(12, '5', 'FAIQ RIZKY ARYAPUTRA', 10, 12, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122160135', NULL, '2018-10-06 01:52:24', '2018-10-06 01:52:24'),
(13, '3074', 'FIRNA HASNAENI MAULIDA', 10, 13, 'Perempuan', 'Ds. Leuwiseeng', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321813494', NULL, '2018-10-06 01:52:25', '2018-10-06 01:52:25'),
(14, '3075', 'FIRYAL AINA SALSABILA', 10, 14, 'Perempuan', 'Ds. Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08987995024', NULL, '2018-10-06 01:52:26', '2018-10-06 01:52:26'),
(15, '3078', 'GALUH ZAHRA SAFITRI', 10, 15, 'Perempuan', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295345510', NULL, '2018-10-06 01:52:26', '2018-10-06 01:52:26'),
(16, '6', 'GHEFIRA ZAHIRA SHAFFA', 10, 16, 'Perempuan', 'Ds. Gunung Kuning', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08112430060', NULL, '2018-10-06 01:52:27', '2018-10-06 01:52:27'),
(17, '3082', 'GHIFARI AZRIEL AKBARI', 10, 17, 'Laki-Laki', 'Perum BTN Andir', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353835595', NULL, '2018-10-06 01:52:27', '2018-10-06 01:52:27'),
(18, '3099', 'IRSYAD NAFI MUTHOHHAR', 10, 18, 'Laki-Laki', 'Jl. Babakan Koda', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321395751', NULL, '2018-10-06 01:52:28', '2018-10-06 01:52:28'),
(19, '3105', 'LAHIRA PUTRA FEBRIANO', 10, 19, 'Laki-Laki', 'Jl. Olahraga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315981510', NULL, '2018-10-06 01:52:28', '2018-10-06 01:52:28'),
(20, '3121', 'MISBAH SETIAHARJA', 10, 20, 'Laki-Laki', 'Jl. Pemuda', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122213147', NULL, '2018-10-06 01:52:29', '2018-10-06 01:52:29'),
(21, '3126', 'MUHAMMAD DAFFA AZHAR MUSYAFFA', 10, 21, 'Laki-Laki', 'Jl. Kesehatan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082216167619', NULL, '2018-10-06 01:52:30', '2018-10-06 01:52:30'),
(22, '3142', 'NAILA SARAH MAHARANI', 10, 22, 'Perempuan', 'Ds. Cihaur, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315666619', NULL, '2018-10-06 01:52:31', '2018-10-06 01:52:31'),
(23, '3152', 'NAJWA KHAIRANI SHAFA BILQIS', 10, 23, 'Perempuan', 'Jl. Siti Armilah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082214445854', NULL, '2018-10-06 01:52:32', '2018-10-06 01:52:32'),
(24, '3147', 'NAJYA KAILA DEITHA WAHNADIAN', 10, 24, 'Perempuan', 'Jl. Satari Pasar Balong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220366535', NULL, '2018-10-06 01:52:33', '2018-10-06 01:52:33'),
(25, '3146', 'NAZWA ADZRAA LABIQA', 10, 25, 'Perempuan', 'Ds. Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122448260', NULL, '2018-10-06 01:52:34', '2018-10-06 01:52:34'),
(26, '3173', 'RESTU GALUH FADLILLAHI', 10, 26, 'Laki-Laki', 'Ds. Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295511305', NULL, '2018-10-06 01:52:35', '2018-10-06 01:52:35'),
(27, '3176', 'REYHAN AUFA SHIDQI RAMADHANI', 10, 27, 'Laki-Laki', 'Ds. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082214365861', NULL, '2018-10-06 01:52:36', '2018-10-06 01:52:36'),
(28, '32243224', 'RIKI FEBRIYANTO', 10, 28, 'Laki-Laki', 'Sukasari kaler, Maja', 'Majalengka', '2018-10-06', 'noimage.jpg', NULL, NULL, NULL, NULL, '085295192688', NULL, '2018-10-06 01:52:37', '2018-10-06 03:16:07'),
(29, '3194', 'SERGIA AGGASSY', 10, 29, 'Perempuan', 'Cijati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323144942', NULL, '2018-10-06 01:52:38', '2018-10-06 01:52:38'),
(30, '3196', 'SHALSA AULIA', 10, 30, 'Perempuan', 'Ds. Heuleut', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085318541954', NULL, '2018-10-06 01:52:39', '2018-10-06 01:52:39'),
(31, '3205', 'SYAFANTI DENA MAHARANI', 10, 31, 'Perempuan', 'Jl. Kesehatan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082218618994', NULL, '2018-10-06 01:52:40', '2018-10-06 01:52:40'),
(32, '3206', 'SYAHLA DHIYAUL AULIA', 10, 32, 'Perempuan', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324240493', NULL, '2018-10-06 01:52:41', '2018-10-06 01:52:41'),
(33, '3217', 'YUDA DARUL HUSIN', 10, 33, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089660250595', NULL, '2018-10-06 01:52:41', '2018-10-06 01:52:41'),
(34, '3220', 'ZAHWA AWALUNNISA MULYA ZAINABBIYAH', 10, 34, 'Perempuan', 'Jl. Pahlawan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08132073267', NULL, '2018-10-06 01:52:42', '2018-10-06 01:52:42'),
(35, '3005', 'ADRIAN MAULANA', 12, 35, 'Laki-Laki', 'Blok Mawar, Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087744374486', NULL, '2018-10-06 02:13:42', '2018-10-06 02:13:42'),
(36, '3006', 'AGUNG AULIA RAHMAN', 12, 36, 'Laki-Laki', 'Koplek Giri Asih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295695679', NULL, '2018-10-06 02:13:43', '2018-10-06 02:13:43'),
(37, '3011', 'AKBAR RIZKI MAULANA', 12, 37, 'Laki-Laki', 'Paku Beureum', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224444804', NULL, '2018-10-06 02:13:44', '2018-10-06 02:13:44'),
(38, '3022', 'AMILA FI\'LA AMALIA', 12, 38, 'Perempuan', 'Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224383924', NULL, '2018-10-06 02:13:44', '2018-10-06 02:13:44'),
(39, '3030', 'ARVIAN RADITYA ROSISSENDRA', 12, 39, 'Laki-Laki', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320358395', NULL, '2018-10-06 02:13:45', '2018-10-06 02:13:45'),
(40, '3035', 'AWALINA ZULFA', 12, 40, 'Perempuan', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324250074', NULL, '2018-10-06 02:13:45', '2018-10-06 02:13:45'),
(41, '3045', 'DANDY LUTHFI FATTUROHMAN', 12, 41, 'Laki-Laki', 'Jatisawit', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324383349', NULL, '2018-10-06 02:13:46', '2018-10-06 02:13:46'),
(42, '3049', 'DIAN AYU MARTIARANI', 12, 42, 'Perempuan', 'Ds. Leuwikidang, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295773888', NULL, '2018-10-06 02:13:46', '2018-10-06 02:13:46'),
(43, '3052', 'DINARA SAFINA SYAHRIN', 12, 43, 'Perempuan', 'Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08161977093', NULL, '2018-10-06 02:13:47', '2018-10-06 02:13:47'),
(44, '3064', 'FAJAR RAFID IHSAN', 12, 44, 'Laki-Laki', 'Cipadung', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220643636', NULL, '2018-10-06 02:13:47', '2018-10-06 02:13:47'),
(45, '3069', 'FERDY HERNAWAN', 12, 45, 'Laki-Laki', 'Perum Munjul Indah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224830331', NULL, '2018-10-06 02:13:48', '2018-10-06 02:13:48'),
(46, '3072', 'FIQRI NURDIANSYAH', 12, 46, 'Laki-Laki', 'Mekar Sari', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320352186', NULL, '2018-10-06 02:13:48', '2018-10-06 02:13:48'),
(47, '3076', 'FITRI HANDAYANI NUR KARIMAH', 12, 47, 'Perempuan', 'Ds. Tajur', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082317050986', NULL, '2018-10-06 02:13:49', '2018-10-06 02:13:49'),
(48, '3084', 'GHINAA RAUDHATUL FARIHAH', 12, 48, 'Perempuan', 'Ds. Wanahayu, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082116200706', NULL, '2018-10-06 02:13:49', '2018-10-06 02:13:49'),
(49, '3088', 'HAMIYA AISYA MARDHIYA', 12, 49, 'Perempuan', 'Blok Mekar Sari, Ds. Sagara, Argapura', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082310310507', NULL, '2018-10-06 02:13:50', '2018-10-06 02:13:50'),
(50, '3100', 'JIHAN FATHI RATNA DITA', 12, 50, 'Perempuan', 'Simpur Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315549303', NULL, '2018-10-06 02:13:50', '2018-10-06 02:13:50'),
(51, '3113', 'M.HAFIDH ABDURAHIM', 12, 51, 'Laki-Laki', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:13:51', '2018-10-06 02:13:51'),
(52, '3118', 'MIFTAH RIZKY', 12, 52, 'Laki-Laki', 'Babakan Koda', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324670260', NULL, '2018-10-06 02:13:51', '2018-10-06 02:13:51'),
(53, '3139', 'NABILA PUTRI AULIYA', 12, 53, 'Perempuan', 'Ds. Girimukti', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221446803', NULL, '2018-10-06 02:13:52', '2018-10-06 02:13:52'),
(54, '3141', 'NAILA FAKHIRA AZ ZAHRA', 12, 54, 'Perempuan', 'Ds. Jerukleueut, Sindangwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320631594', NULL, '2018-10-06 02:13:52', '2018-10-06 02:13:52'),
(55, '3222', 'NENA NURJANAH', 12, 55, 'Perempuan', 'Gg. Hidayat, Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081381640365', NULL, '2018-10-06 02:13:53', '2018-10-06 02:13:53'),
(56, '3157', 'NURATI WAHYUNI', 12, 56, 'Perempuan', 'Ds. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323387906', NULL, '2018-10-06 02:13:53', '2018-10-06 02:13:53'),
(57, '3160', 'NURUL AZZAHRA', 12, 57, 'Perempuan', 'Gg. Tawakal, Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320280290', NULL, '2018-10-06 02:13:54', '2018-10-06 02:13:54'),
(58, '3164', 'RADITYA RABANI', 12, 58, 'Laki-Laki', 'Ds. Sukaraja Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085211998248', NULL, '2018-10-06 02:13:55', '2018-10-06 02:13:55'),
(59, '3168', 'RAKA MI\'RAJ HIKMAWAN', 12, 59, 'Laki-Laki', 'Jatisawit', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081384392624', NULL, '2018-10-06 02:13:55', '2018-10-06 02:13:55'),
(60, '3171', 'REIVA ARRINZANI', 12, 60, 'Perempuan', 'Kel. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224802270', NULL, '2018-10-06 02:13:56', '2018-10-06 02:13:56'),
(61, '3175', 'REVANY PERMANA PUTRI', 12, 61, 'Perempuan', 'Leuwikidang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0825220268886', NULL, '2018-10-06 02:13:56', '2018-10-06 02:13:56'),
(62, '3186', 'SALMA FAHIRA AZZAHRA', 12, 62, 'Perempuan', 'Ds. Jatisawit, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082316035387', NULL, '2018-10-06 02:13:57', '2018-10-06 02:13:57'),
(63, '3188', 'SALSABILA SHOLIHAH AZZAHRA', 12, 63, 'Perempuan', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317128495', NULL, '2018-10-06 02:13:57', '2018-10-06 02:13:57'),
(64, '3190', 'SALWA SUCITA SANTOSA', 12, 64, 'Perempuan', 'Link. Pusaka Indah Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081322145872', NULL, '2018-10-06 02:13:58', '2018-10-06 02:13:58'),
(65, '3193', 'SERDI MAHAZ JANUAR', 12, 65, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324575043', NULL, '2018-10-06 02:13:58', '2018-10-06 02:13:58'),
(66, '3199', 'SINTIA SUCI HERYAWATI', 12, 66, 'Perempuan', 'Ds. Bojong, Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315791166', NULL, '2018-10-06 02:13:59', '2018-10-06 02:13:59'),
(67, '3202', 'SITI SOFFIAH', 12, 67, 'Perempuan', 'Jl. Jakalalana, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315406535', NULL, '2018-10-06 02:13:59', '2018-10-06 02:13:59'),
(68, '3214', 'WIDIA PANGESTU', 12, 68, 'Perempuan', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:14:00', '2018-10-06 02:14:00'),
(69, '3219', 'ZAENAL MUHAJIRIN', 12, 69, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:14:00', '2018-10-06 02:14:00'),
(70, '3221', 'ZAKI FARHAN NASA', 12, 70, 'Laki-Laki', 'Blok Kerta Raharja, Cicalung, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082216881910', NULL, '2018-10-06 02:14:01', '2018-10-06 02:14:01'),
(71, '3001', 'ADAM SATRIO PAMUNGKAS', 13, 71, 'Laki-Laki', 'Ds. Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081910060478', NULL, '2018-10-06 02:16:02', '2018-10-06 02:16:02'),
(72, '3007', 'AHMAD MUSTOPA ALPAREZA', 13, 72, 'Laki-Laki', 'Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324333233', NULL, '2018-10-06 02:16:03', '2018-10-06 02:16:03'),
(73, '3010', 'AISYAH ARNO', 13, 73, 'Perempuan', 'Jl. Kartini', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081289414758', NULL, '2018-10-06 02:16:03', '2018-10-06 02:16:03'),
(74, '3013', 'ALDI MOCHAMAD FAUZAN', 13, 74, 'Laki-Laki', 'Ds. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395622368', NULL, '2018-10-06 02:16:04', '2018-10-06 02:16:04'),
(75, '3015', 'ALVI DESVIANA', 13, 75, 'Laki-Laki', 'Simpur, Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315906454', NULL, '2018-10-06 02:16:05', '2018-10-06 02:16:05'),
(76, '3018', 'AMANDA DEA FAUZIYYAH', 13, 76, 'Perempuan', 'Ds. Cibulan, Lemahsugih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317205024', NULL, '2018-10-06 02:16:05', '2018-10-06 02:16:05'),
(77, '3027', 'ANNISA DWI LESTARI', 13, 77, 'Perempuan', 'Jl. KH. Abdul Halim', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395564143', NULL, '2018-10-06 02:16:06', '2018-10-06 02:16:06'),
(78, '3028', 'ANTON SUMARTONO', 13, 78, 'Laki-Laki', 'Ds. Sukamenak', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353315879', NULL, '2018-10-06 02:16:06', '2018-10-06 02:16:06'),
(79, '3029', 'ARRUM ERNADIANI', 13, 79, 'Perempuan', 'Kel. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321273248', NULL, '2018-10-06 02:16:07', '2018-10-06 02:16:07'),
(80, '3050', 'DIKA NUR IHSAN', 13, 80, 'Laki-Laki', 'Ds. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322238765', NULL, '2018-10-06 02:16:07', '2018-10-06 02:16:07'),
(81, '3051', 'DIMAS SURYA SYAHPUTRA', 13, 81, 'Laki-Laki', 'Ds. Kertabasuki', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082128574579', NULL, '2018-10-06 02:16:08', '2018-10-06 02:16:08'),
(82, '3053', 'DINDA PUTRI RAHMAWATI', 13, 82, 'Perempuan', 'Ds. Cijurey', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324485915', NULL, '2018-10-06 02:16:08', '2018-10-06 02:16:08'),
(83, '3061', 'FAHDA ADILAH', 13, 83, 'Perempuan', 'Ds. Tajur', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294448001', NULL, '2018-10-06 02:16:09', '2018-10-06 02:16:09'),
(84, '3067', 'FAUZAN NUGROHO', 13, 84, 'Laki-Laki', 'BTN Munjul', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324025754', NULL, '2018-10-06 02:16:09', '2018-10-06 02:16:09'),
(85, '3080', 'GERY NURFAUZY', 13, 85, 'Laki-Laki', 'Ds. Karyamukti', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08112340076', NULL, '2018-10-06 02:16:10', '2018-10-06 02:16:10'),
(86, '3083', 'GHINA DWI JAYANI MUFIDA', 13, 86, 'Perempuan', 'Ds. Cijati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320726308', NULL, '2018-10-06 02:16:10', '2018-10-06 02:16:10'),
(87, '3087', 'HALIMATUSYADIAH', 13, 87, 'Perempuan', 'Ds. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320024875', NULL, '2018-10-06 02:16:11', '2018-10-06 02:16:11'),
(88, '3095', 'ILHAM BAIHAQQI', 13, 88, 'Laki-Laki', 'Ds. Leuwikidang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0853216070', NULL, '2018-10-06 02:16:11', '2018-10-06 02:16:11'),
(89, '3103', 'KHILDA MAWADATUSSURUR MUNAWAROH', 13, 89, 'Perempuan', 'Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224213131', NULL, '2018-10-06 02:16:12', '2018-10-06 02:16:12'),
(90, '3104', 'KHOERUL JAM JAM MUZADI', 13, 90, 'Laki-Laki', 'Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085318395619', NULL, '2018-10-06 02:16:12', '2018-10-06 02:16:12'),
(91, '3106', 'LINA SEPTIA', 13, 91, 'Perempuan', 'Ds. Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323487783', NULL, '2018-10-06 02:16:13', '2018-10-06 02:16:13'),
(92, '3109', 'LUTHFI AMIRUL HISYAM', 13, 92, 'Laki-Laki', 'Jl. Pemuda', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:16:14', '2018-10-06 02:16:14'),
(93, '3111', 'M. HUDA JANATUL MAKWA', 13, 93, 'Laki-Laki', 'Ds. Kadu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:16:14', '2018-10-06 02:16:14'),
(94, '3115', 'MEGA SINDI RAHAYU', 13, 94, 'Perempuan', 'Ds. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315733670', NULL, '2018-10-06 02:16:15', '2018-10-06 02:16:15'),
(95, '3122', 'MITA', 13, 95, 'Perempuan', 'Kel. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082295408959', NULL, '2018-10-06 02:16:15', '2018-10-06 02:16:15'),
(96, '3133', 'MUHAMMAD RAFI FADLURAHMAN', 13, 96, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224585224', NULL, '2018-10-06 02:16:16', '2018-10-06 02:16:16'),
(97, '3138', 'MUHAMMAD ZAKI RAMADHAN', 13, 97, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127308666', NULL, '2018-10-06 02:16:16', '2018-10-06 02:16:16'),
(98, '3159', 'NURHASANAH', 13, 98, 'Perempuan', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082128740323', NULL, '2018-10-06 02:16:17', '2018-10-06 02:16:17'),
(99, '3167', 'RAHADIAN SYIFA MAULANA', 13, 99, 'Laki-Laki', 'Ds. Cijurey', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295775558', NULL, '2018-10-06 02:16:17', '2018-10-06 02:16:17'),
(100, '3174', 'REVANIA RIHAN NUR HIZRI', 13, 100, 'Perempuan', 'Ds. Jatisawit', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317897060', NULL, '2018-10-06 02:16:18', '2018-10-06 02:16:18'),
(101, '3178', 'RIFA BAIDHA SYAHLA', 13, 101, 'Perempuan', 'Ds. Tomo', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081223025600', NULL, '2018-10-06 02:16:18', '2018-10-06 02:16:18'),
(102, '3179', 'RIFQI BANI ALI', 13, 102, 'Laki-Laki', 'Jl. Salamodin', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315784149', NULL, '2018-10-06 02:16:19', '2018-10-06 02:16:19'),
(103, '3187', 'SALMA MAULA RAHMADANI', 13, 103, 'Perempuan', 'Ds. Sunapulo, Talaga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082129111023', NULL, '2018-10-06 02:16:19', '2018-10-06 02:16:19'),
(104, '3195', 'SHALMA SA\'ADATUN HAMIDAH', 13, 104, 'Perempuan', 'Kel. Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082219572731', NULL, '2018-10-06 02:16:20', '2018-10-06 02:16:20'),
(105, '3213', 'VINIA QONITA HANIFA', 13, 105, 'Perempuan', 'Ds. Sutawangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315917888', NULL, '2018-10-06 02:16:21', '2018-10-06 02:16:21'),
(106, '3227342', 'ALYA FAUZIYAH', 13, 106, 'Perempuan', 'Ds. Pasir Muncang', 'Majalengka', '2018-10-06', 'noimage.jpg', 'Majalengka', NULL, NULL, NULL, '082118956253', NULL, '2018-10-06 02:16:21', '2018-10-06 02:57:24'),
(107, '3009', 'AINNY FAHMA RAMADHANI', 15, 107, 'Perempuan', 'Gunung Kuning', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324407945', NULL, '2018-10-06 02:17:07', '2018-10-06 02:17:07'),
(108, '3014', 'ALIF AQIILAH ZAKI', 15, 108, 'Laki-Laki', 'Beber, Jatitujuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:08', '2018-10-06 02:17:08'),
(109, '3019', 'AMARULLAH ARYANANDIKA', 15, 109, 'Laki-Laki', 'Ling. Lamejajar', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:08', '2018-10-06 02:17:08'),
(110, '3020', 'AMELIA FUJI ARYANTI', 15, 110, 'Perempuan', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082318882434', NULL, '2018-10-06 02:17:09', '2018-10-06 02:17:09'),
(111, '3033', 'ASMARANI ASTRIA LEGIANA', 15, 111, 'Perempuan', 'Talaga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082214650595', NULL, '2018-10-06 02:17:09', '2018-10-06 02:17:09'),
(112, '3055', 'DITA FITRIYANI LAILATUL QODRIYAH', 15, 112, 'Perempuan', 'Gunung Kuning', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082120176244', NULL, '2018-10-06 02:17:10', '2018-10-06 02:17:10'),
(113, '3057', 'DWI PURNAMA ASRI', 15, 113, 'Perempuan', 'Kec. Susukan, Cirebon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324412501', NULL, '2018-10-06 02:17:10', '2018-10-06 02:17:10'),
(114, '3058', 'DZAKI RIZKI PRATAMA', 15, 114, 'Laki-Laki', 'Sukaraja, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224112288', NULL, '2018-10-06 02:17:11', '2018-10-06 02:17:11'),
(115, '3060', 'FADLI ALWAN MULYANA', 15, 115, 'Laki-Laki', 'Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:11', '2018-10-06 02:17:11'),
(116, '3065', 'FAKHRIL DZIKRI ALGHIFARI', 15, 116, 'Laki-Laki', 'Sumber Jaya', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:12', '2018-10-06 02:17:12'),
(117, '3077', 'GALIH FADILAH IMAN', 15, 117, 'Laki-Laki', 'Jati Tujuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:12', '2018-10-06 02:17:12'),
(118, '3089', 'HARIS RIFALDI', 15, 118, 'Laki-Laki', 'Jati Mulya', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395567382', NULL, '2018-10-06 02:17:13', '2018-10-06 02:17:13'),
(119, '3094', 'IKHMATUL HASANAH', 15, 119, 'Perempuan', 'Malausma', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085316477770', NULL, '2018-10-06 02:17:13', '2018-10-06 02:17:13'),
(120, '3108', 'LULU NUR SALSABILA', 15, 120, 'Perempuan', 'Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127535880', NULL, '2018-10-06 02:17:14', '2018-10-06 02:17:14'),
(121, '3127', 'M. FAISAL ANWAR', 15, 121, 'Laki-Laki', 'Sumedang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:14', '2018-10-06 02:17:14'),
(122, '3117', 'MELI ANDRIANI', 15, 122, 'Perempuan', 'Babakan Manjeti', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295710221', NULL, '2018-10-06 02:17:15', '2018-10-06 02:17:15'),
(123, '3123', 'MOCH FAIZ FATHUL HAKIM', 15, 123, 'Laki-Laki', 'Leuwikidang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320829112', NULL, '2018-10-06 02:17:15', '2018-10-06 02:17:15'),
(124, '3137', 'MUHAMAD YUDIS PRASTYA', 15, 124, 'Laki-Laki', 'Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:16', '2018-10-06 02:17:16'),
(125, '3124', 'MUHAMMAD AUFA ALMAN FALUTHI', 15, 125, 'Laki-Laki', 'Bekasi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:16', '2018-10-06 02:17:16'),
(126, '3130', 'MUHAMMAD LUTHFI', 15, 126, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324242848', NULL, '2018-10-06 02:17:17', '2018-10-06 02:17:17'),
(127, '3143', 'NAILA SEPLIA HUMAIRA', 15, 127, 'Perempuan', 'Subang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081386644300', NULL, '2018-10-06 02:17:17', '2018-10-06 02:17:17'),
(128, '3145', 'NAJMA AFIFAH NURFADHILAH', 15, 128, 'Perempuan', 'Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320677829', NULL, '2018-10-06 02:17:18', '2018-10-06 02:17:18'),
(129, '3150', 'NAYLA NURUL FADILAH', 15, 129, 'Perempuan', 'Tolengas', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0895365247008', NULL, '2018-10-06 02:17:18', '2018-10-06 02:17:18'),
(130, '3155', 'NOVIA AULI FITRIANI', 15, 130, 'Perempuan', 'Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082214327326', NULL, '2018-10-06 02:17:19', '2018-10-06 02:17:19'),
(131, '3165', 'RAESHARD DZIKRA RIFANDY', 15, 131, 'Laki-Laki', 'Sumedang, Ujung Jaya', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:20', '2018-10-06 02:17:20'),
(132, '3166', 'RAFI ALGHIFARI', 15, 132, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122165868', NULL, '2018-10-06 02:17:20', '2018-10-06 02:17:20'),
(133, '3169', 'RANIA HUSNUL TRIANI', 15, 133, 'Perempuan', 'Mekar Sari, Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295577146', NULL, '2018-10-06 02:17:21', '2018-10-06 02:17:21'),
(134, '3192', 'SAYIDINA ALI', 15, 134, 'Laki-Laki', 'Pilangsari', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '083103453959', NULL, '2018-10-06 02:17:21', '2018-10-06 02:17:21'),
(135, '3198', 'SHOTYA WIDI HASTUTI', 15, 135, 'Perempuan', 'Pinangraja, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081318456400', NULL, '2018-10-06 02:17:22', '2018-10-06 02:17:22'),
(136, '3203', 'SRI BERLIAN GUSTINA', 15, 136, 'Perempuan', 'Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:23', '2018-10-06 02:17:23'),
(137, '3204', 'SUGIANA', 15, 137, 'Laki-Laki', 'Citayeum, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:17:23', '2018-10-06 02:17:23'),
(138, '3208', 'SYAMMAKH FATIH FARHAT', 15, 138, 'Laki-Laki', 'Garawastu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127553355', NULL, '2018-10-06 02:17:24', '2018-10-06 02:17:24'),
(139, '3216', 'YELLI AULIA RAMADHANI', 15, 139, 'Perempuan', 'Leuwi Laja, Sindangwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224478177', NULL, '2018-10-06 02:17:24', '2018-10-06 02:17:24'),
(140, '3218', 'YULIANA AZKA HIFDILAH', 15, 140, 'Perempuan', 'Pilangsari', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315554891', NULL, '2018-10-06 02:17:25', '2018-10-06 02:17:25'),
(141, '2765', 'ADI MUHAMAD ILHAM', 20, 141, 'Laki-Laki', 'Ds.Sunia,Banjaran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221039275', NULL, '2018-10-06 02:18:34', '2018-10-06 02:18:34'),
(142, '2770', 'AENA FARHATUL LAEL', 20, 142, 'Laki-Laki', 'Ds.Pilangsari,Jatitujuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324307832', NULL, '2018-10-06 02:18:34', '2018-10-06 02:18:34'),
(143, '2772', 'AFDI BINTANG SYA\'BANA', 20, 143, 'Laki-Laki', 'Ds.Sindangkerta,Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221080812', NULL, '2018-10-06 02:18:35', '2018-10-06 02:18:35'),
(144, '2791', 'ASEP BUDIMAN', 20, 144, 'Laki-Laki', 'Ds.Argamukti,Argapura', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085311659211', NULL, '2018-10-06 02:18:35', '2018-10-06 02:18:35'),
(145, '2803', 'DAFFA SHIDQIA RABBANY', 20, 145, 'Laki-Laki', 'Ds.Loji,Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220718875', NULL, '2018-10-06 02:18:36', '2018-10-06 02:18:36'),
(146, '2807', 'DHEA SYFA ANANDA', 20, 146, 'Perempuan', 'Ds.Padangsari,Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395622811', NULL, '2018-10-06 02:18:36', '2018-10-06 02:18:36'),
(147, '2818', 'DWI FERDIANSYAH', 20, 147, 'Laki-Laki', 'Ds.Lukanegara,Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '083147536145', NULL, '2018-10-06 02:18:37', '2018-10-06 02:18:37'),
(148, '2823', 'ERSAL FATUROHMAN', 20, 148, 'Laki-Laki', 'Ds.Maja Utara,Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221302444', NULL, '2018-10-06 02:18:38', '2018-10-06 02:18:38'),
(149, '2828', 'FAHMI FATHUL HUDA', 20, 149, 'Laki-Laki', 'Ds.Burujul Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324259399', NULL, '2018-10-06 02:18:39', '2018-10-06 02:18:39'),
(150, '2831', 'FARHAN HIDAYAT W.', 20, 150, 'Laki-Laki', 'Ds.Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:40', '2018-10-06 02:18:40'),
(151, '2837', 'FAZA FAUZAN AZHIMA NURUDDIN', 20, 151, 'Laki-Laki', 'Ds.Bojong Cideres, Dawan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320316134', NULL, '2018-10-06 02:18:41', '2018-10-06 02:18:41'),
(152, '2858', 'ILFI ARIFATUL HIKMAH', 20, 152, 'Perempuan', 'Ds.Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122190905', NULL, '2018-10-06 02:18:42', '2018-10-06 02:18:42'),
(153, '2870', 'LALA SYAHIDATUL FADILAH', 20, 153, 'Perempuan', 'Ds.Argasari,Talaga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08139875832', NULL, '2018-10-06 02:18:42', '2018-10-06 02:18:42'),
(154, '2872', 'M. ALFIAN NUROHMAN', 20, 154, 'Laki-Laki', 'Ds.Kertabasuki,Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224102829', NULL, '2018-10-06 02:18:43', '2018-10-06 02:18:43'),
(155, '2890', 'MUHAMAD FAIZAL GITSNI', 20, 155, 'Laki-Laki', 'Jln.Cibasale', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:43', '2018-10-06 02:18:43'),
(156, '2901', 'MUHAMMAD DZULKARNAIN', 20, 156, 'Laki-Laki', 'Ds.Banjaran,Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085240543375', NULL, '2018-10-06 02:18:44', '2018-10-06 02:18:44'),
(157, '2893', 'MUHAMMAD FARIS MAULANA', 20, 157, 'Laki-Laki', 'Ds.Liang Julang,Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:44', '2018-10-06 02:18:44'),
(158, '2898', 'MUHAMMAD NATHAN YUSABIRAN', 20, 158, 'Laki-Laki', 'Ds.Margajaya,Lemah Sugih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081214200303', NULL, '2018-10-06 02:18:45', '2018-10-06 02:18:45'),
(159, '2910', 'MUHAMMAD YAHYA FIDAURRAHMAN', 20, 159, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:45', '2018-10-06 02:18:45'),
(160, '2911', 'MUHTAJ FARHAN M A', 20, 160, 'Laki-Laki', 'Ds.Leuwikidang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324966444', NULL, '2018-10-06 02:18:46', '2018-10-06 02:18:46'),
(161, '2912', 'NABIL SAYYIDA LEGAWA', 20, 161, 'Laki-Laki', 'Komplek Giri Asih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324230663', NULL, '2018-10-06 02:18:46', '2018-10-06 02:18:46'),
(162, '2913', 'NABILA ADANIA NUR', 20, 162, 'Perempuan', 'Ds.Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324600670', NULL, '2018-10-06 02:18:47', '2018-10-06 02:18:47'),
(163, '2914', 'NABILA SADIDAH', 20, 163, 'Perempuan', 'Ds.Teja Mulya,Argapura', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085721166956', NULL, '2018-10-06 02:18:47', '2018-10-06 02:18:47'),
(164, '2919', 'NAFISA SALSABILA', 20, 164, 'Perempuan', 'Ds.Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122320670', NULL, '2018-10-06 02:18:48', '2018-10-06 02:18:48'),
(165, '2921', 'NAJLA SYAKIRA', 20, 165, 'Perempuan', 'Ds.kasturi,Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127835257', NULL, '2018-10-06 02:18:48', '2018-10-06 02:18:48'),
(166, '2926', 'NIDA AMINATURRIZQI', 20, 166, 'Perempuan', 'Ds.Cijati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294561012', NULL, '2018-10-06 02:18:49', '2018-10-06 02:18:49'),
(167, '2940', 'RAHMA ZAHRA', 20, 167, 'Perempuan', 'Ds.Candrajaya', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294450029', NULL, '2018-10-06 02:18:50', '2018-10-06 02:18:50'),
(168, '2946', 'REXI EKA PUTRA KURNIAWAN', 20, 168, 'Laki-Laki', 'Ds.Tarikolot,Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:50', '2018-10-06 02:18:50'),
(169, '2961', 'ROJAN NURJANNAH', 20, 169, 'Perempuan', 'Ds.Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317116260', NULL, '2018-10-06 02:18:51', '2018-10-06 02:18:51'),
(170, '2971', 'SHENY JASMINE ZAHIRAH', 20, 170, 'Perempuan', 'Ds.Heuleut', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:18:51', '2018-10-06 02:18:51'),
(171, '2976', 'SHINTA AULIA AGUSTIN', 20, 171, 'Perempuan', 'Ds.Heubeulisuk,Agapura', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08231741524', NULL, '2018-10-06 02:18:52', '2018-10-06 02:18:52'),
(172, '2977', 'SITI AISYAH ZULFA', 20, 172, 'Perempuan', 'Ds.Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324925569', NULL, '2018-10-06 02:18:52', '2018-10-06 02:18:52'),
(173, '2979', 'SITI LAELA SARI', 20, 173, 'Perempuan', 'Ds.Wanahayu,Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082121927282', NULL, '2018-10-06 02:18:53', '2018-10-06 02:18:53'),
(174, '2987', 'VIRGI FAISYA ADHAZIA', 20, 174, 'Perempuan', 'Ds.Cijati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082319150811', NULL, '2018-10-06 02:18:53', '2018-10-06 02:18:53'),
(175, '2774', 'AHMAD FADHILLAH NUGRAHA', 21, 175, 'Laki-Laki', 'Ds. Tajur', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087881268991', NULL, '2018-10-06 02:19:44', '2018-10-06 02:19:44'),
(176, '2780', 'ALDY ALFARIZY', 21, 176, 'Laki-Laki', 'Ds. Pasir Malati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085314299790', NULL, '2018-10-06 02:19:45', '2018-10-06 02:19:45'),
(177, '2781', 'ALFIANA RAHMAH', 21, 177, 'Perempuan', 'Ds. Liang Julang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220273346', NULL, '2018-10-06 02:19:45', '2018-10-06 02:19:45'),
(178, '2790', 'ARIEF RACHMANSYACH HAIDAR', 21, 178, 'Laki-Laki', 'Ds. Leuwiseeng, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224386808', NULL, '2018-10-06 02:19:46', '2018-10-06 02:19:46'),
(179, '2800', 'BAGUS ANGGA ANGGARAKSA', 21, 179, 'Laki-Laki', 'Ds. Tarikolot, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082224403166', NULL, '2018-10-06 02:19:47', '2018-10-06 02:19:47'),
(180, '2806', 'DARA ZULIA RAHMA HAMDANI', 21, 180, 'Perempuan', 'Ds. Cibatu, Munjul', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082318096021', NULL, '2018-10-06 02:19:47', '2018-10-06 02:19:47'),
(181, '2808', 'DEDE ABDUL KHOLIK', 21, 181, 'Laki-Laki', 'Ds. Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085213426267', NULL, '2018-10-06 02:19:48', '2018-10-06 02:19:48'),
(182, '2817', 'DIVANI KHAIRA ANGGISTYA', 21, 182, 'Perempuan', 'BTN Munjul Indah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315784175', NULL, '2018-10-06 02:19:48', '2018-10-06 02:19:48'),
(183, '2820', 'ELSA OKTAVIA AZZAHRA', 21, 183, 'Perempuan', 'Ds. Sukamukti, Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081323844486', NULL, '2018-10-06 02:19:49', '2018-10-06 02:19:49'),
(184, '2824', 'ESSA NURMUTIA', 21, 184, 'Perempuan', 'Ds. Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221181421', NULL, '2018-10-06 02:19:49', '2018-10-06 02:19:49'),
(185, '2833', 'FARIDATU SHOPIA', 21, 185, 'Perempuan', 'BTN Munjul Indah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324025754', NULL, '2018-10-06 02:19:50', '2018-10-06 02:19:50'),
(186, '2834', 'FATHAR NURHAMAD ZIDAN', 21, 186, 'Laki-Laki', 'Ds. Pasirmuncang, panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127646046', NULL, '2018-10-06 02:19:50', '2018-10-06 02:19:50'),
(187, '2836', 'FAUZIAH KHAERUNNISA HAMIMI', 21, 187, 'Perempuan', 'Ds. Babakan Manjeti, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081295821806', NULL, '2018-10-06 02:19:51', '2018-10-06 02:19:51'),
(188, '2839', 'FITRA AULIYA MALINDA', 21, 188, 'Perempuan', 'Ds. Cikasarung, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221657742', NULL, '2018-10-06 02:19:51', '2018-10-06 02:19:51'),
(189, '2843', 'GHIFARY FIRDAUS MAHARDIKA', 21, 189, 'Laki-Laki', 'Ds. Sukaraja Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395617691', NULL, '2018-10-06 02:19:52', '2018-10-06 02:19:52'),
(190, '2846', 'GINANJAR RIFA AINURIZAL', 21, 190, 'Laki-Laki', 'Ds. Argamukti, Argapura', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353819588', NULL, '2018-10-06 02:19:52', '2018-10-06 02:19:52'),
(191, '2848', 'HABIL RAUUF MUSSODIQ', 21, 191, 'Laki-Laki', 'Ds. Pakuberem, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221503314', NULL, '2018-10-06 02:19:53', '2018-10-06 02:19:53'),
(192, '2862', 'INTAN MULIACY', 21, 192, 'Perempuan', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224890075', NULL, '2018-10-06 02:19:53', '2018-10-06 02:19:53'),
(193, '2868', 'KHAREESMA PUTRI ANNADHI', 21, 193, 'Perempuan', 'Ds. Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081219091230', NULL, '2018-10-06 02:19:54', '2018-10-06 02:19:54'),
(194, '2875', 'M. MUADZ AL AFIF', 21, 194, 'Laki-Laki', 'Ds. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324033228', NULL, '2018-10-06 02:19:55', '2018-10-06 02:19:55'),
(195, '2891', 'MUHAMAD FAQIH IHSANUDIN', 21, 195, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:19:55', '2018-10-06 02:19:55'),
(196, '2892', 'MUHAMAD FARHAN MALDINI', 21, 196, 'Laki-Laki', 'Ds. Leuwiseeng, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089698285933', NULL, '2018-10-06 02:19:56', '2018-10-06 02:19:56'),
(197, '2894', 'MUHAMAD FATHUR MALDINI', 21, 197, 'Laki-Laki', 'Ds. Leuwiseeng, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089698285933', NULL, '2018-10-06 02:19:56', '2018-10-06 02:19:56'),
(198, '2902', 'MUHAMMAD FAATHIR AL MUKHRIJ', 21, 198, 'Laki-Laki', 'Ds. Leuwikidang, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321076606', NULL, '2018-10-06 02:19:57', '2018-10-06 02:19:57'),
(199, '2897', 'MUHAMMAD JILDAN JAISYURROHMAN', 21, 199, 'Laki-Laki', 'Ds. Banjaran, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221115588', NULL, '2018-10-06 02:19:57', '2018-10-06 02:19:57'),
(200, '2906', 'MUHAMMAD NEFAS NADJIBA GUMILANG', 21, 200, 'Laki-Laki', 'Ds. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08112404278', NULL, '2018-10-06 02:19:58', '2018-10-06 02:19:58'),
(201, '2899', 'MUHAMMAD RIZAL NUGRAHA', 21, 201, 'Laki-Laki', 'Ds. Babakan Jawa', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295703545', NULL, '2018-10-06 02:19:58', '2018-10-06 02:19:58'),
(202, '2915', 'NADIA FATHARANI ALYA', 21, 202, 'Perempuan', 'Ds. Cimeong, Banjaran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322285130', NULL, '2018-10-06 02:19:59', '2018-10-06 02:19:59'),
(203, '2918', 'NAFISA ALMA ASSOFI', 21, 203, 'Perempuan', 'Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082128338468', NULL, '2018-10-06 02:19:59', '2018-10-06 02:19:59'),
(204, '1', 'NAJWA ALYA FAUZIAH', 21, 204, 'Perempuan', 'Ds. Paniis, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085222732839', NULL, '2018-10-06 02:20:00', '2018-10-06 02:20:00'),
(205, '2927', 'NIDA MAULIDA ZAKIYYAH', 21, 205, 'Perempuan', 'Ds. Tarikolot, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395989156', NULL, '2018-10-06 02:20:01', '2018-10-06 02:20:01'),
(206, '2928', 'NIRMALA APRILIA', 21, 206, 'Perempuan', 'Ds. Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082130480877', NULL, '2018-10-06 02:20:01', '2018-10-06 02:20:01'),
(207, '2942', 'RANIA PUTRI SAFIRA', 21, 207, 'Perempuan', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081313040076', NULL, '2018-10-06 02:20:02', '2018-10-06 02:20:02'),
(208, '2965', 'SANIYYAH NURWANGSANA', 21, 208, 'Perempuan', 'Ds. Maja Utara, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082317084413', NULL, '2018-10-06 02:20:02', '2018-10-06 02:20:02'),
(209, '2968', 'SHAFA HASNA FADHILAH', 21, 209, 'Perempuan', 'Majalengka Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085382276729', NULL, '2018-10-06 02:20:03', '2018-10-06 02:20:03'),
(210, '2975', 'SILVIA ADITIA HAERUNISA', 21, 210, 'Perempuan', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315477611', NULL, '2018-10-06 02:20:03', '2018-10-06 02:20:03'),
(211, '2986', 'VIONA ANGGIYA WARDHANI', 21, 211, 'Perempuan', 'Ds. Leuwilenggik, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0853244155555', NULL, '2018-10-06 02:20:04', '2018-10-06 02:20:04'),
(212, '2988', 'VIRYAL SYIFA MAITSA', 21, 212, 'Perempuan', 'Maja Selatan, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082119299788', NULL, '2018-10-06 02:20:04', '2018-10-06 02:20:04'),
(246, '2764', 'ADHWA BARQI HAIFAN', 22, 213, 'Laki-Laki', 'Villa Sangraja Permai, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085524000106', NULL, '2018-10-06 02:45:29', '2018-10-06 02:45:29'),
(247, '2766', 'ADIL JIHAD DARMAWAN', 22, 214, 'Laki-Laki', 'Ds. Margajaya, Lemahsugih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085211820223', NULL, '2018-10-06 02:45:29', '2018-10-06 02:45:29'),
(248, '3226', 'ADITYA SETIAWAN', 22, 215, 'Laki-Laki', 'Ds. Jatiserang, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081287776399', NULL, '2018-10-06 02:45:30', '2018-10-06 02:45:30'),
(249, '2776', 'AI SITI MARWAH', 22, 216, 'Perempuan', 'Jl. Pemuda, Ds. Sukamukti, Cikijing', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082317007295', NULL, '2018-10-06 02:45:30', '2018-10-06 02:45:30'),
(250, '2', 'AI SITI NAI LATUL KHOER', 22, 217, 'Perempuan', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222171389', NULL, '2018-10-06 02:45:31', '2018-10-06 02:45:31'),
(251, '2777', 'AKMAL HAKIM', 22, 218, 'Laki-Laki', 'Jl. Narada, Tanah tinggi, Johar, Jakarta Pusat', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082299341970', NULL, '2018-10-06 02:45:31', '2018-10-06 02:45:31'),
(252, '2783', 'ALIMATUSSILMI DINDA HIDAYAH', 22, 219, 'Perempuan', 'Jl. Sangraja kidul, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324929973', NULL, '2018-10-06 02:45:32', '2018-10-06 02:45:32'),
(253, '2788', 'ARI PERMANA', 22, 220, 'Laki-Laki', 'Ds. Dukuh Sawah, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221233163', NULL, '2018-10-06 02:45:32', '2018-10-06 02:45:32'),
(254, '2796', 'AYURA SUWARGANING FADILLAH', 22, 221, 'Perempuan', 'Jl. Desa Dawuan, Dawuan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0813199480', NULL, '2018-10-06 02:45:33', '2018-10-06 02:45:33'),
(255, '2804', 'DAIRA AULIA WINDIYA', 22, 222, 'Perempuan', 'Ds. Pilangsari, Jatitujuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087728925890', NULL, '2018-10-06 02:45:34', '2018-10-06 02:45:34'),
(256, '2811', 'DHARUL AUDI HIQMANA', 22, 223, 'Laki-Laki', 'Ds. Bongas, Sumberjaya', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:45:34', '2018-10-06 02:45:34'),
(257, '2850', 'HANIF SABIL ROBANI', 22, 224, 'Laki-Laki', 'Kel. Majalengka kulon, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324047607', NULL, '2018-10-06 02:45:35', '2018-10-06 02:45:35'),
(258, '2855', 'HERVANDA KUSUMA NURPUAD', 22, 225, 'Laki-Laki', 'Ds. Sukaperna, Talaga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324214465', NULL, '2018-10-06 02:45:35', '2018-10-06 02:45:35'),
(259, '2864', 'IRFAN MAULANA ISHAQ', 22, 226, 'Laki-Laki', 'Ds. Girimukti, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294537342', NULL, '2018-10-06 02:45:36', '2018-10-06 02:45:36'),
(260, '2871', 'LUTHFI JANAN HANDOYO', 22, 227, 'Laki-Laki', 'Kel. Majalengka kulon, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322325144', NULL, '2018-10-06 02:45:36', '2018-10-06 02:45:36'),
(261, '2876', 'M. RIFKI HIDAYATULLAH', 22, 228, 'Laki-Laki', 'Blok Kamis, Ds. Maja Utara, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224939606', NULL, '2018-10-06 02:45:37', '2018-10-06 02:45:37'),
(262, '2879', 'MAUNTY ISNAENY', 22, 229, 'Perempuan', 'Blok Senin, Ds. Maja Selatan, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320630166', NULL, '2018-10-06 02:45:38', '2018-10-06 02:45:38'),
(263, '2884', 'MOCHAMAD NAIN USNAINI', 22, 230, 'Laki-Laki', 'Ds. Tarikolot, Tarikolot', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085351550550', NULL, '2018-10-06 02:45:38', '2018-10-06 02:45:38'),
(264, '2916', 'NADIYA FITHRI AMALIYA', 22, 231, 'Perempuan', 'Ds. Enggalwangi, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081313232619', NULL, '2018-10-06 02:45:39', '2018-10-06 02:45:39'),
(265, '2931', 'NURMALA AMALIAH', 22, 232, 'Perempuan', 'Ds. Wanajaya, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082128115323', NULL, '2018-10-06 02:45:39', '2018-10-06 02:45:39'),
(266, '2937', 'RADHIFAN ASYADUDDIN', 22, 233, 'Laki-Laki', 'Dsn. Cibeureum, Ds. Pakubeuteum, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085864754016', NULL, '2018-10-06 02:45:40', '2018-10-06 02:45:40'),
(267, '2943', 'REIFAN WILDAN FIRDAUS', 22, 234, 'Laki-Laki', 'Ds. Candrajaya, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321713675', NULL, '2018-10-06 02:45:41', '2018-10-06 02:45:41'),
(268, '2945', 'REVA ADREVI AZ ZAHRA', 22, 235, 'Perempuan', 'Ds. Karangsambung, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122241257', NULL, '2018-10-06 02:45:41', '2018-10-06 02:45:41'),
(269, '2949', 'RIDWAN SOLEHUDIN', 22, 236, 'Laki-Laki', 'Ds. Palasah, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323146972', NULL, '2018-10-06 02:45:42', '2018-10-06 02:45:42'),
(270, '2962', 'SAHRUN NAJIB', 22, 237, 'Laki-Laki', 'Ds. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315733349', NULL, '2018-10-06 02:45:42', '2018-10-06 02:45:42'),
(271, '2970', 'SHELLY SILVIA PUTRI', 22, 238, 'Perempuan', 'Ds. Girimukti, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315790146', NULL, '2018-10-06 02:45:43', '2018-10-06 02:45:43'),
(272, '2980', 'SITI WAROSATIL AULIA', 22, 239, 'Perempuan', 'Ds. Gandu, Dawuan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323104507', NULL, '2018-10-06 02:45:43', '2018-10-06 02:45:43'),
(273, '2985', 'TSANI USADHI', 22, 240, 'Perempuan', 'Ds. Cicadas, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122311483', NULL, '2018-10-06 02:45:44', '2018-10-06 02:45:44'),
(274, '2793', 'ASTRI KHOERUNNISA', 23, 241, 'Perempuan', 'Ds. Pakubeureum, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324539996', NULL, '2018-10-06 02:45:44', '2018-10-06 02:45:44'),
(275, '2990', 'WILDAN RIZKARD AL AZKARI', 22, 242, 'Laki-Laki', 'Ds. Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222000275', NULL, '2018-10-06 02:45:45', '2018-10-06 02:45:45'),
(276, '2797', 'AZZIZAH FATIMAH AZZAHRO', 23, 243, 'Perempuan', 'Lingk. Margaraharja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122457562', NULL, '2018-10-06 02:45:46', '2018-10-06 02:45:46'),
(277, '2991', 'WINDRA EKA GANESHA', 22, 244, 'Laki-Laki', 'Ds. Sleman, Sliyeg, Indramayu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122302258', NULL, '2018-10-06 02:45:46', '2018-10-06 02:45:46'),
(278, '2815', 'DIHA SAEHUDIN', 23, 245, 'Laki-Laki', 'Ds. Nunuk, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085795856218', NULL, '2018-10-06 02:45:47', '2018-10-06 02:45:47'),
(279, '2992', 'WULAN KUSUMAH BELINDA PUTRI', 22, 246, 'Perempuan', 'Kel. Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315590575', NULL, '2018-10-06 02:45:47', '2018-10-06 02:45:47'),
(280, '2847', 'HABIBI FARHAN RAHMAT D.', 22, 247, 'Laki-Laki', 'Kel. Majalengka kulon, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122439048', NULL, '2018-10-06 02:45:48', '2018-10-06 02:45:48'),
(281, '2819', 'DWI NANDA PRINADI', 23, 248, 'Laki-Laki', 'Ds. Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321971346', NULL, '2018-10-06 02:45:48', '2018-10-06 02:45:48'),
(282, '2826', 'EYSA SAPITRI LESTARI', 23, 249, 'Perempuan', 'Kel. Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085351414832', NULL, '2018-10-06 02:45:49', '2018-10-06 02:45:49'),
(283, '3', 'MOCH. RIZKY RAMADHAN', 22, 250, 'Laki-Laki', 'Ds. Liangjulang. Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081313013750', NULL, '2018-10-06 02:45:49', '2018-10-06 02:45:49'),
(284, '2832', 'FARHAN RUHIMAT PRATAMA', 23, 251, 'Laki-Laki', 'Ds. Kulur, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294261599', NULL, '2018-10-06 02:45:59', '2018-10-06 02:45:59'),
(285, '2840', 'FITRIYAH JULIA CANDRANINGSIH', 23, 252, 'Perempuan', 'Kel. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0895615737328', NULL, '2018-10-06 02:45:59', '2018-10-06 02:45:59');
INSERT INTO `students` (`id`, `nis`, `name`, `grade_id`, `pin`, `kelamin`, `alamat`, `tmplahir`, `tgllahir`, `foto`, `kota`, `tahun_masuk`, `nohp`, `nmortu`, `nohp_ortu`, `nowa`, `created_at`, `updated_at`) VALUES
(286, '2849', 'HANABARA KUSWARA', 23, 253, 'Perempuan', 'Ds. Tenjolayar, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324248552', NULL, '2018-10-06 02:46:00', '2018-10-06 02:46:00'),
(287, '2851', 'HANNA ALIYAH AVRILIA', 23, 254, 'Perempuan', 'BTN Andir Panyingkirian', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222917069', NULL, '2018-10-06 02:46:00', '2018-10-06 02:46:00'),
(288, '2854', 'HENDIKA AGUSTIAN', 23, 255, 'Laki-Laki', 'Ds. Cicenang, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224222448', NULL, '2018-10-06 02:46:01', '2018-10-06 02:46:01'),
(289, '2859', 'ILHAM FATHURAHMAN', 23, 256, 'Laki-Laki', 'Ds. Babakan Manjeti, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085314350688', NULL, '2018-10-06 02:46:01', '2018-10-06 02:46:01'),
(290, '2874', 'M RIFKI AGUSTIAN', 23, 257, 'Laki-Laki', 'Ds. Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081214781595', NULL, '2018-10-06 02:46:02', '2018-10-06 02:46:02'),
(291, '2886', 'MUHAMAD ABDAN SYAKURO', 23, 258, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085351025103', NULL, '2018-10-06 02:46:02', '2018-10-06 02:46:02'),
(292, '2908', 'MUHAMMAD RIZAL RAMDHANI', 23, 259, 'Laki-Laki', 'Ds. Majalengka Wetan Link. Kaputihan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082119621178', NULL, '2018-10-06 02:46:03', '2018-10-06 02:46:03'),
(293, '2929', 'NOVY SRI RAHAYU', 23, 260, 'Perempuan', 'Kec. Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315919891', NULL, '2018-10-06 02:46:03', '2018-10-06 02:46:03'),
(294, '2936', 'PUTRI ISNA RAHMAH PADILAH', 23, 261, 'Perempuan', 'Ds. Maja Selatan, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221743420', NULL, '2018-10-06 02:46:04', '2018-10-06 02:46:04'),
(295, '2939', 'RAFLI SAEFUDIN', 23, 262, 'Laki-Laki', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321715916', NULL, '2018-10-06 02:46:04', '2018-10-06 02:46:04'),
(296, '2941', 'RAKA RAMADHANI', 23, 263, 'Laki-Laki', 'Jl. Siti Armilah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321024618', NULL, '2018-10-06 02:46:05', '2018-10-06 02:46:05'),
(297, '2954', 'RIZKA MAULANA ASYAPARI', 23, 264, 'Laki-Laki', 'Ds. Pakubeureum, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081312704473', NULL, '2018-10-06 02:46:06', '2018-10-06 02:46:06'),
(298, '2959', 'RIZKY MUHAMAD AREL\'S', 23, 265, 'Laki-Laki', 'Jl. Olah Raga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321431674', NULL, '2018-10-06 02:46:06', '2018-10-06 02:46:06'),
(299, '2963', 'SAID SURYA DHARMA', 23, 266, 'Laki-Laki', 'Ds. Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082218716030', NULL, '2018-10-06 02:46:07', '2018-10-06 02:46:07'),
(300, '2966', 'SARAH SITI AEINUR FALAH', 23, 267, 'Perempuan', 'Ds. Maja Utara, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224404665', NULL, '2018-10-06 02:46:07', '2018-10-06 02:46:07'),
(301, '2974', 'SILVA NURHALIZA', 23, 268, 'Perempuan', 'Ds. Lebak Siuh, Jatigede', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081313183394', NULL, '2018-10-06 02:46:08', '2018-10-06 02:46:08'),
(302, '2982', 'SRI NURHALIMAH', 23, 269, 'Perempuan', 'Ds. Tarikolot, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081312181884', NULL, '2018-10-06 02:46:08', '2018-10-06 02:46:08'),
(303, '2930', 'NURLAELA', 23, 270, 'Perempuan', 'Ds. Pasar Balong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127008640', NULL, '2018-10-06 02:46:09', '2018-10-06 02:46:09'),
(304, '2957', 'RIZKY CAHAYA AKBAR', 23, 271, 'Laki-Laki', 'Ds. Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085352442045', NULL, '2018-10-06 02:46:09', '2018-10-06 02:46:09'),
(305, '2794', 'AUFA RIZKI ABDILLAH', 23, 272, 'Laki-Laki', 'Ds. Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:10', '2018-10-06 02:46:10'),
(306, '2795', 'AULIA ZAHRA MUSTAHAYAH', 23, 273, 'Perempuan', 'Ds. Bojong Jati, Rancaekek', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082262353585', NULL, '2018-10-06 02:46:10', '2018-10-06 02:46:10'),
(307, '2810', 'DEILLA FADHILAH', 23, 274, 'Perempuan', 'Ds. Ciomas, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295907135', NULL, '2018-10-06 02:46:11', '2018-10-06 02:46:11'),
(308, '2814', 'DIHAN AZIS', 23, 275, 'Laki-Laki', 'Ds. Leuwikidang, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089661446168', NULL, '2018-10-06 02:46:11', '2018-10-06 02:46:11'),
(309, '2816', 'DISYA AMALIA FATIHA', 23, 276, 'Perempuan', 'Ds. Burujul Wetan, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224749775', NULL, '2018-10-06 02:46:12', '2018-10-06 02:46:12'),
(310, '2845', 'GINA DEA MARLIAN', 23, 277, 'Perempuan', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:13', '2018-10-06 02:46:13'),
(311, '2907', 'MUHAMMAD RAFI', 23, 278, 'Laki-Laki', 'Ds. Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:13', '2018-10-06 02:46:13'),
(312, '2944', 'RENDI MAWARDI SAID', 23, 279, 'Laki-Laki', 'Ds. Burujul Wetan, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:14', '2018-10-06 02:46:14'),
(313, '2947', 'REZA AIDIL FITRI FADILAH', 23, 280, 'Laki-Laki', 'Ds. Leuwikidang, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220101889', NULL, '2018-10-06 02:46:14', '2018-10-06 02:46:14'),
(314, '2952', 'RIZAL ABDULLAH', 23, 281, 'Laki-Laki', 'Ds. Leuwikidang, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:15', '2018-10-06 02:46:15'),
(315, '2972', 'SEPHIA MUKTI', 23, 282, 'Perempuan', 'Ds.Tarikolot, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085222955586', NULL, '2018-10-06 02:46:15', '2018-10-06 02:46:15'),
(316, '2984', 'SYIFA KHAERUNNISA', 23, 283, 'Perempuan', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224008565', NULL, '2018-10-06 02:46:16', '2018-10-06 02:46:16'),
(317, '2994', 'ZULFAH SYAFARIANI MARSHA', 23, 284, 'Perempuan', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295749550', NULL, '2018-10-06 02:46:17', '2018-10-06 02:46:17'),
(318, '3223', 'AYU RAHMAWATI', 23, 285, 'Perempuan', 'Ds. Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081322128707', NULL, '2018-10-06 02:46:17', '2018-10-06 02:46:17'),
(319, '2768', 'ADINDA MAHARANI', 24, 286, 'Perempuan', 'Ds. Wanahayu, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220757778', NULL, '2018-10-06 02:46:33', '2018-10-06 02:46:33'),
(320, '2771', 'AEP SUDIANTO', 24, 287, 'Laki-Laki', 'Ds. Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085328038196', NULL, '2018-10-06 02:46:34', '2018-10-06 02:46:34'),
(321, '2778', 'ALDI FIRMAN FADILAH', 24, 288, 'Laki-Laki', 'Ds. Panenjoan, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082219310101', NULL, '2018-10-06 02:46:35', '2018-10-06 02:46:35'),
(322, '2786', 'ANDI MUHAMMAD ALIMI S', 24, 289, 'Laki-Laki', 'Ds. Panyingkiran,Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220141606', NULL, '2018-10-06 02:46:35', '2018-10-06 02:46:35'),
(323, '2792', 'ASEP SUPRIYADI', 24, 290, 'Laki-Laki', 'Ds. Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085328938196', NULL, '2018-10-06 02:46:36', '2018-10-06 02:46:36'),
(324, '2812', 'DHIYA AULIYA SALSABILA', 24, 291, 'Perempuan', 'Ds. Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295368643', NULL, '2018-10-06 02:46:36', '2018-10-06 02:46:36'),
(325, '2813', 'DIDA MAULANA NUGRAHA', 24, 292, 'Laki-Laki', 'Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315105179', NULL, '2018-10-06 02:46:37', '2018-10-06 02:46:37'),
(326, '2821', 'ERFINA HALFA KHOIRUNNISA', 24, 293, 'Perempuan', 'Ds. Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324437727', NULL, '2018-10-06 02:46:37', '2018-10-06 02:46:37'),
(327, '2852', 'HAPID WAHYUDIN', 24, 294, 'Laki-Laki', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353783921', NULL, '2018-10-06 02:46:38', '2018-10-06 02:46:38'),
(328, '2853', 'HASNAH NAJMAH ALFIA', 24, 295, 'Perempuan', 'Ds. Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315555070', NULL, '2018-10-06 02:46:38', '2018-10-06 02:46:38'),
(329, '2860', 'INDAH AMALIA PUTRI', 24, 296, 'Perempuan', 'Ds. Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081220380560', NULL, '2018-10-06 02:46:39', '2018-10-06 02:46:39'),
(330, '2885', 'JEMBAR PANGYUGA ABDILLAH', 24, 297, 'Laki-Laki', 'Kec. Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322564900', NULL, '2018-10-06 02:46:40', '2018-10-06 02:46:40'),
(331, '2869', 'KHARISMA LAZUARDI', 24, 298, 'Laki-Laki', 'Ds. Panenjoan, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089623409973', NULL, '2018-10-06 02:46:40', '2018-10-06 02:46:40'),
(332, '2877', 'MARIATUL ULFAH', 24, 299, 'Perempuan', 'Ds. Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:41', '2018-10-06 02:46:41'),
(333, '2880', 'MOCH ALIFH FAKHRI', 24, 300, 'Laki-Laki', 'Ds. Babakan Jawa, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081224128606', NULL, '2018-10-06 02:46:41', '2018-10-06 02:46:41'),
(334, '2882', 'MOCH RIDWAN', 24, 301, 'Laki-Laki', 'Ds. Ciparay, Leuwimunding', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127648952', NULL, '2018-10-06 02:46:42', '2018-10-06 02:46:42'),
(335, '2896', 'MUHAMAD IRHAM', 24, 302, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:42', '2018-10-06 02:46:42'),
(336, '2900', 'MUHAMMAD  FARAS ADY', 24, 303, 'Laki-Laki', 'Ds. Dukuh Pari, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321309990', NULL, '2018-10-06 02:46:43', '2018-10-06 02:46:43'),
(337, '2917', 'NAFASYA RAHMANDINI', 24, 304, 'Perempuan', 'Ds. Tenjolayar, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295753106', NULL, '2018-10-06 02:46:44', '2018-10-06 02:46:44'),
(338, '2920', 'NAIMATUL UDHIYAH', 24, 305, 'Perempuan', 'Ds. Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:44', '2018-10-06 02:46:44'),
(339, '2932', 'NURMALA DWIANI', 24, 306, 'Perempuan', 'Ds. Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315390689', NULL, '2018-10-06 02:46:45', '2018-10-06 02:46:45'),
(340, '2933', 'NURSAYID MAULID F.', 24, 307, 'Laki-Laki', 'Ds. Tomo, Tomo', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224223644', NULL, '2018-10-06 02:46:45', '2018-10-06 02:46:45'),
(341, '2934', 'ORIN DEWI AGUSTIANI', 24, 308, 'Perempuan', 'Ds. Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315690656', NULL, '2018-10-06 02:46:46', '2018-10-06 02:46:46'),
(342, '2935', 'PUTRI DHARA QISTIA', 24, 309, 'Perempuan', 'Jl. Gerakan Koperasi, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085294602102', NULL, '2018-10-06 02:46:46', '2018-10-06 02:46:46'),
(343, '2953', 'RIZAL SOPIYAN', 24, 310, 'Laki-Laki', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:47', '2018-10-06 02:46:47'),
(344, '2967', 'SELVY OKTAVIA', 24, 311, 'Perempuan', 'Ds. Bojong Cideres, Dawuan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089666051423', NULL, '2018-10-06 02:46:47', '2018-10-06 02:46:47'),
(345, '2997', 'MAULIDI ALIFIAN', 24, 312, 'Laki-Laki', 'Gg. Pajagalan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0813244661966', NULL, '2018-10-06 02:46:48', '2018-10-06 02:46:48'),
(346, '2779', 'ALDI KRISNADIJAYA', 24, 313, 'Laki-Laki', 'Ds. Kutamanggu, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082320644965', NULL, '2018-10-06 02:46:49', '2018-10-06 02:46:49'),
(347, '2809', 'DEDE GIRI SETIAWAN', 24, 314, 'Laki-Laki', 'Ds. Girimukti, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322070034', NULL, '2018-10-06 02:46:49', '2018-10-06 02:46:49'),
(348, '2825', 'EUIS KARLINA', 24, 315, 'Perempuan', 'Ds. Sidamukti, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321837235', NULL, '2018-10-06 02:46:50', '2018-10-06 02:46:50'),
(349, '2835', 'FATTIH SHAN GIBRAN ADHITYA', 24, 316, 'Laki-Laki', 'Ds. Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081214029972', NULL, '2018-10-06 02:46:50', '2018-10-06 02:46:50'),
(350, '2841', 'FUZAIRA', 24, 317, 'Perempuan', 'Ds. Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082317495900', NULL, '2018-10-06 02:46:51', '2018-10-06 02:46:51'),
(351, '2861', 'INDRI SEPTIANI', 24, 318, 'Perempuan', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085318497723', NULL, '2018-10-06 02:46:51', '2018-10-06 02:46:51'),
(352, '2881', 'MOCH FAJRIEN', 24, 319, 'Laki-Laki', 'Perum Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081312383491', NULL, '2018-10-06 02:46:52', '2018-10-06 02:46:52'),
(353, '4', 'MUHAMMAD NAUFAL ADITYA', 24, 320, 'Laki-Laki', 'Ds. Palasah, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322321889', NULL, '2018-10-06 02:46:52', '2018-10-06 02:46:52'),
(354, '2923', 'NAJWA PUTRI MAHARDIKA', 24, 321, 'Perempuan', 'Ds. Cibuaya, Cibuaya, Karawang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085846381971', NULL, '2018-10-06 02:46:53', '2018-10-06 02:46:53'),
(355, '2924', 'NOUVAL FAZA ARYA PUTRA', 24, 322, 'Laki-Laki', 'Kp. Cipeucang, Cileungsi, Bogor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:46:53', '2018-10-06 02:46:53'),
(356, '2925', 'NENG ELIS LISTIANI', 24, 323, 'Perempuan', 'Ds. Girimukti, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324865334', NULL, '2018-10-06 02:46:54', '2018-10-06 02:46:54'),
(357, '2960', 'ROFI FITRIYANI', 24, 324, 'Perempuan', 'Ds. Girimukti, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324336594', NULL, '2018-10-06 02:46:55', '2018-10-06 02:46:55'),
(358, '2969', 'SANDI SATRIA PERMANA', 24, 325, 'Laki-Laki', 'Ds. Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089509819822', NULL, '2018-10-06 02:46:55', '2018-10-06 02:46:55'),
(359, '3229', 'PUTRI AULIA ZAHRA', 24, 326, 'Perempuan', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224166023', NULL, '2018-10-06 02:46:56', '2018-10-06 02:46:56'),
(392, '2773', 'AFRIANI SRI DEWI', 19, 327, 'Perempuan', NULL, NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322886767', NULL, '2018-10-06 02:53:28', '2018-10-06 02:53:28'),
(393, '2782', 'ALIFAH SUKMA ASIH', 19, 328, 'Perempuan', 'Ds. Leuwiseeng, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0811230151', NULL, '2018-10-06 02:53:28', '2018-10-06 02:53:28'),
(394, '2784', 'ALYA AULIA NUR MUHAMAD', 19, 329, 'Perempuan', 'Ds. Jatiwangi, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221346983', NULL, '2018-10-06 02:53:29', '2018-10-06 02:53:29'),
(395, '2785', 'ALYSHA KHAIRUNNISA', 19, 330, 'Perempuan', 'Jl. Siti Armilah, Majalengka Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082130036452', NULL, '2018-10-06 02:53:30', '2018-10-06 02:53:30'),
(396, '2801', 'CRISNA SATRIA FEBRIANA', 19, 331, 'Laki-Laki', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127173611', NULL, '2018-10-06 02:53:30', '2018-10-06 02:53:30'),
(397, '2802', 'DAFFA FASYA SUMAWIJAYA', 19, 332, 'Laki-Laki', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081380981152', NULL, '2018-10-06 02:53:31', '2018-10-06 02:53:31'),
(398, '2827', 'FADLAN RESTU PRATAMA', 19, 333, 'Laki-Laki', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081319193262', NULL, '2018-10-06 02:53:31', '2018-10-06 02:53:31'),
(399, '2995', 'FAHMI NURDIN', 19, 334, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:53:32', '2018-10-06 02:53:32'),
(400, '2829', 'FAJRIN NABILA HAPID', 19, 335, 'Perempuan', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08121441131', NULL, '2018-10-06 02:53:32', '2018-10-06 02:53:32'),
(401, '2830', 'FARHAN ABDILLAH', 19, 336, 'Laki-Laki', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:53:33', '2018-10-06 02:53:33'),
(402, '2838', 'FERI SUTIYAWAN', 19, 337, 'Laki-Laki', 'Lingkungan Melati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089661173076', NULL, '2018-10-06 02:53:33', '2018-10-06 02:53:33'),
(403, '2842', 'GEULIS TIARA CUCU ELIAWATI', 19, 338, 'Perempuan', 'Ds. Jengah, Jatigede, Sumedang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324461757', NULL, '2018-10-06 02:53:34', '2018-10-06 02:53:34'),
(404, '2844', 'GIFFASYA AUFADHIA AGISTIYAS', 19, 339, 'Perempuan', 'Ds. Kamun, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082318792888', NULL, '2018-10-06 02:53:35', '2018-10-06 02:53:35'),
(405, '2857', 'IBNU SAHIBA', 19, 340, 'Laki-Laki', 'Majalengka Wetan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081956229762', NULL, '2018-10-06 02:53:35', '2018-10-06 02:53:35'),
(406, '2863', 'IQBAL SYAHRIAL ROSADI', 19, 341, 'Laki-Laki', 'Ds. Bojong, Dawuan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085222734124', NULL, '2018-10-06 02:53:36', '2018-10-06 02:53:36'),
(407, '2866', 'KAFKA AZZIKRA RAHMAN', 19, 342, 'Laki-Laki', 'Perum Cijati Residence No. A10', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082116421135', NULL, '2018-10-06 02:53:36', '2018-10-06 02:53:36'),
(408, '2867', 'KHALIFA ADIANI SYAM', 19, 343, 'Perempuan', 'Mujul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082116257455', NULL, '2018-10-06 02:53:37', '2018-10-06 02:53:37'),
(409, '2873', 'M ARSYAD NURSYAHID SARONI', 19, 344, 'Laki-Laki', 'Perum BCA Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085314344422', NULL, '2018-10-06 02:53:38', '2018-10-06 02:53:38'),
(410, '2878', 'MARWA SOPHIA NATHANIA', 19, 345, 'Perempuan', 'Ds. Karangsambung, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085311838035', NULL, '2018-10-06 02:53:38', '2018-10-06 02:53:38'),
(411, '2883', 'MOCH RIZKY RAMADHAN', 19, 346, 'Laki-Laki', NULL, NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:53:39', '2018-10-06 02:53:39'),
(412, '2889', 'MUHAMAD DAFFA RIZQULLAH', 19, 347, 'Laki-Laki', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321190111', NULL, '2018-10-06 02:53:39', '2018-10-06 02:53:39'),
(413, '2996', 'MUHAMMAD ALIF AL-FAWWAZ', 19, 348, 'Laki-Laki', 'Jl. Pemuda, Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324043531', NULL, '2018-10-06 02:53:40', '2018-10-06 02:53:40'),
(414, '2903', 'MUHAMMAD GIBRAN M.S.', 19, 349, 'Laki-Laki', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 02:53:40', '2018-10-06 02:53:40'),
(415, '2895', 'MUHAMMAD HAIKAL HAFID RANIDA', 19, 350, 'Laki-Laki', 'Majalengka Wetan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295501298', NULL, '2018-10-06 02:53:41', '2018-10-06 02:53:41'),
(416, '2909', 'MUHAMMAD WILDAN THOORIQ ARRISYAD', 19, 351, 'Laki-Laki', 'Sarijadi, Sukasari, Bandung', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087823455586', NULL, '2018-10-06 02:53:41', '2018-10-06 02:53:41'),
(417, '2938', 'RAFA NADA SITI NAJ ADILAH', 19, 352, 'Perempuan', 'Ds. Maja Utara, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224851977', NULL, '2018-10-06 02:53:42', '2018-10-06 02:53:42'),
(418, '2948', 'RIANTI AULIANI', 19, 353, 'Perempuan', 'Ds. Simpeureum, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315403052', NULL, '2018-10-06 02:53:42', '2018-10-06 02:53:42'),
(419, '2951', 'RIKA SRI ASIH HASANAH', 19, 354, 'Perempuan', 'Ds. Cicalung, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082320031983', NULL, '2018-10-06 02:53:43', '2018-10-06 02:53:43'),
(420, '2955', 'RIZQON MUSTHAPA AL NASR', 19, 355, 'Laki-Laki', 'Ds. Babakancuyu, Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324284375', NULL, '2018-10-06 02:53:44', '2018-10-06 02:53:44'),
(421, '2964', 'SALWA SAFINA NOOR ATHALA', 19, 356, 'Perempuan', 'Ds. Sindangwangi, Sindangwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082218179200', NULL, '2018-10-06 02:53:44', '2018-10-06 02:53:44'),
(422, '2973', 'SHIFA NUR AROFAH', 19, 357, 'Perempuan', 'Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322890410', NULL, '2018-10-06 02:53:45', '2018-10-06 02:53:45'),
(423, '2978', 'SITI INDIRA KHOEROTUNNISA', 19, 358, 'Perempuan', 'Ds. Sukahaji, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122391722', NULL, '2018-10-06 02:53:45', '2018-10-06 02:53:45'),
(424, '2981', 'SRI NANDA HIKMATULLOH', 19, 359, 'Perempuan', 'Ds. Jatipamor, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082116518812', NULL, '2018-10-06 02:53:46', '2018-10-06 02:53:46'),
(425, '2993', 'YUSAN NURUL SEPTIYANI', 19, 360, 'Perempuan', 'Ds. Liangjulang, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082219076628', NULL, '2018-10-06 02:53:46', '2018-10-06 02:53:46'),
(460, '2999', 'ABDULLAH TSANI AZZUHAIR', 11, 361, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:02:08', '2018-10-06 03:02:08'),
(461, '3000', 'ABID FAUZAN', 11, 362, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082315733622', NULL, '2018-10-06 03:02:08', '2018-10-06 03:02:08'),
(462, '3003', 'ADILA SALSABILA', 11, 363, 'Perempuan', 'Cicurug, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224104326', NULL, '2018-10-06 03:02:09', '2018-10-06 03:02:09'),
(463, '3004', 'ADINDA SHAIRA RISKI NASTARI SYAHRIR', 11, 364, 'Perempuan', 'Komplek Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089662270027', NULL, '2018-10-06 03:02:09', '2018-10-06 03:02:09'),
(464, '3008', 'AHMAD QOSIM KUSYAERI', 11, 365, 'Laki-Laki', 'Panyingkiran Kananga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081240193236', NULL, '2018-10-06 03:02:10', '2018-10-06 03:02:10'),
(465, '3021', 'AMELYARIHAN PUTRI HAULIYAN', 11, 366, 'Perempuan', 'Jl. Bojong, Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082214333088', NULL, '2018-10-06 03:02:11', '2018-10-06 03:02:11'),
(466, '3026', 'ANNA AGHITSNA NURFARAS D', 11, 367, 'Perempuan', 'Jl. Laswi, Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317237616', NULL, '2018-10-06 03:02:11', '2018-10-06 03:02:11'),
(467, '3031', 'ARVINA PUTRI RACHMAN', 11, 368, 'Perempuan', 'Jl. Suma, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324381111', NULL, '2018-10-06 03:02:12', '2018-10-06 03:02:12'),
(468, '3032', 'ARY APRILLA TARIGAN', 11, 369, 'Laki-Laki', 'Blok Margaharja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082218444554', NULL, '2018-10-06 03:02:12', '2018-10-06 03:02:12'),
(469, '3227', 'AZMI NUR SHIDIQ RIDWAN', 11, 370, 'Laki-Laki', 'Maja, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324731035', NULL, '2018-10-06 03:02:13', '2018-10-06 03:02:13'),
(470, '3041', 'BUNGA LAIL INSYIROH', 11, 371, 'Perempuan', 'Blok Kenanga, Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222155512', NULL, '2018-10-06 03:02:13', '2018-10-06 03:02:13'),
(471, '3044', 'DAFFA HAIBAN MUJAKKI', 11, 372, 'Laki-Laki', 'Jl. Babakan Koda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224499392', NULL, '2018-10-06 03:02:14', '2018-10-06 03:02:14'),
(472, '3046', 'DERIS DARMAWAN HENDARSAH', 11, 373, 'Laki-Laki', 'Jl. Pangeran Muhammad', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224933755', NULL, '2018-10-06 03:02:14', '2018-10-06 03:02:14'),
(473, '3079', 'GERA NURFAUZA', 11, 374, 'Laki-Laki', 'Panyingkiran, BTN Andir', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08112340076', NULL, '2018-10-06 03:02:15', '2018-10-06 03:02:15'),
(474, '3081', 'GEULIS KHARISMA PUTRI AL- HARIYATI', 11, 375, 'Perempuan', 'Perum Sindangkasih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395642126', NULL, '2018-10-06 03:02:15', '2018-10-06 03:02:15'),
(475, '3086', 'HADI AUDIANSYAH', 11, 376, 'Laki-Laki', 'Kadipaten, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082118942727', NULL, '2018-10-06 03:02:16', '2018-10-06 03:02:16'),
(476, '3090', 'HASNA NUR FAJRIYANTY', 11, 377, 'Perempuan', 'Panyingkiran, BTN Andir', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324339971', NULL, '2018-10-06 03:02:17', '2018-10-06 03:02:17'),
(477, '3092', 'HELSA MUTIARA RAHMAWATI', 11, 378, 'Perempuan', 'Cicenang, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320674724', NULL, '2018-10-06 03:02:17', '2018-10-06 03:02:17'),
(478, '3102', 'KANIA PUSPA KIRANI', 11, 379, 'Perempuan', 'Perum BCA, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221279980', NULL, '2018-10-06 03:02:18', '2018-10-06 03:02:18'),
(479, '3110', 'LUTHFIAH KHAIRUNNISA', 11, 380, 'Perempuan', 'Jl. Imam Bonjol', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221143109', NULL, '2018-10-06 03:02:18', '2018-10-06 03:02:18'),
(480, '3114', 'MARISA DWI LESTARI', 11, 381, 'Perempuan', 'Kodim 0617, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085223860211', NULL, '2018-10-06 03:02:19', '2018-10-06 03:02:19'),
(481, '3129', 'MUHAMMAD ISMUL ADHOM ZAKIYA FARID', 11, 382, 'Laki-Laki', 'Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122215360', NULL, '2018-10-06 03:02:20', '2018-10-06 03:02:20'),
(482, '3136', 'MUHAMMAD ROYHAN FARHAN HABIBI', 11, 383, 'Laki-Laki', 'Jl. Suma, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224446057', NULL, '2018-10-06 03:02:20', '2018-10-06 03:02:20'),
(483, '3148', 'NANA SEPTIANA', 11, 384, 'Laki-Laki', 'Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221395106', NULL, '2018-10-06 03:02:21', '2018-10-06 03:02:21'),
(484, '3149', 'NAURA NASHTIA KHANSA', 11, 385, 'Perempuan', 'Perum Munjul, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317731338', NULL, '2018-10-06 03:02:21', '2018-10-06 03:02:21'),
(485, '3153', 'NINDA LESTARI', 11, 386, 'Perempuan', 'Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224770925', NULL, '2018-10-06 03:02:22', '2018-10-06 03:02:22'),
(486, '3154', 'NISA FUZIANTI RAMADHAN', 11, 387, 'Perempuan', 'Jl. Pahlawan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224379905', NULL, '2018-10-06 03:02:22', '2018-10-06 03:02:22'),
(487, '3158', 'NURAZIZAH WIDIASIH', 11, 388, 'Perempuan', 'Gg. Panday, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082119621178', NULL, '2018-10-06 03:02:23', '2018-10-06 03:02:23'),
(488, '3162', 'PRAMUDHITA DWI PRASASTI', 11, 389, 'Perempuan', 'Tonjong, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082130738988', NULL, '2018-10-06 03:02:23', '2018-10-06 03:02:23'),
(489, '3163', 'PUTRI AZZAHRA', 11, 390, 'Perempuan', 'Ds. Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081312080146', NULL, '2018-10-06 03:02:24', '2018-10-06 03:02:24'),
(490, '3170', 'Rd. ZAIDAN AKMAL FADHLULLOH', 11, 391, 'Laki-Laki', 'Perum BCA, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127310027', NULL, '2018-10-06 03:02:24', '2018-10-06 03:02:24'),
(491, '3172', 'RESTU ADJI PAMUNGKAS', 11, 392, 'Laki-Laki', 'Komplek Pusaka Indah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222555540', NULL, '2018-10-06 03:02:25', '2018-10-06 03:02:25'),
(492, '3183', 'RIZKY ADINUL AKBAR', 11, 393, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089618813813', NULL, '2018-10-06 03:02:26', '2018-10-06 03:02:26'),
(493, '3215', 'WILDAN ZHILAL MANAFI', 11, 394, 'Laki-Laki', 'Ds. Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085318415779', NULL, '2018-10-06 03:02:26', '2018-10-06 03:02:26'),
(494, '3230', 'ABDI FADILAH', 7, 395, 'Laki-Laki', 'Babakan Manjeti', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089505411827', NULL, '2018-10-06 03:13:44', '2018-10-06 03:13:44'),
(495, '32393239', 'AINY NUR FATIHATI', 7, 396, 'Perempuan', 'Cikijing', 'Majalengka', '2018-10-06', 'noimage.jpg', NULL, NULL, NULL, NULL, '085215526738', NULL, '2018-10-06 03:13:45', '2018-10-06 03:17:09'),
(496, '3240', 'AKMAL FEBRIAN', 7, 397, 'Laki-Laki', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085219464915', NULL, '2018-10-06 03:13:45', '2018-10-06 03:13:45'),
(497, '3241', 'ALIFA DWI MUTIA', 7, 398, 'Perempuan', 'Kertajati', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:46', '2018-10-06 03:13:46'),
(498, '3249', 'ANGGA SUWANDI', 7, 399, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:46', '2018-10-06 03:13:46'),
(499, '3260', 'BINTANG ANANDITA', 7, 400, 'Perempuan', 'Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295316090', NULL, '2018-10-06 03:13:47', '2018-10-06 03:13:47'),
(500, '3263', 'CUCU SUMIATI', 7, 401, 'Perempuan', 'Indramayu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081313167073', NULL, '2018-10-06 03:13:47', '2018-10-06 03:13:47'),
(501, '3276', 'DIKY FERNANDO', 7, 402, 'Laki-Laki', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:48', '2018-10-06 03:13:48'),
(502, '3280', 'DZIKRY DAAFIQ NURROHMAN', 7, 403, 'Laki-Laki', 'Jatinangor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:48', '2018-10-06 03:13:48'),
(503, '3282', 'EDELWEISS ABDAH AL-BARR YUSTIKA', 7, 404, 'Perempuan', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395556331', NULL, '2018-10-06 03:13:49', '2018-10-06 03:13:49'),
(504, '3283', 'EDO EDWAR SUTANTO', 7, 405, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:49', '2018-10-06 03:13:49'),
(505, '3289', 'EVA HIKMATUL AULYA', 7, 406, 'Perempuan', 'Ds. Kadu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085289702155', NULL, '2018-10-06 03:13:50', '2018-10-06 03:13:50'),
(506, '3295', 'FANYA PUTRI NAYANDA', 7, 407, 'Perempuan', 'Cieurih, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353511761', NULL, '2018-10-06 03:13:50', '2018-10-06 03:13:50'),
(507, '3306', 'GATAN FIRMANSYAH', 7, 408, 'Laki-Laki', 'Leuwikidang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:51', '2018-10-06 03:13:51'),
(508, '3312', 'GILANG ADITYA RAMADHAN', 7, 409, 'Laki-Laki', 'Ds. Pasirmuncang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221102239', NULL, '2018-10-06 03:13:51', '2018-10-06 03:13:51'),
(509, '3314', 'GINA ZAHRA ZAFIRA', 7, 410, 'Perempuan', 'Sukaraja Kulon', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295122737', NULL, '2018-10-06 03:13:52', '2018-10-06 03:13:52'),
(510, '3324', 'IKBARINO YUSUF HARIMAN', 7, 411, 'Laki-Laki', 'Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:53', '2018-10-06 03:13:53'),
(511, '3327', 'INTAN RAHMAH NURAINI', 7, 412, 'Perempuan', 'Karawang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:54', '2018-10-06 03:13:54'),
(512, '3328', 'IQBAL MAULANA NUR IMAN', 7, 413, 'Laki-Laki', 'Lebaksiuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:54', '2018-10-06 03:13:54'),
(513, '3334', 'KURSIATI', 7, 414, 'Perempuan', 'Indramayu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081912907883', NULL, '2018-10-06 03:13:55', '2018-10-06 03:13:55'),
(514, '3335', 'LATIFAH MUTIARA INDAH', 7, 415, 'Perempuan', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:55', '2018-10-06 03:13:55'),
(515, '3345', 'M. REZZA NUGRAHA A.', 7, 416, 'Laki-Laki', 'Lemah Putih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:56', '2018-10-06 03:13:56'),
(516, '3364', 'MUHAMMAD AMAR MUSHLIH', 7, 417, 'Laki-Laki', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395890233', NULL, '2018-10-06 03:13:56', '2018-10-06 03:13:56'),
(517, '3368', 'MUHAMMAD IKHSAN', 7, 418, 'Laki-Laki', 'Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:57', '2018-10-06 03:13:57'),
(518, '3394', 'NURUL FADHILAH S', 7, 419, 'Perempuan', 'Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085223248477', NULL, '2018-10-06 03:13:57', '2018-10-06 03:13:57'),
(519, '3411', 'REGINA CECILIA RINADI', 7, 420, 'Perempuan', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082321444559', NULL, '2018-10-06 03:13:58', '2018-10-06 03:13:58'),
(520, '3419', 'RIZKY KARSA PUTRA', 7, 421, 'Laki-Laki', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08968630130', NULL, '2018-10-06 03:13:58', '2018-10-06 03:13:58'),
(521, '3429', 'SARIATUL MAGFIROH', 7, 422, 'Perempuan', 'Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:59', '2018-10-06 03:13:59'),
(522, '3431', 'SATYA JAYA PUTRA', 7, 423, 'Laki-Laki', 'Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:13:59', '2018-10-06 03:13:59'),
(523, '3433', 'SHELA ROISATUN NISA', 7, 424, 'Perempuan', 'Sindang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085864757427', NULL, '2018-10-06 03:14:00', '2018-10-06 03:14:00'),
(524, '3436', 'SINDI PUTRI NURALIZA', 7, 425, 'Perempuan', 'Banjaran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220796125', NULL, '2018-10-06 03:14:01', '2018-10-06 03:14:01'),
(525, '3445', 'SYARIEF NUR HIDAYAT HASANUDIN', 7, 426, 'Laki-Laki', 'Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:14:01', '2018-10-06 03:14:01'),
(526, '3454', 'WIDDY WIDIYARTINI RUSTANDI', 7, 427, 'Perempuan', 'Ds. Cijurey, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:14:02', '2018-10-06 03:14:02'),
(527, '3463', 'ZALFA DHIYA PRAYUDI', 7, 428, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220416662', NULL, '2018-10-06 03:14:02', '2018-10-06 03:14:02'),
(563, '3224', 'ALLISA PUTRI AMANDA', 1, 429, 'Perempuan', 'Jl. Olahraga Gg. Simpay Asih No.05', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081224272800', NULL, '2018-10-06 03:16:31', '2018-10-06 03:16:31'),
(564, '3245', 'ALYA RAHMAWATI', 1, 430, 'Perempuan', 'Jatiraga Timur RT.03 RW.03, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082118081166', NULL, '2018-10-06 03:16:32', '2018-10-06 03:16:32'),
(565, '3253', 'ARDINA NUR FATIMAH', 1, 431, 'Perempuan', 'Perum BCA RT.11 RW.06 Jl. Mangga Raya N0.72, Cikalong, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082128882517', NULL, '2018-10-06 03:16:32', '2018-10-06 03:16:32'),
(566, '3254', 'ARVIN FARAND FIRJATULLAH', 1, 432, 'Laki-Laki', 'Jl. Gerakn Koperasi RT.01 RW.04 Ling. Giri Asih, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081214279919', NULL, '2018-10-06 03:16:33', '2018-10-06 03:16:33'),
(567, '3256', 'ASHFA NAYLA NAJA', 1, 433, 'Perempuan', 'Jl. Veteran No. 89 RT.01 RW.01 Blok Ahad, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081224387834', NULL, '2018-10-06 03:16:33', '2018-10-06 03:16:33'),
(569, '3266', 'DARYNNATA NUGRAHA', 1, 434, 'Laki-Laki', 'Jl. Siti Armilah No.8', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08112401339', NULL, '2018-10-06 03:16:34', '2018-10-06 03:16:34'),
(570, '3278', 'DINDA NURFELIZA SUHANDI', 1, 435, 'Perempuan', 'Jl. Ahmad Kusuma RT.05 RW.01', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081283370449', NULL, '2018-10-06 03:16:35', '2018-10-06 03:16:35'),
(571, '3279', 'DREANTAMA MISBACHUDIN YUSMAN', 1, 436, 'Laki-Laki', 'Jl. Pejuang 45 Blok Ahad RT.007 RW.003 Ds. Kulur', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081311581054', NULL, '2018-10-06 03:16:36', '2018-10-06 03:16:36'),
(572, '3296', 'FARHAN FIRJATULLAH', 1, 437, 'Laki-Laki', 'Ling. Leuwilenggik RT.019 RW.010 Kel. Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221025952', NULL, '2018-10-06 03:16:36', '2018-10-06 03:16:36'),
(573, '3305', 'GAHARA RESTU PAMUJI', 1, 438, 'Laki-Laki', 'Ling. Sirahdayeuh RT.03 RW.01, Cicenang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089660635555', NULL, '2018-10-06 03:16:37', '2018-10-06 03:16:37'),
(574, '3315', 'GINAYASYA RIZKY FATHIHA', 1, 439, 'Perempuan', 'Jl. Sangadipa No.46 RT.004 RW.004 Blok Sabtu Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082240140178', NULL, '2018-10-06 03:16:37', '2018-10-06 03:16:37'),
(575, '3322', 'HILMY ABDILLAH MUNGGARAN', 1, 440, 'Laki-Laki', 'Cijati RT.01 RW.04', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320413538', NULL, '2018-10-06 03:16:38', '2018-10-06 03:16:38'),
(576, '3326', 'IMA ALIMA SABILA RAHMA', 1, 441, 'Perempuan', 'Jl. Blok Senin Gandok RT.002 RW.002 Ds. Kertabasuki, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081386432424', NULL, '2018-10-06 03:16:38', '2018-10-06 03:16:38'),
(577, '3331', 'KARTIKA MAULIDA AZZAHRA', 1, 442, 'Perempuan', 'Ds. Kutamanggu RT.08 RW.03', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127685165', NULL, '2018-10-06 03:16:39', '2018-10-06 03:16:39'),
(578, '3349', 'MALIK BARKAH ARRAYHAN', 1, 443, 'Laki-Laki', 'Blok Karapyak No.32 Panglayunga, RT.12 RW.06 Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320077015', NULL, '2018-10-06 03:16:39', '2018-10-06 03:16:39'),
(579, '3351', 'MAULANA YUSUF ISKANDAR', 1, 444, 'Laki-Laki', 'Jl. Cicenang RT.02 RW.05', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082217070054', NULL, '2018-10-06 03:16:40', '2018-10-06 03:16:40'),
(580, '3354', 'MOCHAMAD AZID MUSLIM', 1, 445, 'Laki-Laki', 'Blok Sangkanhurip RT.003 Rw.001 Ds. Pasirmuncang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085219041517', NULL, '2018-10-06 03:16:40', '2018-10-06 03:16:40'),
(581, '3355', 'MOCHAMAD RIZKY MAULANA', 1, 446, 'Laki-Laki', 'Ling. Mekarjaya RT.002 RW.001 No.67 Kel. Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085317784264', NULL, '2018-10-06 03:16:41', '2018-10-06 03:16:41'),
(582, '3367', 'MUHAMMAD HASBI HARDIAN', 1, 447, 'Laki-Laki', 'Jl. Jatisampay Gg. Misna No.18', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085280123782', NULL, '2018-10-06 03:16:41', '2018-10-06 03:16:41'),
(583, '3369', 'MUHAMMAD KAISAR WIBAWA UTAMA', 1, 448, 'Laki-Laki', 'Blok Senin RT.13 RW.05 Ds. Tarikolot, Palasah', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085322070585', NULL, '2018-10-06 03:16:42', '2018-10-06 03:16:42'),
(584, '3372', 'MUHAMMAD QALBI', 1, 449, 'Laki-Laki', 'Jl. Abdul Gani No.9', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320089512', NULL, '2018-10-06 03:16:43', '2018-10-06 03:16:43'),
(585, '3377', 'NABILA AZZAHRA', 1, 450, 'Perempuan', 'Ds. Cibulan Kec. Lemahsugih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085221421000', NULL, '2018-10-06 03:16:43', '2018-10-06 03:16:43'),
(586, '3382', 'NAISA SYAHRA SALSABILA', 1, 451, 'Perempuan', 'Blok Ciandeu RT.02 RW.14 Ds. Sidamukti', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082137962886', NULL, '2018-10-06 03:16:44', '2018-10-06 03:16:44'),
(587, '3384', 'NASYA NAZIBA RAHMADYANA', 1, 452, 'Perempuan', 'Blok Cirahayu RT.01 RW.01 Ds. Baribis, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087761543050', NULL, '2018-10-06 03:16:44', '2018-10-06 03:16:44'),
(588, '3385', 'NASYA ZASKYA HAFIZHA', 1, 453, 'Perempuan', 'Blok Sabtu Rt.01 RW.01 Ds. Heuleut', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324756586', NULL, '2018-10-06 03:16:45', '2018-10-06 03:16:45'),
(589, '3396', 'OKTAVIA REGITA GAYANTRI', 1, 454, 'Perempuan', 'Jl. Margaraharja RT.09 RW.03 Kel. Cicurug', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127895585', NULL, '2018-10-06 03:16:45', '2018-10-06 03:16:45'),
(590, '3403', 'RAFA ABIYASA DARMAJATI', 1, 455, 'Laki-Laki', 'Jl. K.H. Abdul Halim No.238', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320625279', NULL, '2018-10-06 03:16:46', '2018-10-06 03:16:46'),
(591, '3425', 'SANDRINA FADYA DIZA', 1, 456, 'Perempuan', 'Ling. Sirahdayeuh RT.05 RW.02, Cicenang, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295511487', NULL, '2018-10-06 03:16:46', '2018-10-06 03:16:46'),
(592, '3427', 'SANI NAFILAH MARDANI', 1, 457, 'Perempuan', 'Gg. Anggur No.442 Perum Sindangkasih', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321591216', NULL, '2018-10-06 03:16:47', '2018-10-06 03:16:47'),
(593, '3458', 'ZACKY ALFAUZA', 1, 458, 'Laki-Laki', 'Jl. Kartini Gg. Rahayu 1 No.262', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085810189879', NULL, '2018-10-06 03:16:48', '2018-10-06 03:16:48'),
(594, '3459', 'ZAFIRA LEXA IRAWAN', 1, 459, 'Perempuan', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:16:48', '2018-10-06 03:16:48'),
(595, '3461', 'ZAHRA SAFINATUN NAJA', 1, 460, 'Perempuan', 'Jl. Letkol Abdul Gani Gg. Haji Oman No.62', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081222092579', NULL, '2018-10-06 03:16:49', '2018-10-06 03:16:49'),
(596, '3462', 'ZAKI IMAMUL UMAM', 1, 461, 'Laki-Laki', 'Ling. Mekarjaya RT.002 RW.001 No.43 Kel. Tonjong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295896737', NULL, '2018-10-06 03:16:50', '2018-10-06 03:16:50'),
(597, '3464', 'ZASKIA AMELIA NURUDIN', 1, 462, 'Perempuan', 'Blok Babakan Jatimulya RT.08 RW.04 Jatipamor', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082240899688', NULL, '2018-10-06 03:16:50', '2018-10-06 03:16:50'),
(603, '3239', 'BILQIS NUR SALSABILA', 1, 463, 'Perempuan', 'Perum Mutiara Asri II, Blok B No.3 RT.003 RW.001 Ds. Kutamanggu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081283690766', NULL, '2018-10-06 03:17:20', '2018-10-06 03:17:20'),
(633, '3231', 'ADHWA IFTI LABIBA', 4, 464, 'Perempuan', 'Villa Sangraja Permai, Cigasong', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081318605645', NULL, '2018-10-06 03:19:19', '2018-10-06 03:19:19'),
(634, '3232', 'ADZWA SHAFIRA MARYAMAH', 4, 465, 'Perempuan', 'Jl. Pemuda, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '0895373897124', NULL, '2018-10-06 03:19:19', '2018-10-06 03:19:19'),
(635, '3235', 'AHMAD ARIF AL FATIH', 4, 466, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:19:20', '2018-10-06 03:19:20'),
(636, '3248', 'ANDIKA SILVA PRIAMBUDI', 4, 467, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081250343388', NULL, '2018-10-06 03:19:20', '2018-10-06 03:19:20'),
(637, '3251', 'ANISA NURUL HIDAYAH', 4, 468, 'Perempuan', 'Jatipamor RT.01 RW.01', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320661230', NULL, '2018-10-06 03:19:21', '2018-10-06 03:19:21'),
(638, '3261', 'CHANDRA REZA NUGRAHA', 4, 469, 'Laki-Laki', 'Leuwiseeng', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395510955', NULL, '2018-10-06 03:19:22', '2018-10-06 03:19:22'),
(639, '3264', 'DAFFA MUAFFI NUGIANSYAH', 4, 470, 'Laki-Laki', 'Pinangraja, Sukaraja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320206311', NULL, '2018-10-06 03:19:22', '2018-10-06 03:19:22'),
(640, '3272', 'DHEANASYA PUTRI ADIWIGUNA', 4, 471, 'Perempuan', 'Leuwiseeng, Dusun 2 RT.05 RW.03', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224574897', NULL, '2018-10-06 03:19:23', '2018-10-06 03:19:23'),
(641, '3275', 'DIFALDO RIZKY DARMAWAN PUTRA', 4, 472, 'Laki-Laki', 'Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:19:23', '2018-10-06 03:19:23'),
(642, '3292', 'FAIQ AZIZ ALFARIZ', 4, 473, 'Laki-Laki', 'Ds. Kutamanggu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224933944', NULL, '2018-10-06 03:19:24', '2018-10-06 03:19:24'),
(643, '3294', 'FAISAL WIJAYA KUSUMA YANUAR', 4, 474, 'Laki-Laki', 'Jl. Suma No.4 RT.02 RW.02 Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085664252516', NULL, '2018-10-06 03:19:25', '2018-10-06 03:19:25'),
(644, '3302', 'FAUZAN ZULFA ISNAIN', 4, 475, 'Laki-Laki', 'Cijati Pusaka Indah RT.03 Rw.08', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085211519911', NULL, '2018-10-06 03:19:25', '2018-10-06 03:19:25'),
(645, '3303', 'FAUZI NUGRAHA', 4, 476, 'Laki-Laki', 'Perum BTN Munjul No.24', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324025754', NULL, '2018-10-06 03:19:26', '2018-10-06 03:19:26'),
(646, '3332', 'KEISYA SYIFA RIMADHANI', 4, 477, 'Perempuan', 'Blok Kirang, Pasirmuncang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224731877', NULL, '2018-10-06 03:19:26', '2018-10-06 03:19:26'),
(647, '3333', 'KHOFIF SHALAHY', 4, 478, 'Laki-Laki', 'Pakubeureum, Kertajati RT.06 RW.02', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081320464922', NULL, '2018-10-06 03:19:27', '2018-10-06 03:19:27'),
(648, '3344', 'M. NAUFAL DZAKY', 4, 479, 'Laki-Laki', 'Perum Griya Prima Pesona, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220042952', NULL, '2018-10-06 03:19:27', '2018-10-06 03:19:27'),
(649, '3350', 'MATSNA RUWAIDA', 4, 480, 'Perempuan', 'Cikasarung, Tarikolot', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081394677798', NULL, '2018-10-06 03:19:28', '2018-10-06 03:19:28'),
(650, '3352', 'MIRA CHINTYA', 4, 481, 'Perempuan', 'Jatipamor RT.04 RW.02', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085324917776', NULL, '2018-10-06 03:19:29', '2018-10-06 03:19:29'),
(651, '3360', 'MUHAMAD RIZKI FATAHILAH', 4, 482, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085797274204', NULL, '2018-10-06 03:19:29', '2018-10-06 03:19:29'),
(652, '3371', 'MUHAMMAD MAULANA HABIBIE', 4, 483, 'Laki-Laki', 'Jl. Suma No.90 A Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081321438828', NULL, '2018-10-06 03:19:30', '2018-10-06 03:19:30'),
(653, '3374', 'MUTIARA DEVIANA PUTRI', 4, 484, 'Perempuan', 'Babakan Koda RT.15 RW.06', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '089647666747', NULL, '2018-10-06 03:19:30', '2018-10-06 03:19:30'),
(654, '3380', 'NAHLA ROSDIANA', 4, 485, 'Perempuan', 'Majasari, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081288133244', NULL, '2018-10-06 03:19:31', '2018-10-06 03:19:31'),
(655, '3381', 'NAILAN NURUL HAQUE', 4, 486, 'Perempuan', 'Leuwiseeng', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085218349670', NULL, '2018-10-06 03:19:31', '2018-10-06 03:19:31'),
(656, '3395', 'NURUL SHALWA MAULANI', 4, 487, 'Perempuan', 'Jl. Babakan Jawa, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087744403526', NULL, '2018-10-06 03:19:32', '2018-10-06 03:19:32'),
(657, '3400', 'PUTRA PRATAMA F', 4, 488, 'Laki-Laki', 'Ds. Jatiraga Timur No.10 Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220117085', NULL, '2018-10-06 03:19:33', '2018-10-06 03:19:33'),
(658, '3402', 'QOMARUDIN MA\'ARIJ', 4, 489, 'Laki-Laki', 'Jatisawit, Kasokandel', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081395545093', NULL, '2018-10-06 03:19:33', '2018-10-06 03:19:33'),
(659, '3416', 'RIFAN FITRIYADI. S', 4, 490, 'Laki-Laki', 'Jl. Gerakan Koperasi RT.04 RW.04 Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085377027023', NULL, '2018-10-06 03:19:34', '2018-10-06 03:19:34'),
(660, '3420', 'RODHIYA RAHMANI', 4, 491, 'Laki-Laki', 'Sukaraja Kulon, Cikonde RT.01 RW.02', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082127440390', NULL, '2018-10-06 03:19:34', '2018-10-06 03:19:34'),
(661, '3421', 'SALFA FAHRUN JIONISA', 4, 492, 'Perempuan', 'Jl. Pramuka No.3 Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085320363402', NULL, '2018-10-06 03:19:35', '2018-10-06 03:19:35'),
(662, '3437', 'SINDRI KLAUDIA JUNIAR', 4, 493, 'Perempuan', 'Jl. Letkol Abdul Gani Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224597989', NULL, '2018-10-06 03:19:36', '2018-10-06 03:19:36'),
(663, '3438', 'SINTHIA AFRILIANI', 4, 494, 'Perempuan', 'Babakan Koda RT.15 RW.06', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082316040613', NULL, '2018-10-06 03:19:36', '2018-10-06 03:19:36'),
(664, '3455', 'YOSI ARTIKA DEWI', 4, 495, 'Perempuan', 'Jl. Siti Armilah, Munjul', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085316000300', NULL, '2018-10-06 03:19:37', '2018-10-06 03:19:37'),
(665, '3460', 'ZAHRA ISAMAYA ANWAR', 4, 496, 'Perempuan', 'Jl. Siti Armilah RT.03 RW.08', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081312204701', NULL, '2018-10-06 03:19:37', '2018-10-06 03:19:37'),
(666, '3234', 'AGNI NURAENI', 6, 497, 'Perempuan', 'Sumedang, Paseh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081322045060', NULL, '2018-10-06 03:21:01', '2018-10-06 03:21:01');
INSERT INTO `students` (`id`, `nis`, `name`, `grade_id`, `pin`, `kelamin`, `alamat`, `tmplahir`, `tgllahir`, `foto`, `kota`, `tahun_masuk`, `nohp`, `nmortu`, `nohp_ortu`, `nowa`, `created_at`, `updated_at`) VALUES
(667, '3262', 'CHELSEA SETIANA DEVANA', 6, 498, 'Perempuan', 'Tluk Jambe, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082317941386', NULL, '2018-10-06 03:21:02', '2018-10-06 03:21:02'),
(668, '3273', 'DHEMARA ANJANI', 6, 499, 'Perempuan', 'Ciborelang, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085220463426', NULL, '2018-10-06 03:21:02', '2018-10-06 03:21:02'),
(669, '3277', 'DINDA AISHA AMARITA', 6, 500, 'Perempuan', 'Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353798833', NULL, '2018-10-06 03:21:03', '2018-10-06 03:21:03'),
(670, '3286', 'EKASANTI', 6, 501, 'Perempuan', 'Cibodas, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085210072021', NULL, '2018-10-06 03:21:04', '2018-10-06 03:21:04'),
(671, '3297', 'FARRAZ AZMAN MUYASAR', 6, 502, 'Laki-Laki', 'Cijati, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082318853121', NULL, '2018-10-06 03:21:04', '2018-10-06 03:21:04'),
(672, '3304', 'FERDIANSYAH AHMAD FATUROHMAN', 6, 503, 'Laki-Laki', 'Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:21:05', '2018-10-06 03:21:05'),
(673, '3316', 'HAFID MAULANA PRATAMA', 6, 504, 'Laki-Laki', 'Gunung Kuning, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085722812569', NULL, '2018-10-06 03:21:05', '2018-10-06 03:21:05'),
(674, '3317', 'HAFIZH AULIA AL BUKHORI', 6, 505, 'Laki-Laki', 'Tegalsari, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085295855110', NULL, '2018-10-06 03:21:06', '2018-10-06 03:21:06'),
(675, '3318', 'HANIFAH DURROTUL LAILI', 6, 506, 'Perempuan', 'Heuleut, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085315240101', NULL, '2018-10-06 03:21:06', '2018-10-06 03:21:06'),
(676, '3319', 'HAURA SEKAR RENATA', 6, 507, 'Perempuan', 'Burujul Wetan, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081221524141', NULL, '2018-10-06 03:21:07', '2018-10-06 03:21:07'),
(677, '3320', 'HAZZAZ FAIZ ZAKARIA', 6, 508, 'Laki-Laki', 'Pilangsari, Jatitujuh', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085864684357', NULL, '2018-10-06 03:21:07', '2018-10-06 03:21:07'),
(678, '3321', 'HIBAN MUFTI HADHROMI', 6, 509, 'Laki-Laki', 'Jatiraga Timur, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081930000025', NULL, '2018-10-06 03:21:08', '2018-10-06 03:21:08'),
(679, '3323', 'IKBAL IRSYADUL MALIK', 6, 510, 'Laki-Laki', 'Cibentar, Sukaraja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081394942627', NULL, '2018-10-06 03:21:09', '2018-10-06 03:21:09'),
(680, '3336', 'LISA SAHARA', 6, 511, 'Perempuan', 'Leuwimunding, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082216158757', NULL, '2018-10-06 03:21:09', '2018-10-06 03:21:09'),
(681, '3340', 'LUTHFIA NURKHALIFATUL JANNAH', 6, 512, 'Perempuan', 'Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082320951195', NULL, '2018-10-06 03:21:10', '2018-10-06 03:21:10'),
(682, '3362', 'MUHAMMAD AKASYAH HASAN ZAENI', 6, 513, 'Laki-Laki', 'Karayunan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-06 03:21:10', '2018-10-06 03:21:10'),
(683, '3370', 'MUHAMMAD MALIK HASAN', 6, 514, 'Laki-Laki', 'Cirebon, Plered', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085224355262', NULL, '2018-10-06 03:21:11', '2018-10-06 03:21:11'),
(684, '3376', 'NABIL KHAIRULLAH NABHANI OKTAR', 6, 515, 'Laki-Laki', 'Sukaraja Wetan, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085872909034', NULL, '2018-10-06 03:21:11', '2018-10-06 03:21:11'),
(685, '3388', 'NAYLA ARBANI', 6, 516, 'Perempuan', 'Ciborelang, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085257783474', NULL, '2018-10-06 03:21:12', '2018-10-06 03:21:12'),
(686, '3389', 'NAZWA NURUL ITSNAINI', 6, 517, 'Perempuan', 'Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08818365735', NULL, '2018-10-06 03:21:12', '2018-10-06 03:21:12'),
(687, '3392', 'NUR ANNISA FADILAH', 6, 518, 'Perempuan', 'Sutawangi, Jatiwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085316721140', NULL, '2018-10-06 03:21:13', '2018-10-06 03:21:13'),
(688, '3398', 'PAWAZ YUSUP PADHILAH', 6, 519, 'Laki-Laki', 'Lengkong Kulon, Sindangwangi', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085864898744', NULL, '2018-10-06 03:21:13', '2018-10-06 03:21:13'),
(689, '3408', 'RAKHA MAHSA AZZAHRI', 6, 520, 'Laki-Laki', 'Anjatan, Indramayu', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '083824889803', NULL, '2018-10-06 03:21:14', '2018-10-06 03:21:14'),
(690, '3413', 'RESTIANA RAMADHAN', 6, 521, 'Perempuan', 'Sunia, Talaga', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '082319643893', NULL, '2018-10-06 03:21:14', '2018-10-06 03:21:14'),
(691, '3414', 'RIANTO FAELANI', 6, 522, 'Laki-Laki', 'Jatigede, Sumedang', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08522392677', NULL, '2018-10-06 03:21:15', '2018-10-06 03:21:15'),
(692, '3417', 'RIJAL AHMAD IHSAN FAUZI', 6, 523, 'Laki-Laki', 'Dawuan, Kadipaten', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085353816685', NULL, '2018-10-06 03:21:16', '2018-10-06 03:21:16'),
(693, '3418', 'RISMA DWITYA NURUL AINI', 6, 524, 'Perempuan', 'Jl. Olahraga, Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '08122314777', NULL, '2018-10-06 03:21:16', '2018-10-06 03:21:16'),
(694, '3426', 'SANDRYNA IKHSANI SYAFITRI', 6, 525, 'Perempuan', 'Panyingkiran, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081324938418', NULL, '2018-10-06 03:21:17', '2018-10-06 03:21:17'),
(695, '3435', 'SIFFA FAUZIAH', 6, 526, 'Perempuan', 'Gunung Kuning, Majalengka', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '081219022563', NULL, '2018-10-06 03:21:17', '2018-10-06 03:21:17'),
(696, '3448', 'TIO BAGAS SYAFARI', 6, 527, 'Laki-Laki', 'Ciomas, Sukahaji', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085391109293', NULL, '2018-10-06 03:21:18', '2018-10-06 03:21:18'),
(697, '3449', 'VINA DWI SEPTIANI', 6, 528, 'Perempuan', 'Jatiserang, Panyingkiran', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085321114699', NULL, '2018-10-06 03:21:18', '2018-10-06 03:21:18'),
(698, '3451', 'WAFA AULIA AZZAHRA', 6, 529, 'Perempuan', 'Majalengka Wetan', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '087723001312', NULL, '2018-10-06 03:21:19', '2018-10-06 03:21:19'),
(699, '3465', 'ZULFA KHAIRIYATU RIYADI', 6, 530, 'Perempuan', 'Paniis, Maja', NULL, NULL, 'noimage.jpg', NULL, NULL, NULL, NULL, '085323404163', NULL, '2018-10-06 03:21:19', '2018-10-06 03:21:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Ari Koepp PhD', 'camylle09', 'zkertzmann@example.org', '$2y$10$obYfoCOm7cPL9zKc1hLRReWL5eF1QZncOaudUl73SHownPySyMh8G', 'SLBrDKXP17', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(2, 'Jamarcus Leffler', 'goldner.jessyca', 'addison55@example.org', '$2y$10$9dVPvj20mTGsuI/rdAVxqu4n5ogXt4CO1wpu6RtT8PNs4m0zergRK', 'nnwOyqOO6O', '2018-10-06 01:49:55', '2018-10-06 01:49:55'),
(3, 'Jeff Marvin V', 'khalil.borer', 'oconnell.betsy@example.com', '$2y$10$Q3ptS0.S3/MC4tMgHX6FD.Es74iHSgUdpkfHkg0KsAMcE8l8YmsZO', 'daBEaz8JS0', '2018-10-06 01:49:55', '2018-10-06 01:49:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alumni_nis_unique` (`nis`),
  ADD KEY `alumni_grade_id_foreign` (`grade_id`);

--
-- Indexes for table `attedances`
--
ALTER TABLE `attedances`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attedances_kode_unique` (`kode`),
  ADD KEY `attedances_student_id_foreign` (`student_id`);

--
-- Indexes for table `fingerprint`
--
ALTER TABLE `fingerprint`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fingerprint_ip_address_unique` (`ip_address`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_user_id_foreign` (`user_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `students_nis_unique` (`nis`),
  ADD UNIQUE KEY `students_pin_unique` (`pin`),
  ADD KEY `students_grade_id_foreign` (`grade_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `attedances`
--
ALTER TABLE `attedances`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fingerprint`
--
ALTER TABLE `fingerprint`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=700;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `alumni`
--
ALTER TABLE `alumni`
  ADD CONSTRAINT `alumni_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attedances`
--
ALTER TABLE `attedances`
  ADD CONSTRAINT `attedances_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `students` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
