<?php

namespace App\Helpers;
use App\Helpers\ssfd_CheckLogin;
use App\Helpers\ssfd_FindString;
use App\Helpers\ssfd_CurlExecution;
use App\Helpers\ssfd_Cookiejar;

class ssfd_Reboot
{

	public static function Reboot($param)
	{
		$page = ssfd_Cookiejar::GetCookie($param);
		$page = ssfd_Cookiejar::GetCookie($param);

		if($page['code'] == 200)
		{
			$page = ssfd_CheckLogin::check_login($param);
			if(ssfd_FindString::FindString($page['data'],'/csl/desktop'))
			{

				curl_setopt_array($param['init'], array(
					CURLOPT_URL			=> 'http://'.$param['ip'].'/form/Device',
					CURLOPT_VERBOSE 	=> 1
				));

				$page = ssfd_CurlExecution::CurlExecution($param);

				if(ssfd_FindString::FindString($page['data'],'Devcie restarting'))
				{

					return array
					(
						'error' 	=> 0,
						'message'	=> 'Boot ulang mesin finger.',
					);

				} else
				{
					return array
					(
						'error' 	=> 1,
						'message'	=> 'Reboot finger gagal, silakan coba lagi.',
					);
				}


			} else
			{
				return array
				(
					'error' 	=> 1,
					'message'	=> 'Gagal login ke mesin finger',
				);
			}

		} else
		{
			return array
			(
				'error' 	=> 1,
				'message'	=> $page['data'],
			);
		}

	}

}