<?php

namespace App\Helpers;
use App\Helpers\ssfd_CheckLogin;
use App\Helpers\ssfd_FindString;
use App\Helpers\ssfd_CurlExecution;
use App\Helpers\ssfd_Cookiejar;

class ssfd_AddUser
{

	public static function AddUser($param)
	{

		$page = ssfd_Cookiejar::GetCookie($param);

		if($page['code'] == 200)
		{

			$page = ssfd_CheckLogin::check_login($param);

			if(ssfd_FindString::FindString($page['data'],'/csl/desktop'))
			{

				$data 	= 	'ucard='.$param['ucard'].'&uname='.$param['uname'].'&upin='.$param['upin'].'&upin2='.$param['upin2'].'&uprivilege='.$param['uprivilege'].'&upwd='.$param['upwd'];

				curl_setopt_array($param['init'], array(
					CURLOPT_URL => 'http://'.$param['ip'].'/csl/user?action=save&id=add',
					CURLOPT_POST		=> 1,
					CURLOPT_VERBOSE 	=> 1,
					CURLOPT_POSTFIELDS	=> $data,
				));
				$page = ssfd_CurlExecution::CurlExecution($param);
				curl_close($param['init']);
				if(ssfd_FindString::FindString($page['data'],'Successfully'))
				{
					return array
					(
						'error' 	=> 0,
						'message'	=> $param['uname'].' berhasil di tambahkan.',
					);
				} else
				{
					return array
					(
						'error' 	=> 1,
						'message'	=> $param['uname'].' gagal ditambahkan.',
						// 'message'	=> $page,
						'results'	=> ''
					);
				}

			} else
			{
				return array
				(
					'error' 	=> 1,
					'message'	=> $param['uname'].' gagal ditambahkan, Cookies bermasalah.',
					'results'	=> ''
				);
			}

		} else
		{
			return $page;
		}
	}

}