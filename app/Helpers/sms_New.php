<?php

namespace App\Helpers;

use App\Helpers\sms_ApiKey;

class sms_New
{

	public static function new()
	{

		$ch 		= curl_init();
		$url 		= 'http://sasofound.zenziva.co.id/api/';

		$userkey 	= sms_ApiKey::UserKey();
		$passkey 	= sms_ApiKey::ApiKey();

    	curl_setopt_array($ch, array
    	(

    		CURLOPT_URL	 			=> 	$url.'readsms/?userkey='.$userkey.'&passkey='.$passkey,
    		CURLOPT_RETURNTRANSFER 	=> 	1,
			CURLOPT_HEADER			=> 	0, #0 ? 1 : 0,
    		CURLOPT_TIMEOUT			=> 	30,

    	));

		$data 	= curl_exec($ch);
		$error 	= curl_error($ch);
		$info 	= curl_getinfo($ch);

		if($data)
		{
			return $data;
		} else
		{
			return $error;
		}

	}

}