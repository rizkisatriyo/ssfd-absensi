<?php

namespace App\Helpers;
use App\Helpers\ssfd_parser;

class ssfd_AddFinger
{
	/* 
	* 	PENJELASAN RESPONSE SOAP DARI FINGERPRINT (Get all information log)
	* 
	*	PIN 			= ID USER
	*	DATETIME 		= Waktu Dan Tanggal
	*	IF STATUS 0  	= Masuk / In 
	*	IF STATUS 1 	= Keluar / Out
	* 	$param 			= Parameter Yang Dikirim Dari Controller
	*/
	public static function AddFinger($param)
	{

		/* Buka Alamat IP dengan PORT 80 */
		$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);

		if($connect)
		{

			/* Format Request XML */
			$soap_request 	= 	"<GetUserTemplate>
									<ArgComKey xsi:type=\"xsd:integer\">".$param['ComKey']."</ArgComKey>
									<Arg>
										<PIN>".$param['pin']."</PIN>
										<FingerID>6</FingerID>
										<Size>1076</Size>
										<Valid>0</Valid>
										<Template>TW1TUzIxAAAELjEECAUHCc7QAAAcL2kBAAAAhNMoky42AJYPTgCXAHghmQCEAJYPdwCPLiMOmgCYANgOeS6YAIkPVABwAGogjwC/AJsP/wDvLjIPsgDxAPYOby7+ACkPQADEATUh0wADAbMNKQALL+MOMwASAf4MHy4VAUUMUQDRASwhLQAWAb0MtQAcL6YNxAAoAXsNmC4rAdkOGQDuAcYiQgArATYO6wA3L0ALYgA1AfAMnS43Ab0OYgCGATAiIABEAb0OVgBALywNGgBFAYcOti5JAcML6wCPAaYjcQBOAUELbQBKL7ILaQBQAXQLiS5RAbIMMgCTAVYjtABWAZsLvwBcL8cNegBZAQ0NLy1eHx939+vq9xvFKHn1d1INLI/NKdaaIYlR/ySX8UHf9Sbz++Xe12MRs4KWF28vQAaS1Jt7ZoGDd1pflai/ANb7qfnAAnUk4NNmgR729DMPCuJDsIIhDQT6RaaAhjkO/fp099rCrYZJeCH/VIJmri90Gv2SDofm9DH0r84ZzYJEfvmtvPYhE/b8XIXJrDAOjQCBh7z3iMkUkO0PDQC8hG3bPX6dfeVubX8hXmR+OIb9g+QDLD30KFoIf+WSCmfMtZJhfGF4ffPhNmCKxHJ9fCTtpKk87T3t0ZYME+4/nNh96HpyOQBZqkwVeARdiIgZnh2OpTkgQwHHKxMgAgCRDBb+zABpIg7//lNpB8WFCjj+WP8JAHLdEPvR/MLAZAYAkx8H0TENAEEr+vU4+3b+DQBIMQMFMMTRVEQOAD49P/460ME+wS8OAPxI89P+T/9E/1vJAFZ8AsD//f/AOlXEJAE/YPQrKpEPBG5p9P4zM8CBbQwuOXP0wCo3ygAzVvzCKsD9wTotXSYBOoH3wP7r/wouPIj3O/4pBUX7KgG2iyJkBcWyiwnARw0AQ5AxLvgWwVMGAHqXSsTEsQYAm5wiwJT/CS5Joe39/f85/vvRwP98BwBLbfD61cH7/wcARnHw+9P8/P8FAJEGJEElAUjE5//8OfvF08HAwAYAZAwG/wUIAEbK8cDZwDYoAWjRFv3/OsEBLmXVHP5DBsVd3A4/wwoAMdv4wG3u/HcEACvk+FwBLlPkKcFDBMVs/AnBwQQAJuaFcwguQuoww1r/+nwBLknnJ1sJAP3vPu/A/8I8AwD77zDqAgC28yD5wBC6NkdZBwCx9fHA/3IFABj2WpnCABrYQW39wgQALPdUiQIQbAIpwMEQezAxSgYQyALyNMYoERgDScHBBsAMPkAEMGL/LcEQ0jJBLQUQMxb/wF4qEVUWIjgD1VATA8AFEIgdMAV+AD6qHVquBRB3HVLqngMQCyFABgQUWSYwTwMQw+lGxC0R2C9D/QTVKjJnwv4DEEo9/8AAPthGJ8T/A9WVTBrDBBC5WAbtUkYuCkMBAAALgFIAAAAAAAA=</Template>
									</Arg>
								</GetUserTemplate>";

			$buffer 		= 	"";
			$newLine 		= 	"\r\n";

			/*  Save Char To File */
			fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
			fputs($connect, "Content-Type: text/xml".$newLine);
			fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
			fputs($connect, $soap_request.$newLine);

			while($Response = fgets($connect, 1024))
			{
				$buffer = $buffer.$Response;
			}

			$buffer = ssfd_parser::do_it($buffer,"<GetUserTemplateResponse>","</GetUserTemplateResponse>");
			$buffer = explode("\r\n", $buffer);

			print_r($buffer);

		} else
		{
			$dataArray['response'][] = array
			(
				'error' 	=> 1,
				'message'	=> 'IP '.$param['ip'].' tidak merespon.',
				'results'	=> ''
			);
		}
	}
}