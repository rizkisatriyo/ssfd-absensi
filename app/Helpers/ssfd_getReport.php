<?php

	namespace App\Helpers;

	class ssfd_GetReport
	{

		/* 
		* 	PENJELASAN RESPONSE SOAP DARI FINGERPRINT
		* 
		*	PIN 			= ID USER
		*	DATETIME 		= Waktu Dan Tanggal
		*	IF STATUS 0  	= Masuk / In 
		*	IF STATUS 1 	= Keluar / Out
		*/
		public static function GetReport($param)
		{

			/* Init Class ssfd_parser */
			$parsing 		= 	new ssfd_parser();

			/* Buka Alamat IP dengan PORT 80 */
			$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);

			if($connect)
			{
				/* Init Class ssfd_parser */
				$parsing 		= 	new ssfd_parser();

				/* Buka Alamat IP dengan PORT 80 */
				$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 4);

				/* Format Request XML */
				$soap_request 	= 	"<GetAttLog>
										<ArgComKey xsi:type=\"xsd:integer\">".$param['keyCom']."</ArgComKey>
										<Arg><PIN xsi:type=\"xsd:integer\">".$param['pin']."</PIN></Arg>
									</GetAttLog>";

				$buffer 		= 	"";
				$newLine 		= 	"\r\n";

				/*  Save Char To File */
				fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($connect, "Content-Type: text/xml".$newLine);
				fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($connect, $soap_request.$newLine);

				while($Response = fgets($connect, 1024))
				{
					$buffer = $buffer.$Response;
				}

				$buffer = $parsing->do_it($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
				$buffer = explode("\r\n",$buffer);

				for($a=0; $a < count($buffer);$a++)
				{

					/* Parsing Hasil Respon */
						$data 			= $parsing->do_it($buffer[$a],"<Row>","</Row>");
						$pin 			= $parsing->do_it($data,"<PIN>","</PIN>");
						$datetinme 		= explode(' ', $parsing->do_it($data,"<DateTime>","</DateTime>"));
						$status 		= $parsing->do_it($data,"<Status>","</Status>");
						$verified		= $parsing->do_it($data,"<Verified>","</Verified>");

						$date_from 		= intval(str_replace('-', '', $param['date_from']));
						$date_to 		= intval(str_replace('-', '', $param['date_from']));
						$date_result 	= intval(str_replace('-', '', $datetinme[0]));
					/* End Parsing */

					if(!empty($pin))
					{

						/*
						*	IF STATUS 0  	= Masuk / In 
						*	IF STATUS 1 	= Keluar / Out
						*/
						if($status == 0)
						{

							$status = 'in';

						} else
						{
							$status = 'out';
						}

						if($date_from < $date_to)
						{
							if($date_result >= $date_from)
							{
								if($date_result <= $date_to)
								{
									$dataArray['response'][] = array
									(
										'error' 	=> 0,
										'message'	=> '',
										'results'	=> array
										(
											'pin' 		=> $pin,
											'date'		=> $datetinme[0],
											'time '		=> $datetinme[1],
											'status'	=> $status,
											'verified'	=> $verified,
										)
									);
								}

							} else
							{
								$dataArray['response'][] = array
								(
									'error' 	=> 0,
									'message'	=> 'Tidak ada data kurang dari tanggal '. $param['date_from'],
									'results'	=> ''
								);
							}

						} else
						{
							$dataArray['response'][] = array
							(
								'error' 	=> 1,
								'message'	=> 'Tanggal awal lebih besar dari tanggal akhir.',
								'results'	=> ''
							);
						}
					}
				}

			} else
			{
				$dataArray['response'][] = array
				(
					'error' 	=> 1,
					'message'	=> 'IP '.$param['ip'].' tidak merespon.',
					'results'	=> ''
				);
			}

			/* Output diubah ke dalam bentuk JSON */
			return json_encode($dataArray, JSON_PRETTY_PRINT);

		}
	}