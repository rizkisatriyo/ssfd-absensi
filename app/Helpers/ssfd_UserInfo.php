<?php

	namespace App\Helpers;

	class ssfd_UserInfo
	{

		/* 
		* 	PENJELASAN RESPONSE SOAP DARI FINGERPRINT
		* 
		*	PIN 			= ID USER
		*	DATETIME 		= Waktu Dan Tanggal
		*	IF STATUS 0  	= Masuk / In 
		*	IF STATUS 1 	= Keluar / Out
		*/
		public static function UserInfo($param)
		{

			/* Init Class ssfd_parser */
			$parsing 		= 	new ssfd_parser();

			/* Buka Alamat IP dengan PORT 80 */
			$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);

			if($connect)
			{

				/* Init Class ssfd_parser */
				$parsing 		= 	new ssfd_parser();

				/* Buka Alamat IP dengan PORT 80 */
				$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 4);

				/* Format Request XML */
				$soap_request 	= 	"<GetUserInfo>
										<ArgComKey xsi:type=\"xsd:integer\">".$param['keyCom']."</ArgComKey>
										<Arg>
											<PIN xsi:type=\"xsd:integer\">".$param['pin']."</PIN>
										</Arg>
									</GetUserInfo>";
				$buffer 		= 	"";
				$newLine 		= 	"\r\n";

				/*  Save Char To File */
				fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($connect, "Content-Type: text/xml".$newLine);
				fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($connect, $soap_request.$newLine);

				while($Response = fgets($connect, 1024))
				{
					$buffer = $buffer.$Response;
				}

				$buffer = $parsing->do_it($buffer,"<GetUserInfoResponse>","</GetUserInfoResponse>");
				$buffer = explode("\r\n",$buffer);

				if(count($buffer) == 3)
				{

					for($a=1; $a < count($buffer) - 1;$a++)
					{

						/* Parsing Hasil Respon */
							$data 			= $parsing->do_it($buffer[$a],"<Row>","</Row>");
							$pin 			= $parsing->do_it($data,"<PIN>","</PIN>");
							$name 			= $parsing->do_it($data,"<Name>","</Name>");
							$password		= $parsing->do_it($data,"<Password>","</Password>");
							$group			= $parsing->do_it($data,"<Group>","</Group>");
							$privilege		= $parsing->do_it($data,"<Privilege>","</Privilege>");
							$card			= $parsing->do_it($data,"<Card>","</Card>");
							$pin2			= $parsing->do_it($data,"<PIN2>","</PIN2>");
							$tz1			= $parsing->do_it($data,"<TZ1>","</TZ1>");
							$tz2			= $parsing->do_it($data,"<TZ2>","</TZ2>");
							$tz3			= $parsing->do_it($data,"<TZ3>","</TZ3>");
						/* End Parsing */

						$dataArray['response'][] = array
						(
							'error' 		=> 0,
							'message'		=> '',
							'results'		=> array
							(
								'pin1' 		=> $pin,
								'name' 		=> $name,
								'password' 	=> $password,
								'group' 	=> $group,
								'privilege'	=> $privilege,
								'card'		=> $card,
								'pin2'		=> $pin2,
								'tz1'		=> $tz1,
								'tz2'		=> $tz2,
								'tz3'		=> $tz3,
							)
						);
					}

				} else
				{
					$dataArray['response'][] = array
					(
						'error' 	=> 1,
						'message'	=> 'User ID '. $param['pin'].' tidak ditemukan.',
						'results'	=> ''
					);
				}


			} else
			{
				$dataArray['response'][] = array
				(
					'error' 	=> 1,
					'message'	=> 'IP '.$param['ip'].' tidak merespon.',
					'results'	=> ''
				);
			}

			/* Output diubah ke dalam bentuk JSON */
			return json_encode($dataArray, JSON_PRETTY_PRINT);

		}
	}