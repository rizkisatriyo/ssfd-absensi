<?php

namespace App\Helpers;
use App\Helpers\ssfd_CurlExecution;

class ssfd_Cookiejar
{

	public static function GetCookie($param)
	{
		curl_setopt_array($param['init'], array(
			CURLOPT_HEADER 	=> 1, #0 ? 1 : 0,
			CURLOPT_VERBOSE => 1,
			CURLOPT_URL		=> 'http://'.$param['ip'].'/',
		));
		return ssfd_CurlExecution::CurlExecution($param);
	}

	public static function Cookiejar()
	{
		$cookie_file 	= 	__DIR__.'/../../storage/logs/cookiejar.txt';
		$page 			=	file_get_contents($cookie_file);
		$exploder1		= 	explode("\n", $page);
		$exploder2		=	explode("	", $exploder1[4]);
		return str_replace("\r", '', $exploder2[6]);
	}

	public static function ClearCookie()
	{
        unset($_COOKIE['SessionID']);
        // setcookie('SessionID', null, -1, '/');
        setcookie("SessionID", "", time()-100 , '/' ); // past time
	}


}