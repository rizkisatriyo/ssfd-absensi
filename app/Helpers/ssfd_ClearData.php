<?php

	namespace App\Helpers;

	class ssfd_ClearData
	{

		public static function ClearData($param)
		{

			/* Init Class ssfd_parser */
			$parsing 		= 	new ssfd_parser();

			/* Buka Alamat IP dengan PORT 80 */
			$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);

			if($connect)
			{
				/* Format Request XML */
				$soap_request 	= 	"<ClearData>
										<ArgComKey xsi:type=\"xsd:integer\">".$param['ComKey']."</ArgComKey>
										<Arg>
											<Value xsi:type=\"xsd:integer\">1</Value>
										</Arg>
									</ClearData>";
				$buffer 		= 	"";
				$newLine 		= 	"\r\n";

				/*  Save Char To File */
				fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($connect, "Content-Type: text/xml".$newLine);
				fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($connect, $soap_request.$newLine);

				while($Response = fgets($connect, 1024))
				{
					$buffer = $buffer.$Response;
				}

				$buffer = $parsing->do_it($buffer,"<ClearDataResponse>","</ClearDataResponse>");
				$buffer = explode("\r\n",$buffer);

				if($buffer != '')
				{
					$data 			= $parsing->do_it($buffer[1],"<Row>","</Row>");
					$result 		= $parsing->do_it($data,"<Result>","</Result>");
					$information	= $parsing->do_it($data,"<Information>","</Information>");

					$dataArray= array
					(
						'error' 	=> 0,
						'message'	=> $information,
					);
				}

			} else
			{
				$dataArray = array
				(
					'error' 	=> 1,
					'message'	=> 'IP '.$param['ip'].' tidak merespon.',
				);
			}


			/* Output diubah ke dalam bentuk JSON */
			return $dataArray;
		}
	}