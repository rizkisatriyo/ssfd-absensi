<?php

namespace App\Helpers;
use App\Helpers\ssfd_Cookiejar;

class ssfd_CurlExecution
{

	private static function user_agent()
	{
		return 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0';
	}

	public static function CurlExecution($param)
	{

		# Inisialisasi class cookiejar
    	curl_setopt_array($param['init'], array(
    		CURLOPT_RETURNTRANSFER 	=> 1,
    		CURLOPT_FOLLOWLOCATION	=> 1,
			CURLOPT_COOKIEFILE 		=> __DIR__.'/../../storage/logs/cookie.txt',
			CURLOPT_COOKIEJAR		=> __DIR__.'/../../storage/logs/cookiejar.txt',
			CURLOPT_USERAGENT 		=> self::user_agent(),
			CURLOPT_NOBODY			=> 0, #0 ? 1 : 0,
			CURLOPT_AUTOREFERER 	=> 0,
    		CURLOPT_TIMEOUT			=> 20,
    		CURLOPT_HTTPHEADER		=> array
    		(
    			'Host: '.$param['ip'],
    			'Referer: http://'.$param['ip'].'/csl/login',
    			'Content-type: application/json; charset=utf-8',
    			'Cookie: SessionID='.ssfd_Cookiejar::Cookiejar()
    		)
    	));

		$data 	= curl_exec($param['init']);
		$error 	= curl_error($param['init']);
		$info 	= curl_getinfo($param['init']);

		if($info['http_code'] == 200)
		{
			ssfd_Cookiejar::ClearCookie();
			return array
			(
				'code' 		=> $info['http_code'],
				'info'		=> $info,
				'data' 		=> $data
			);

		} else
		{
			return array
			(
				'code' 		=> $info['http_code'],
				'info'		=> $info,
				'data' 		=> $error
			);
		}
	}
}