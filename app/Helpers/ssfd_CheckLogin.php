<?php

namespace App\Helpers;
use App\Helpers\ssfd_CurlExecution;

class ssfd_CheckLogin
{
	public static function check_login($param)
	{
		$data = 'username='.$param['username'].'&userpwd='.$param['userpwd'];

		curl_setopt_array($param['init'], array(
			CURLOPT_URL				=> 'http://'.$param['ip'].'/csl/check',
			CURLOPT_POST 			=> 1,
			CURLOPT_VERBOSE 		=> 1,
			CURLOPT_POSTFIELDS		=> $data,
		));
		return ssfd_CurlExecution::CurlExecution($param);
	}
}