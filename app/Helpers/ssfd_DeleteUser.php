<?php

	namespace App\Helpers;

	class ssfd_DeleteUser
	{
		/* 
		* 	PENJELASAN RESPONSE SOAP DARI FINGERPRINT (Get all information log)
		* 
		*	PIN 			= ID USER
		*	DATETIME 		= Waktu Dan Tanggal
		*	IF STATUS 0  	= Masuk / In 
		*	IF STATUS 1 	= Keluar / Out
		* 	$param 			= Parameter Yang Dikirim Dari Controller
		*/
		public static function DeleteUser($param)
		{

			/* Init Class ssfd_parser */
			$parsing 		= 	new ssfd_parser();

			/* Buka Alamat IP dengan PORT 80 */
			$connect 		= 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);

			if($connect)
			{

				/* Format Request XML */
				$soap_request 	= 	"<DeleteUser>
										<ArgComKey xsi:type=\"xsd:integer\">".$param['ComKey']."</ArgComKey>
										<Arg>
											<PIN xsi:type=\"xsd:integer\">".$param['pin']."</PIN>
										</Arg>
									</DeleteUser>";

				$buffer 		= 	"";
				$newLine 		= 	"\r\n";

				/*  Save Char To File */
				fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
				fputs($connect, "Content-Type: text/xml".$newLine);
				fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
				fputs($connect, $soap_request.$newLine);

				while($Response = fgets($connect, 1024))
				{
					$buffer = $buffer.$Response;
				}

				$buffer = $parsing->do_it($buffer,"<DeleteUserResponse>","</DeleteUserResponse>");
				$buffer = explode("\r\n",$buffer);
				print_r($buffer);

			} else
			{
				$dataArray['response'][] = array
				(
					'error' 	=> 1,
					'message'	=> 'IP '.$param['ip'].' tidak merespon.',
					'results'	=> ''
				);
			}

			// return $dataArray
		}
	}