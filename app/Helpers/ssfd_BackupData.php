<?php

namespace App\Helpers;
use App\Helpers\ssfd_CheckLogin;
use App\Helpers\ssfd_FindString;
use App\Helpers\ssfd_CurlExecution;
use App\Helpers\ssfd_Cookiejar;

class ssfd_BackupData
{
	public static function BackupData($param)
	{

		$page = ssfd_Cookiejar::GetCookie($param);
		$page = ssfd_Cookiejar::GetCookie($param);

		if($page['code'] == 200)
		{

			$page = ssfd_CheckLogin::check_login($param);

			if(ssfd_FindString::FindString($page['data'],'/csl/desktop'))
			{

				curl_setopt_array($param['init'], array(
					CURLOPT_URL			=> 'http://'.$param['ip'].'/form/DataApp',
					CURLOPT_POST		=> 1,
					CURLOPT_VERBOSE 	=> 0,
					CURLOPT_HEADER		=> 0,
					CURLOPT_POSTFIELDS	=> 'style=2',
				));
				return ssfd_CurlExecution::CurlExecution($param);

			} else
			{
				return $page;
			}

		} else
		{
			return $page;
		}

	}

}