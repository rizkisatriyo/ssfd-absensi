<?php

namespace App\Helpers;
use App\Helpers\ssfd_parser;

class ssfd_GetAttLog
{

	/* 
	* 	PENJELASAN RESPONSE SOAP DARI FINGERPRINT (Get all information log)
	* 
	*	PIN 			= ID USER
	*	DATETIME 		= Waktu Dan Tanggal
	*	IF STATUS 0  	= Masuk / In 
	*	IF STATUS 1 	= Keluar / Out
	* 	$param 			= Parameter Yang Dikirim Dari Controller
	*/
	public static function GetAttLog($param)
	{
		$dt[0] = '';
		$dt[1] = '';

		/* Init Class ssfd_parser */
		$parsing = 	new ssfd_parser();

		/* Buka Alamat IP dengan PORT 80 */
		$connect = 	@fsockopen($param['ip'], '80', $errno, $errstr, 1);
		if($connect)
		{
			/* Format Request XML */
			$soap_request 	= 	"<GetAttLog>
									<ArgComKey xsi:type=\"xsd:integer\">".$param['ComKey']."</ArgComKey>
									<Arg>
										<PIN xsi:type=\"xsd:integer\">".$param['pin']."</PIN>
									</Arg>
								</GetAttLog>";

			$buffer 		= 	"";
			$newLine 		= 	"\r\n";

			/*  Save Char To File */
			fputs($connect, "POST /iWsService HTTP/1.0".$newLine);
			fputs($connect, "Content-Type: text/xml".$newLine);
			fputs($connect, "Content-Length: ".strlen($soap_request).$newLine.$newLine);
			fputs($connect, $soap_request.$newLine);

			while($Response = fgets($connect, 1024))
			{
				$buffer = $buffer.$Response;
			}

			$buffer = ssfd_parser::do_it($buffer,"<GetAttLogResponse>","</GetAttLogResponse>");
			$buffer = explode("\r\n",$buffer);

			if(count($buffer) > 2)
			{
				$dataArray = array();
				for($a=0; $a < count($buffer);$a++) {
					/* Parsing Hasil Respon */
						$data 			= ssfd_parser::do_it($buffer[$a],"<Row>","</Row>");
						$pin 			= ssfd_parser::do_it($data,"<PIN>","</PIN>");
						$datetinme 		= ssfd_parser::do_it($data,"<DateTime>","</DateTime>");
						$status 		= ssfd_parser::do_it($data,"<Status>","</Status>");
						$verified		= ssfd_parser::do_it($data,"<Verified>","</Verified>");
					/* End Parsing */
					/*
					*	Pecah waktu dan tanggal
					*/
					if($datetinme != '') {
						$dt = explode(' ', $datetinme);
					}
					/*
					*	IF STATUS 0  	= Masuk / In 
					*	IF STATUS 1 	= Keluar / Out
					*/
					if($status == 0) {
						$status = 'in';
					} else{
						$status = 'out';
					}
					if($dt[0] == date('Y-m-d') && $pin != "") {
						$dataArray['response'][] = array(
							'error' 	=> 0,
							'message'	=> '',
							'results'	=> array
							(
								'ip'		=> $param['ip'],
								'pin' 		=> $pin,
								'date'		=> $dt[0],
								'time'		=> $dt[1],
								'status'	=> $status,
								'verified'	=> $verified,
							)
						);
					}
				}

				return $dataArray;
			} else
			{
				return $dataArray['response'] = array(
					'error' 	=> 1,
					'message'	=> 'Belum terdapat absen masuk pada IP '.$param['ip'],
					'results'	=> ''
				);
			}
		}

		return $dataArray['response'][] = array(
			'error' 	=> 1,
			'message'	=> 'IP '.$param['ip'].' tidak merespon.',
			'results'	=> ''
		);
	}
}