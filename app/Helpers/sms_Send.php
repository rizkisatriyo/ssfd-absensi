<?php

namespace App\Helpers;

use     App\Helpers\sms_ApiKey;

class sms_Send
{

	public static function send($nohp,$pesan)
	{

		$ch 		= curl_init();
    	curl_setopt_array($ch, array
    	(

    		CURLOPT_URL	 			=> 	'http://sasofound.zenziva.co.id/api/sendsms/',
    		CURLOPT_RETURNTRANSFER 	=> 	1,
			CURLOPT_HEADER			=> 	0, #0 ? 1 : 0,
    		CURLOPT_TIMEOUT			=> 	30,
    		CURLOPT_POST			=> 	1,
    		CURLOPT_POSTFIELDS		=> 	array
    		(
    			'userkey' 	=> sms_ApiKey::UserKey(),
    			'passkey'	=> sms_ApiKey::ApiKey(),
    			'nohp'		=> $nohp,
    			'pesan'		=> $pesan
    		)
    	));

		$data 	= curl_exec($ch);
		$error 	= curl_error($ch);
		$info 	= curl_getinfo($ch);

		if($data)
		{
			return $data;
		} else
		{
			return $error;
		}

	}

}