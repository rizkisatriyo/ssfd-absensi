<?php

namespace App\Helpers;
use App\Helpers\ssfd_parser;
class ssfd_CheckStatus
{

	public static function CheckStatus($param)
	{
		curl_setopt_array($param['init'], array(
			CURLOPT_VERBOSE => 1,
			CURLOPT_URL 	=> 'http://'.$param['ip'].'/csl/desktop',
		));

		$page = ssfd_CurlExecution::CurlExecution($param);

		if($page['code'] == 200)
		{
			$response = array
			(
				'code'		=> 	$page['code'],
				'results'	=>	self::array_result($param,$page['data'])
			);
		} else
		{
			$response = array('code' => $page['code'], 'results' => $page['data']);
		}
		return $response;
	}

	private static function array_result($param,$page)
	{
		return array
		(
			'device_name' 		=> ssfd_parser::do_it($page, 'Device Name</td><TD width=35%>','</td><TD width=30%></td></tr>'),
			'serial_number'		=> ssfd_parser::do_it($page, 'Serial Number</td><TD>','</td><TD></td></tr>'),
			'produce_date' 		=> ssfd_parser::do_it($page, 'Produce Date</td><TD>','</td><TD></td></tr>'),
			'user_capacity'		=> ssfd_parser::do_it($page, 'User capacity</td><TD>','</td><TD></td></tr>'),
			'trans_capacity'	=> ssfd_parser::do_it($page, 'Transaction capacity</td><TD>','</td><TD></td></tr>'),
			'finger_capacity'	=> ssfd_parser::do_it($page, 'Finger capacity</td><TD>','</td><TD></td></tr>'),
			'lock'				=> ssfd_parser::do_it($page, 'Lock</td><TD>','</td><TD></td></tr>'),
			'rf_card'			=> ssfd_parser::do_it($page, 'RF Card</td><TD>','</td><TD></td></tr>'),
			'short_message'		=> ssfd_parser::do_it($page, 'Short Message Management</td><TD>','</td><TD></td></tr>'),
			'usb_disk'			=> ssfd_parser::do_it($page, 'Usb Disk</td><TD>','</td><TD></td></tr>'),
			'usb_client'		=> ssfd_parser::do_it($page, 'Usb Client</td><TD>','</td><TD></td></tr>'),
			'remote'			=> ssfd_parser::do_it($page, 'Remote Identification Server</td><TD>','</td><TD></td></tr>'),
			'ip_address'		=> $param['ip'],
			'user_id'			=> $param['user_id'],
			'password'			=> $param['password'],
			'com_key'			=> $param['com_key']
		);
	}
}