<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attedances extends Model
{

	protected $fillable = [
                'student_id',
                'kode',
                'jam_masuk',
                'jam_keluar',
                'hadir',
                'alfa',
                'terlambat',
                'sakit',
                'izin',
                'pulang',
                'nsm',
                'nsp',
                'kjm',
                'kjp'
	];

	public function student() 
	{
                return $this->belongsTo(Student::class, 'student_id', 'id');
		// return $this->belongsTo(Student::class);
		// return $this->hasManyThrough('App\Student');
	}
}
