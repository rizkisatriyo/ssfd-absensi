<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = [
        'nis',
        'name',
        'grade_id',
        'pin',
        'kelamin',
        'alamat',
        'tmplahir',
        'tgllahir',
        'foto',
        'kota',
        'tahun_masuk',
        'nohp',
        'nohp_ortu'
    ];

    public function grade() 
    {
    	return $this->belongsTo(Grade::class);
    }

    public function attedance()
    {
    	return $this->hasOne(Attedances::class);
    }

}