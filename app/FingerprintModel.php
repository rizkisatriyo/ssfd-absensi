<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FingerprintModel extends Model
{
    protected 	$table 	= 	'fingerprint';
}
