<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Helpers\ssfd_GetAttLog;
use App\FingerprintModel;
use App\Events\AttendancesReceive;
use App\Attedances;
use App\Student;
use App\Helpers\sms_Send;
use File;

class read_finger_out extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:read_finger_out';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Baca Absensi Ke Mesin Finger Out';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->manifest  = $this->read_manifest_file();
        $this->timenow   = date('h:i:s');
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $file = $this->manifest['read_finger_out'];
            /* Membaca kontent dari file */
            $content = File::get($file);
            /*
            | Masuk Ke Looping Siswa dengan
            | mengirimkan parameter untuk memulai looping
            */
            $msg = $this->student($content);
        } catch (\Exception $e) {
            $this->hit_resetter(0);
            $msg = $e->getMessage();
        }

        $exec = new AttendancesReceive($msg);
        $this->info(event($exec));
    }

    protected function student($content)
    {
        /* Hitung jumlah siswa */
        $record         = Student::all()->count();
        /* Batasan Looping */
        $limit          = $this->manifest['limit'];
        if($content >= $record)
        {
            /* Isi konten file diubah ke 0 */
            $add_counter    =   0;
            $this->hit_resetter($add_counter);
            return 'Hitungan di reset kembali';
        } else
        {
            /* Tambah counter dengan batas limit */
            $add_counter    =   $content + $limit;
            $query          =   Student::limit($limit)->offset($content)->get();
            $this->hit_resetter($add_counter);
            /* Batasi looping siswa */
            foreach($query as $row)
            {
                $this->info($this->get_attlogs($row));
            }
        }
    }

    protected function get_attlogs($data)
    {

        $fingerprint = FingerprintModel::All();

        foreach($fingerprint as $row)
        {
            try
            {

                $with_array     =   array
                 (
                     'ip'        => $row->ip_address,
                     'ComKey'    => $row->com_key,
                     'status'    => 'out',
                     'pin'       => $data['id']
                 );
                $attLog = ssfd_GetAttLog::GetAttLog($with_array);
                /* Looping data dari mesin finger */
                foreach($attLog['response'] as $log)
                {
                    if($log['results']['status'] == "out")
                    {
                        $sendata = [
                            'student_id' => $data->id,
                            'kode' => date('Y-m-d_'.$data->id),
                            'jam_keluar' => $this->timenow,
                            'pulang' => 1,
                        ];
                        $this->update_to_db($log,$data,$sendata);
                    }
                }
            } catch (\Exception $e)
            {
                $this->info('Error : '. $e->getMessage());
            }
        }
    }

    protected function update_to_db($log,$data,$sendata)
    {
        Attedances::where('kode', $sendata['kode'])->update([
            'pulang' => $sendata['pulang'],
            'jam_keluar' => $sendata['jam_keluar'],
        ]);
        $this->info('Update absen berhasil.');
    }

    protected function hit_resetter($hit)
    {
        /* Ubah kembali isi konten file */
        File::put($this->manifest['read_finger_out'], $hit);
    }

    protected function read_manifest_file()
    {
        return json_decode(File::get('./public/manifest.json'), true);
    }
}





// <?php

// namespace App\Console\Commands;
// use Illuminate\Console\Command;
// use App\Helpers\ssfd_GetAttLog;
// use App\FingerprintModel;
// use App\Events\AttendancesReceive;
// use App\Attedances;

// class read_finger_out extends Command
// {
//     /**
//      * The name and signature of the console command.
//      *
//      * @var string
//      */
//     protected $signature = 'command:read_finger_out';

//     /**
//      * The console command description.
//      *
//      * @var string
//      */
//     protected $description = 'Baca Absensi Ke Mesin Finger Out';

//     /**
//      * Create a new command instance.
//      *
//      * @return void
//      */
//     public function __construct()
//     {
//         parent::__construct();
//     }

//     /**
//      * Execute the console command.
//      *
//      * @return mixed
//      */
//     public function handle()
//     {
//         /* Panggil semua data mesin finger */
//         $finger     = FingerprintModel::All();
//         /* Looping Mesin Finger */
//         foreach($finger as $row)
//         {

//             /* Parameter untuk mengambil data ke mesin finger */
//             $with_array     =   array
//             (
//                 'ip'        => $row->ip_address,
//                 'ComKey'    => $row->com_key,
//                 'status'    => 'out',
//                 'pin'       => 'ALL'
//             );

//             /* Ambil data ke mesin finger */
//             try
//             {

//                 $attLog = ssfd_GetAttLog::GetAttLog($with_array);
//                 /* Looping data dari mesin finger */
//                 foreach($attLog['response'] as $log)
//                 {
//                     if($log['results']['status'] == "out")
//                     {
//                         $this->insert_to_db($log);
//                     }
//                 }

//             } catch (\Exception $e)
//             {
                
//             }

//         }
//     }

//     private function insert_to_db($log)
//     {
//         $clock  = $this->read_file();
//         $pin    = $log['results']['pin'];
//         $date   = $log['results']['date'];
//         $time   = $log['results']['time'];
//         $status = $log['results']['status'];

//         $time_p = explode(':', $time);
//         $gabung_time = $time_p[0].':'.$time_p[1];

//        //  Output ID = 2018-02-01_1
//         $uid  = $date.'_'.$pin;

//         $eloquent = new Attedances();

//         if($time >= $clock['out_clock'])
//         {
//             try
//             {
//                 $eloquent = Attedances::where('kode', $uid)->update(['pulang' => 1,'jam_keluar' => $time]);
//                 $this->info('Data absensi berhasil diubah.');
//             } catch (\Exception $e)
//             {
//                 $this->info($e->getMessage());
//             }
//         }

//         $exec       = new AttendancesReceive($log);
//         event($exec);
//     }

//     private function read_file()
//     {
//         return json_decode(file_get_contents('./public/manifest.json'), true);
//     }
// }
