<?php

namespace App\Console\Commands;
/* Use Core From Laravel */
use Illuminate\Console\Command;
use File;
/* Use Events */
use App\Events\AttendancesReceive;
/* Use Models */
use App\Attedances;
use App\FingerprintModel;
use App\Student;
/* Use Helpers */
use App\Helpers\ssfd_GetAttLog;
use App\Helpers\sms_Send;

class read_finger_in extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:read_finger_in';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Baca Absensi Ke Mesin Finger In';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->manifest  = $this->read_manifest_file();
        $this->timenow   = date('h:i:s');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $file = $this->manifest['read_finger_in'];
            /* Membaca kontent dari file */
            $content = File::get($file);
            /*
            | Masuk Ke Looping Siswa dengan
            | mengirimkan parameter untuk memulai looping
            */
            $msg = $this->student($content);
        } catch (\Exception $e) {
            $this->hit_resetter(0);
            $msg = $e->getMessage();
        }

        $exec = new AttendancesReceive($msg);
        $this->info(event($exec));
    }

    protected function student($content)
    {
        /* Hitung jumlah siswa */
        $record         = Student::all()->count();
        /* Batasan Looping */
        $limit          = $this->manifest['limit'];
        if($content >= $record)
        {
            /* Isi konten file diubah ke 0 */
            $add_counter    =   0;
            $this->hit_resetter($add_counter);
            return 'Hitungan di reset kembali';
        } else
        {
            /* Tambah counter dengan batas limit */
            $add_counter    =   $content + $limit;
            $query          =   Student::limit($limit)->offset($content)->get();
            $this->hit_resetter($add_counter);
            /* Batasi looping siswa */
            foreach($query as $row)
            {
                $this->info($this->get_attlogs($row));
            }
        }
    }

    protected function get_attlogs($data)
    {

        $fingerprint = FingerprintModel::All();

        foreach($fingerprint as $row)
        {
            try
            {

                $with_array     =   array
                 (
                     'ip'        => $row->ip_address,
                     'ComKey'    => $row->com_key,
                     'status'    => 'in',
                     'pin'       => $data['id']
                 );
                $attLog = ssfd_GetAttLog::GetAttLog($with_array);
                /* Looping data dari mesin finger */
                if(isset($attLog['response']))
                {
                    foreach($attLog['response'] as $log)
                    {
                        if($log['results']['status'] == "in")
                        {
                            $this->decision($log,$data);
                        }
                    }
                } else
                {
                    if($this->timenow > $this->manifest['time_off_schedular_in']) {
                        $sendata = [
                            'student_id' => $data->id,
                            'kode' => date('Y-m-d_'.$data->id),
                            'jam_masuk' => $this->timenow,
                            'hadir' => 0,
                            'alfa' => 1,
                            'terlambat' => 0,
                        ];
                        $this->insert_to_db($sendata);
                    } else
                    {
                        $this->info($this->timenow. ' Waktu alfa belum berakhir');
                    }
                }
            } catch (\Exception $e)
            {
                $this->info('Error : '. $e->getMessage());
            }
        }
    }

    protected function decision($log,$data)
    {
        $pin    = $log['results']['pin'];
        $date   = $log['results']['date'];
        $time   = $log['results']['time'];
        $status = $log['results']['status'];

         // Output ID = 2018-02-01_1
        $uid = $date.'_'.$pin;
        if($time <= $this->manifest['in_clock']) {
            $this->info($this->prepare_to_create($log,$data, 'hadir'));
        } else if($time > $this->manifest['in_clock'] && $time <= $this->manifest['time_off_schedular_in']) {
            $this->info($this->prepare_to_create($log,$data, 'terlambat'));
        } else
        {
            $this->info('Tidak ada aktivitas insert absen ke database!');
        }
    }

    protected function prepare_to_create($log,$data,$status)
    {
        $pin    = $log['results']['pin'];
        $date   = $log['results']['date'];
        $time   = $log['results']['time'];
        $status = $log['results']['status'];
        //  Output ID = 2018-02-01_1
        $uid        = $date.'_'.$pin;

        if($status == 'hadir')
        {
            $hadir      = 1;
            $alfa       = 0;
            $terlambat  = 0;
        } else
        {
            $hadir      = 0;
            $alfa       = 0;
            $terlambat  = 1;
        }

        try {
            $sendata = [
                'student_id' => $data->id,
                'kode' => $uid,
                'jam_masuk' => $time,
                'hadir' => $hadir,
                'alfa' => $alfa,
                'terlambat' => $terlambat,
            ];
            $this->insert_to_db($sendata);
        } catch (\Exception $e) {
            $this->info('Insert absen error : '. $e->getMessage());
        }
    }

    protected function insert_to_db($param)
    {
        Attedances::create([
            'student_id'    => $param['student_id'],
            'kode'          => $param['kode'],
            'jam_masuk'     => $param['jam_masuk'],
            'jam_keluar'    => '00:00:00',
            'hadir'         => $param['hadir'],
            'alfa'          => $param['alfa'],
            'terlambat'     => $param['terlambat'],
            'sakit'         => 0,
            'izin'          => 0,
            'pulang'        => 0,
            'nsm'           => 0,
            'nsp'           => 0,
            'kjm'           => $this->manifest['in_clock'],
            'kjp'           => $this->manifest['out_clock']
        ]);
        $this->info('Insert absen berhasil.');
    }

    protected function hit_resetter($hit)
    {
        /* Ubah kembali isi konten file */
        File::put($this->manifest['read_finger_in'], $hit);
    }

    protected function read_manifest_file()
    {
        return json_decode(File::get('./public/manifest.json'), true);
    }
}