<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\sms_Send;
use App\Attedances;
use App\Student;
use App\Grade;
use File;

class send_sms_notifier extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_sms_notifier';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengirimkan Notifikasi SMS Ke Orang Tua Siswa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->manifest  = $this->read_manifest_file();
        $this->timenow   = date('h:i:s');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $file = $this->manifest['counter_notifier'];
            /* Membaca kontent dari file */
            $content = File::get($file);
            $this->info($this->run_it($content));
        } catch (\Exception $e) {
            $this->hit_resetter(0);
            // Show Info
            $this->info($e->getMessage());
        }
    }

    public function run_it($content)
    {

        /* Batasan Looping */
        $limit = $this->manifest['limit'];
        //  pendefinisian tanggal awal
        $tgl1 = date('Y-m-d');
        //  operasi penjumlahan tanggal
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        // Query Get Data With Between
        $query = Attedances::whereBetween('created_at', [ $tgl1, $tgl2]);
        $record = $query->count();

        if($record > 0)
        {

            $query_limit = $query->limit(10)->offset($content);

            if($content >= $record)
            {
                /* Isi konten file diubah ke 0 */
                $add_counter    =   0;
                $this->hit_resetter($add_counter);
                return 'Hitungan di reset kembali';
            } else
            {
               $attendances = $query->get();
                foreach ($attendances as $row) {
                    $student = $this->get_student($row->student_id);
                    $this->info($this->prepare_send_notifier($student,$row));
                }
            }
        } else
        {
            return 'Tidak ada absensi masuk hari ini';
        }
    }

    protected function prepare_send_notifier($student,$attendance)
    {

        if($attendance->hadir == 1) {
            $status = 'hadir';
        }

        if($attendance->terlambat == 1) {
            $status = 'terlambat';
        }

        if($attendance->alfa == 1) {
            $status = 'alfa';
        }

        $grade = $this->get_grade($student->id);

        if($attendance->nsm == 0) {
            $this->send_nsm_notifier($student,$grade,$attendance,$status);
        }

        if($attendance->nsp == 0) {
            $this->send_nsp_notifier($student,$grade,$attendance);
        }
        return 'Tidak ada notifikasi yang perlu dikirimkan.';
    }

    protected function send_nsm_notifier($student,$grade,$attendance,$status)
    {
        $date = explode(' ', $attendance->created_at);
        $text1 = array('[nama]','[kelas]','[jam_masuk]','[tanggal]','[status]');
        $text2 = array(
            $student->name,
            $grade->name,
            $attendance->jam_masuk,
            $date[0],
            $status,
        );
        $notifier = $this->manifest['sms_notification'];
        if($notifier == 'Y' OR $notifier == 'y') {

            $msg = $this->manifest['message_format_in'];
            $msg = str_replace($text1, $text2, $msg);
            $send = json_decode(sms_Send::send($student->nohp_ortu,$msg));

            if($send->status == 1) {
                $this->info($this->notifier_updated($attendance->id,'nsm')); 
            } else {
                $this->info("SMS Gagal Dikirim");
            }
        } else {
            $this->info('Notifikasi SMS ke orang tua siswa di nonaktifkan.');
        }
    }

    protected function send_nsp_notifier($student,$grade,$attendance)
    {
        $date = explode(' ', $attendance->created_at);
        $text1 = array('[nama]','[kelas]','[jam_keluar]','[tanggal]');
        $text2 = array(
            $student->name,
            $grade->name,
            $attendance->jam_keluar,
            $date[0]
        );
        $notifier = $this->manifest['sms_notification'];
        if($notifier == 'Y' OR $notifier == 'y') {

            $msg = $this->manifest['message_format_out'];
            $msg = str_replace($text1, $text2, $msg);
            $send = json_decode(sms_Send::send($student->nohp_ortu,$msg));

            if($send->status == 1) {
                $this->info($this->notifier_updated($attendance->id,'nsp')); 
            } else {
                $this->info("SMS Gagal Dikirim");
            }
        } else {
            $this->info('Notifikasi SMS ke orang tua siswa di nonaktifkan.');
        }
    }

    protected function notifier_updated($id,$notifier)
    {
        if($notifier == 'nsm') {
            $field = ['nsm' => 1];
        }
        if($notifier == 'nsp') {
            $field = ['nsp' => 1];   
        }
        Attedances::where('id', $id)->update($field);
        return 'Sms notifikasi berhasil dikirimkan.';
    }

    protected function get_grade($id)
    {
        return Grade::find($id);
    }

    protected function get_student($id)
    {
        return Student::find($id);
    }

    protected function hit_resetter($hit)
    {
        /* Ubah kembali isi konten file */
        File::put($this->manifest['counter_notifier'], $hit);
    }

    protected function read_manifest_file() {
        return json_decode(File::get('./public/manifest.json'), true);
    }
}
