<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\DumpTest;
use App\Helpers\sms_New;
use App\Events\ReceiveMessagesEvent;

class ReceiveMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:receive_messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive Messages Every Minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_sms    = sms_New::new();
        $exec       = new ReceiveMessagesEvent($get_sms);
        event($exec);
        $this->info($this->create_logs($get_sms));
    }

    private function create_logs($param)
    {

        $getflog    = json_decode(file_get_contents('./public/manifest.json'), true);
        $filename   = './storage/app/public/'.$getflog['sms_flog'];

        if(!file_exists($filename))
        {
             $open_file  = fopen($filename, "w");
            fwrite($open_file,'');
        }

        $decode     = json_decode($param, true);

        if(isset($decode['msg-count']))
        {
            for($i = 0; $i < $decode['msg-count']; $i++)
            {

                $id     = $decode['msg'][$i]['messageId'];
                $date   = $decode['msg'][$i]['date'];
                $dari   = $decode['msg'][$i]['dari'];
                $pesan  = $decode['msg'][$i]['isiPesan'];

                $text   = $id.'|'.$dari.'|'.$pesan.'|'.$date;
                file_put_contents($filename, $text.PHP_EOL , FILE_APPEND | LOCK_EX);
                sleep(2);
            }
            return 'Terdapat '. $decode['msg-count'].' pesan baru masuk.';
        } else
        {
            return 'tidak ada pesan hari ini';
        }
    }
}
