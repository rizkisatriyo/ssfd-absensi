<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = \Auth::user()->role->name;
        if ($role == 'superadmin' || 'middleadmin' || 'lowadmin' ) {
          return $next($request);
        }
        
        return response('Cannot Access Page', 403);
    }
}
