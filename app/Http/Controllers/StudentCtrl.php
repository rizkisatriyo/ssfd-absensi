<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder; 
use App\Http\Controllers\Controller;
use App\Helpers\ssfd_DeleteUser;
use App\Helpers\ssfd_AddUser;
use App\Attedances;
use App\Student;
use App\Grade;
use Excel;
use App\FingerprintModel;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Validator;

class StudentCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        /* CEK REQUEST AJAX */
       if ($request->ajax()) {
            /* MENAMPILKAN DATA YANG AKAN DITAMPILKAN PADA DATATABLE DENGAN EAGER LOADING*/
            $student = Student::with('grade')->orderBy("pin","desc");
             return DataTables::of($student) 
                ->addColumn('action', function($student) {
                    return view('default_theme.datatable.action', [
                        'model' => $student,
                        'hapus' => Route('student.destroy', $student->id),
                        'edit'  => Route('student.edit', $student->id)
                    ]);
                })->make(true);
        }

        /* MENAMPILKAN KONTEN SISWA*/
        $html = $htmlBuilder
        ->addColumn(['data'=>'nis', 'name'=>'nis', 'title'=>'NIS'])
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'Nama Siswa'])
        ->addColumn(['data'=>'kelamin', 'name'=>'kelamin', 'title'=>'Gender'])
        ->addColumn(['data'=>'grade.name', 'name'=>'grade.name', 'title'=>'Kelas'])
        ->addColumn(['data'=>'action', 'name'=>'action', 'title'=>'Action', 'orderable' => false, 'searchable' => false]);
        /* MENAMPILKAN KE VIEW*/
        return view('default_theme.siswa.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('default_theme.siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $finger = FingerprintModel::All();
            $name = substr($request->name,0,26);
            foreach($finger as $row)
            {
                $with_array     =   array
                (
                    'init'          =>  curl_init(), # Inisialisasi CURL
                    'ip'            =>  $row->ip_address, # Arahkan IP Address fingerprint
                    'username'      =>  $row->user_id, # Username Login For Web App Fingerprint
                    'userpwd'       =>  $row->password,
                    'ucard'         =>  0, # Nomor Kartu
                    'uname'         =>  $name, # Nama User
                    'upin'          =>  null, # PIN / ID User (Update Data User)
                    'upin2'         =>  $request->pin, # PIN / ID User
                    'uprivilege'    =>  0, # 0 = (User) & 14 = (Administrator)
                    'upwd'          =>  '12345' # Password login User
                );
                ssfd_AddUser::AddUser($with_array);
            }
            Student::create($request->all());
            return array('error' => 1, 'msg' => 'Data berhasil disimpan!');
        } catch (\Exception $e) {
            return array('error' => 1, 'msg' => $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);
        return view('default_theme.siswa.edit', ['student' => $student])->with(compact('student'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'nis' => 'required|numeric',
            'tmplahir' => 'required',
            'alamat' => 'required',
            'nohp_ortu' => 'required|numeric',
            'foto' => 'required'
        ];

        $valid = Validator::make($request->all(), $rules, $this->messages());
        if(!$valid->fails())
        {
            Student::where('id', $id)->update(
                [
                    'nis' => $request->nis,
                    'name' => $request->name,
                    'tmplahir' => $request->tmplahir,
                    'tgllahir' => $request->tgllahir,
                    'kelamin' => $request->kelamin,
                    'grade_id' => $request->grade_id,
                    'alamat' => $request->alamat,
                    'kota' => $request->kota,
                    'tahun_masuk' => $request->tahun_masuk,
                    'nohp_ortu' => $request->nohp_ortu,
                    'foto' => $request->foto,
                ]
            );
            return array('error' => 0, 'msg' => 'Data berhasil diubah!');
        }
        return array('error' => 1,'results' => $valid->errors()->all());
    }

    private function messages()
    {
        $messages = [
            'name.required'         => 'Nama tidak boleh kosong.',
            'nis.required'          => 'NIS tidak boleh kosong.',
            'nis.numeric'           => 'NIS hanya angka saja.',
            'tmplahir.required'     => 'Tempat lahir tidak boleh kosong.',
            'alamat.required'       => 'Alamat tidak boleh kosong.',
            'nohp.required'         => 'Nomor HP tidak boleh kosong.',
            'nohp.numeric'          => 'Nomor HP hanya angka saja.',
            'foto.required'         => 'Foto tidak boleh kosong.',
        ];
        return $messages;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $finger = FingerprintModel::All();
        foreach($finger as $row)
        {
            $with_array     =   array
            (
                'ip'            =>  $row->ip_address,
                'pin'           =>  $student->pin,
                'ComKey'        =>  $row->com_key
            );
            ssfd_DeleteUser::DeleteUser($with_array);
        }
         Student::destroy($id);
         return redirect()->route('student.index');
    }    
}

