<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Grade;
use App\Attedances;
use App\FingerprintModel;

class DashboardCtrl extends Controller
{
    public function index() 
	{
		/* MENGHITUNG JUMLAH TOTAL DATA SISWA */
		$this->data['siswa'] = Student::all()->count();

		/* MENGHITUNG JUMLAH TOTAL DATA KELAS */
    	$this->data['kelas'] = Grade::all()->count();

		/* MENGHITUNG JUMLAH TOTAL DATA ABSENSI KESELURUHAN */
    	$this->data['attedances'] = Attedances::all()->count();

		/* MENGHITUNG JUMLAH TOTAL DATA ABSENSI KESELURUHAN */
    	$this->data['fingerprint'] = FingerprintModel::all()->count();

    	/* INISIALISASI TITLE */
		$this->data['title'] = 'Dashboard - Administrator'; 

		/* TAMPILKAN */
		return view('default_theme.index', $this->data); 

    }
}
