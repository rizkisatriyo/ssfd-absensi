<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Attedances;
use App\Student;
use App\Grade;
use Validator;
use Excel;
use File;

class Report extends Controller
{
    public function index()
    {
        return view('default_theme.report.index');
    }

    private function messages()
    {
        $messages = [
            'f_date.required'       => 'Tanggal periode mulai tidak boleh kosong',
            't_date.required'       => 'Tanggal periode akhir tidak boleh kosong',
            'name.required'     	=> 'Kelas tidak boleh kosong',
        ];
        return $messages;
    }

    public function api(Request $request)
    {
        $rules = [
            'name' => 'required',
            'f_date' => 'required',
            't_date' => 'required',
        ];

        $valid = Validator::make($request->all(), $rules, $this->messages());

        if($valid->fails())
        {
            return array
            (
                'error'     => 1,
                'results'   => $valid->errors()->all()
            );
        } else
        {
        	// Ambil terlebih dahulu data kelas yang ada
        	$a 	= Grade::where('name', $request->name)->first();
        	$b 	= Student::where('grade_id', $a->id);
        	$_b = $b->get();

            $data['request']    = $request->all();
        	$data['data_kelas'] = $a;
            if($b->count() >= 1)
            {
                for ($i=0; $i < $b->count(); $i++)
                { 
                    $data['data_siswa'] = $_b;
                    $data['data_siswa'][$i]['data_absen'] = array();
                    $c  =   Attedances::where('student_id', $_b[$i]->id)
                            ->whereBetween('created_at', [
                                $request->f_date,
                                $request->t_date,
                            ]);
                    $_c = $c->get();
                    for ($n=0; $n < $c->count(); $n++)
                    {
                        $data['data_siswa'][$i]['data_absen'] = $_c;
                    }
                }
                $this->create_excel_report($data);
                return $data;
            }
        }
    }

    protected function create_excel_report($data = null)
    {
        if($data != null)
        {
            Excel::create('absensi_Siswa_'.date('dmy').'_'.$data['data_kelas']['name'], function($excel) use($data)
            {
                $excel->setTitle('Absensi Siswa');
                $excel->setCreator('Absensi Siswa')->setCompany('Absensi Siswa');
                $excel->setDescription('Absensi Siswa');

                /* Set Sheet */
                $excel->sheet('Absensi Kelas '. $data['data_kelas']['name'],
                function($sheet) use($data)
                {
                    /* Set Merge Cell */
                    $sheet->mergeCells('A1:AL1')->row(1, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('A2:AL2')->row(2, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(14);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('A3:AL3')->row(3, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('A5:A6')->row(5, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('B5:B6')->row(5, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('C5:AG5')->row(5, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('C5:AG5')->row(5, function($row)
                    {
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->mergeCells('AH5:AL5')->row(5);
                    /* End set merge cell */

                    $periode = 'Periode : '.$data['request']['f_date'].' s/d '.$data['request']['t_date'];

                    /* Set Columns */
                    $sheet->row(1, array('ABSENSI SISWWA KELAS '.
                        $data['data_kelas']['name']));
                    $sheet->row(2, array($this->manifest()));
                    $sheet->row(3, array($periode));
                    $sheet->row(5, array('NIS','NAMA','PERTEMUAN KE'));
                    $sheet->cell('AH5', function($row) {
                        $row->setValue('TOTAL');
                        $row->setFontFamily('Times New Roman');
                        $row->setFontSize(12);
                        $row->setFontWeight('bold');
                        $row->setAlignment('center');
                    });
                    $sheet->row(6, $this->set_column());
                    $sheet->setWidth($this->set_width_cells());
                    /* End set columns */

                    $num = 7;
                    for ($i=0; $i < count($data['data_siswa']); $i++)
                    {
                        $absen = $data['data_siswa'][$i]['data_absen'];
                        $sheet->cell('A'.$num, $data['data_siswa'][$i]['nis']);
                        $sheet->cell('B'.$num, $data['data_siswa'][$i]['name']);

                        $total_hadir        = 0;
                        $total_sakit        = 0;
                        $total_izin         = 0;
                        $total_terlambat    = 0;
                        $total_alfa         = 0;

                        $tgl = 1;
                        for ($n=0; $n <= 31; $n++)
                        { 

                            if(isset($absen[$n]['hadir']) && $absen[$n]['hadir'] ==1)
                            {
                                $sheet->cell($this->date_cell()[$n].$num,'H');
                                $total_hadir++;
                            }

                            if(isset($absen[$n]['sakit']) && $absen[$n]['sakit'] == 1)
                            {
                                $sheet->cell($this->date_cell()[$n].$num,'S');
                                $total_sakit++;
                            }
                            if(isset($absen[$n]['terlambat']) && $absen[$n]['terlambat'] == 1)
                            {
                                $sheet->cell($this->date_cell()[$n].$num,'T');
                                $total_terlambat++;
                            }
                            if(isset($absen[$n]['alfa']) && $absen[$n]['alfa'] == 1)
                            {
                                $sheet->cell($this->date_cell()[$n].$num,'A');
                                $total_alfa++;
                            }
                            if(isset($absen[$n]['izin']) && $absen[$n]['izin'] == 1)
                            {
                                $sheet->cell($this->date_cell()[$n].$num,'I');
                                $total_izin++;
                            }
                        $tgl++;
                        }

                        $sheet->cell('AH'.$num, $total_hadir);
                        $sheet->cell('AI'.$num, $total_terlambat);
                        $sheet->cell('AJ'.$num, $total_izin);
                        $sheet->cell('AK'.$num, $total_sakit);
                        $sheet->cell('AL'.$num, $total_alfa);

                    $num++;
                    }

                }); /* End set sheet */

            })->export('xls');
        }
    }

    protected function manifest()
    {
        /* Mengambil Settingan Aplikasi Seperti Mengambil nama sekolah */
        $app = json_decode(file_get_contents('./manifest.json'));
        return $app->first_name.' '.$app->middle_name.' '.$app->last_name;
    }

    protected function date_cell()
    {
        $a = array('C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG',);
        return $a;
    }

    protected function set_width_cells()
    {
        return array(
            'A'     =>  9,
            'B'     =>  50,
            'C'     =>  3,
            'D'     =>  3,
            'E'     =>  3,
            'F'     =>  3,
            'G'     =>  3,
            'H'     =>  3,
            'I'     =>  3,
            'J'     =>  3,
            'K'     =>  3,
            'L'     =>  3,
            'M'     =>  3,
            'N'     =>  3,
            'O'     =>  3,
            'P'     =>  3,
            'Q'     =>  3,
            'R'     =>  3,
            'S'     =>  3,
            'T'     =>  3,
            'U'     =>  3,
            'V'     =>  3,
            'W'     =>  3,
            'X'     =>  3,
            'Y'     =>  3,
            'Z'     =>  3,
            'AA'    =>  3,
            'AB'    =>  3,
            'AC'    =>  3,
            'AD'    =>  3,
            'AE'    =>  3,
            'AF'    =>  3,
            'AG'    =>  3,
            'AH'    =>  5,
            'AI'    =>  5,
            'AJ'    =>  5,
            'AK'    =>  5,
            'AL'    =>  5,
        );
    }

    protected function set_column()
    {
        $data = array(0 => '', 1 => '');
        $no = 0;
        for ($i=2; $i <= 32; $i++)
        { 
           $data[$i] = $no + 1;
           $no++;
        }
        $data[33] = 'H';
        $data[34] = 'T';
        $data[35] = 'I';
        $data[36] = 'S';
        $data[37] = 'A';
        return $data;
    }
}
