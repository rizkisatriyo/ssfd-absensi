<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Services\DataTable;
use App\Http\Requests\StoreGradeRequest;
use App\Http\Requests\UpdateGradeRequest;
use App\Grade;

class GradeCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
        /* CEK REQUEST AJAX */
        if ($request->ajax()) {
            /* MEMILIH DATA YANG AKAN DI TAMPILKAN PADA DATATABLE */
            $grade = Grade::select(['id','name']);
             return DataTables::of($grade) 
                ->addColumn('action', function($grade) {
                    return view('default_theme.datatable.action', [
                        'model' => $grade,
                        'hapus' => Route('grade.destroy', $grade->id),
                        'edit'  => Route('grade.edit', $grade->id)
                    ]);
                })->make(true);
        }

        /* MENAMPILKAN KONTEN KELAS*/
        $html = $htmlBuilder
        ->addColumn(['data'=>'id', 'name'=>'id', 'title'=>'ID','orderable' => true,'style' => 'width:20%;'])
        ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'Kelas','orderable' => true,'style' => 'width:50%;'])
        ->addColumn(['data'=>'action', 'name'=>'action', 'title'=>'', 'orderable' => false, 'searchable' => false,'style' => 'width:30%;']);
        
        /* MENAMPILKAN VIEW */
        return view('default_theme.kelas.index')->with(compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('default_theme.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGradeRequest $request)
    {
      $grade = Grade::create($request->all());
      return redirect()->route('grade.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade = Grade::find($id);
        
        return view('default_theme.kelas.edit', ['grade' => $grade])->with(compact('grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGradeRequest $request, $id)
    {
        $grade = Grade::find($id);
        $grade->update($request->all());

        return redirect()->route('grade.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Grade::destroy($id);

        return redirect()->route('grade.index');
    }

    public function apiGrade() 
    {
       $grade = Grade::all();
       // return Datatables::of($grade)->make(true);
       return $grade;
    }
}
