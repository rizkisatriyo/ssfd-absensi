<?php

namespace App\Http\Controllers\Sms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Helpers\sms_Credit;
use App\Helpers\sms_Inbox;
use App\Helpers\sms_Outbox;
use App\Helpers\sms_New;
use App\Helpers\sms_Send;


class Sms extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('default_theme.sms.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function del_temp_sms()
    {
        $getflog    = json_decode(file_get_contents(url('manifest.json')), true);
        Storage::disk('public')->delete($getflog['sms_flog']);
        Storage::disk('public')->put($getflog['sms_flog'],'');
    }

    public function credit()
    {
        return sms_Credit::credit();
    }

    public function new()
    {
        $getflog    = json_decode(file_get_contents(url('manifest.json')), true);
        return file(base_path('storage/app/public/'.$getflog['sms_flog']));
    }

    public function inbox(Request $request)
    {
        $dateFrom       =   explode('-', $request->inbox_datefrom);
        $DateTo         =   explode('-', $request->inbox_dateto);
        $with_array     =   array
        (
            'DateFrom'  =>  $dateFrom[2].'/'.$dateFrom[1].'/'.$dateFrom[0],
            'DateTo'    =>  $DateTo[2].'/'.$DateTo[1].'/'.$DateTo[0],
        );
        return sms_Inbox::inbox($with_array);
    }

    public function outbox(Request $request)
    {
        $dateFrom       =   explode('-', $request->outbox_datefrom);
        $DateTo         =   explode('-', $request->outbox_dateto);
        $with_array     =   array
        (
            'DateFrom'  =>  $dateFrom[2].'/'.$dateFrom[1].'/'.$dateFrom[0],
            'DateTo'    =>  $DateTo[2].'/'.$DateTo[1].'/'.$DateTo[0],
        );
        return str_replace('msg-status','msg_status', sms_Outbox::outbox($with_array));
    }

    public function kirim_sms(Request $request)
    {
        return sms_Send::send($request->to, $request->message);
    }

}
