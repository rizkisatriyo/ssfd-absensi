<?php

namespace App\Http\Controllers\Fingerprint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ssfd_ClearData;
use App\FingerprintModel;

class DeleteLog extends Controller
{
    public function index($id)
    {

        /* Mengambil data finger print */ 
        $show = FingerprintModel::find($id);
        $with_array     =   array
        (
            'init'    =>  curl_init(),
            'ip'      =>  $show->ip_address,
            'ComKey'  =>  $show->com_key,
        );
        return ssfd_ClearData::ClearData($with_array);

    }
}
