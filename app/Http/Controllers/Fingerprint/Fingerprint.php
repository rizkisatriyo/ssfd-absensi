<?php

namespace App\Http\Controllers\Fingerprint;

use     Illuminate\Http\Request;
use     App\Http\Controllers\Controller;
use     App\FingerprintModel;
use     Validator;

class Fingerprint extends Controller
{

    public function __construct()
    {
        // parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = array('title' => 'Pengaturan - Fingerprint');
        return view('default_theme.fingerprint.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('default_theme.fingerprint.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules      =
        [
            'name'                  => 'required',
            'user_id'               => 'required',
            'password'              => 'required',
            'ip_address'            => 'required|unique:fingerprint|max:15|ip',
            'com_key'               => 'required|numeric',
        ];


        $validator = Validator::make($request->all(), $rules, $this->messages());

        if($validator->fails())
        {  
            $response = array
            (
                'error'     => 1,
                'results'   => $validator->errors()->all()
            ); 
        } else
        {
            $eloquent               = new FingerprintModel();
            $eloquent->name         = $request->input('name');
            $eloquent->user_id      = $request->input('user_id');
            $eloquent->password     = $request->input('password');
            $eloquent->ip_address   = $request->input('ip_address');
            $eloquent->com_key      = $request->input('com_key');
            $eloquent->save();
            $response = array('error' => 0);
        }

        return json_encode($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$a = null)
    {

        $this->attrib['data'] = FingerprintModel::where('id', $id)->first();

        if($a == null)
        {

            return view('default_theme.fingerprint.show', $this->attrib);

        } else
        {
            return $this->attrib['data'];
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $finger = FingerprintModel::find($id);
        return view('default_theme.fingerprint.edit', ['finger' => $finger])->with(compact('finger') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        /* Mengambil data finger print */ 
        $show = $this->show($request->id,'a');
        /*
        *   Samakan ip address jika sama maka rule tidak ada validasi uniq, jika tidak sama maka rule unique diadakan
        */
        if($request->ip_address == $show->ip_address)
        {
            $rules = ['name' => 'required', 'ip_address' => 'required|max:15|ip','com_key' => 'required|numeric'];
        } else
        {
            $rules = ['name' => 'required', 'ip_address' => 'required|unique:fingerprint|max:15|ip','com_key'=> 'required|numeric'];
        }

        $validator = Validator::make($request->all(), $rules, $this->messages());

        if($validator->fails())
        {
            $response = array
            (
                'error'     => 1,
                'results'   => $validator->errors()->all()
            );
        } else
        {
            $eloquent = FingerprintModel::where('id', $id)->update(
                [
                    'name'          => $request->name,
                    'user_id'       => $request->user_id,
                    'password'      => $request->password,
                    'ip_address'    => $request->ip_address,
                    'com_key'       => $request->com_key
                ]
            );
            $response = array('error' => 0);
        }
        return json_encode($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eloquent = FingerprintModel::where('id',$id)->delete();
        if($eloquent)
        {
           $response = array
            (
                'error'     => 0,
                'results'   => ''
            );
        } else
        {
           $response = array
            (
                'error'     => 1,
                'results'   => 'Error hapus data.'
            );
        }
        return json_encode($response);
    }

    public function api()
    {
        return datatables(FingerprintModel::orderBy('id','desc')->get())->toJson();
    }

    private function messages()
    {
        $messages = [
            'name.required'         => 'Nama mesin tidak boleh kosong',
            'user_id.required'      => 'User ID fingerprint tidak boleh kosong',
            'password.required'     => 'Password fingerprint tidak boleh kosong',
            'ip_address.ip'         => 'Format ip address salah.',
            'ip_address.required'   => 'Ip address tidak boleh kosong.',
            'ip_address.unique'     => 'Ip address sudah ada.',
            'ip_address.max'        => 'Ip address maksimal 15 karakter.',
            'com_key.required'      => 'Kunci mesin tidak boleh kosong.',
            'com_key.numeric'       => 'Kunci mesin hanya angka saja.',
        ];
        return $messages;
    }
}
