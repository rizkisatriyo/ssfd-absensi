<?php

namespace App\Http\Controllers\Fingerprint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ssfd_CheckStatus;
use App\FingerprintModel;

class FingerprintStatus extends Controller
{
    public function index($id)
    {

        /* Mengambil data finger print */ 
        $show = FingerprintModel::where('id', $id)->first();
        $with_array     =   array
        (
            'init'      =>  curl_init(),
            'name'      =>  $show->name,
            'user_id'   =>  $show->user_id,
            'password'  =>  $show->password,
            'com_key'   =>  $show->com_key,
            'ip'        =>  $show->ip_address
        );
        $response   = ssfd_CheckStatus::CheckStatus($with_array);
        return view('default_theme.fingerprint.check_connection', compact('response','show'));

    }
}
