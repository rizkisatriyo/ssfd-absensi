<?php

namespace App\Http\Controllers\Fingerprint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FingerprintModel;
use App\Helpers\ssfd_BackupData;
use Symfony\Component\HttpFoundation\Response;

class BackupData extends Controller
{
    public function index($id)
    {
    	/* Mengambil data finger print */ 
    	$show = FingerprintModel::find($id);
    	$with_array     =   array
    	(
    		'init'          =>  curl_init(),        	# Inisialisasi CURL
    	    'ip'            =>  $show->ip_address,   	# Arahkan IP Address fingerprint
    	    'ComKey'        =>  $show->com_key,  		# Default adalah 0,
    	    'username'      =>  $show->user_id,     	# Username Login For Web App Fingerprint
    	    'userpwd'       =>  $show->password,		# Password Login For Web App Fingerprint
    	);

    	$page = ssfd_BackupData::BackupData($with_array);

    	if($page['code'] == 200)
    	{
	    	return response($page['data'], 200)
	    	                  ->header('Content-Disposition', 'attachment; filename="'.date('Y_m_d').'_data_user.dat"');
		} else
		{
			return $page;
		}

    }
}
