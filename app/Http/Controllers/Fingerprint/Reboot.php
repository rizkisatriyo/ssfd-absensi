<?php

namespace App\Http\Controllers\Fingerprint;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ssfd_Reboot;
use App\FingerprintModel;

class Reboot extends Controller
{
    public function index($id)
    {

        /* Mengambil data finger print */ 
        $show           = FingerprintModel::find($id);
        $with_array     =   array
        (
            'init'      =>  curl_init(),
            'ip'        =>  $show->ip_address,
            'username'  =>  $show->user_id,
            'userpwd'   =>  $show->password,
        );
        return ssfd_Reboot::Reboot($with_array);

    }
}
