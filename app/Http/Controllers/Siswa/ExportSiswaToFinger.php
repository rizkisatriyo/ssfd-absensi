<?php

namespace App\Http\Controllers\Siswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ssfd_AddUser;

class ExportSiswaToFinger extends Controller
{
    public function index(Request $request)
    {
        /*
        *   substr($request->nama_siswa,0,26)
        *   Potong nama siswa sampai dengan 26 karakter, karena untuk memuat nama siswa
        *   sampai dengan 26 karakter pada mesin, jika melebihi dari 26 karakter maka ID user
            pada fingerprint akan tercantum Huruf terakhir dari nama siswa tersebut
            Contoh  :
            - Nama ADINDA SHAIRA RIZKINASTARI SYAHRIR dengan inputan id 8 akan terinput ID nya menjadi IR8
        *
        */
        $nama_siswa     = substr($request->nama_siswa,0,26);

        $with_array     =   array
        (
        	'init'          =>  curl_init(),        	# Inisialisasi CURL
            'ip'            =>  $request->ip_address,   # Arahkan IP Address fingerprint
            'username'      =>  $request->username,   	# Username Login For Web App Fingerprint
            'userpwd'       =>  $request->userpwd,		# Password Login For Web App Fingerprint
            'ucard'         =>  0,                		# Nomor Kartu
            'uname'         =>  $nama_siswa,  		    # Nama User
            'upin'          =>  null,       			# PIN / ID User (Update Data User)
            'upin2'         =>  $request->id_siswa,     # PIN / ID User
            'uprivilege'    =>  0,                		# 0 = (User) & 14 = (Administrator)
            'upwd'          =>  '12345'             	# Password login User
        );
        return ssfd_AddUser::AddUser($with_array);
    }
}
