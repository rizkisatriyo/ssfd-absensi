<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Controller;
use App\Helpers\ssfd_DeleteUser;
use Illuminate\Http\Request;
use App\FingerprintModel;
use App\AlumniModel;
use App\Student;
use Exception;

class MutasiSiswa extends Controller
{
    public function index(Request $request)
    {

      /* Tampilkan data siswa berdasarkan kelas yang telah dipilih */
      $query    = Student::where('grade_id', $request->f_class);
      /* Query To Object */
      $student  = $query->get();
      /* Query To Count */
      $counter  = $query->count();
      /* Cek Jika Total Siswa Lebih Dari 0 */
      if($counter > 0)
      {
        foreach ($student as $data)
        {
          /* Cek Jika Siswa Akan Dipindahkan Sebagai Alumni */
          if($request->t_class == 'alumni')
          {
            /* Call Alumni Model */
            $eloquent = new AlumniModel();
            $eloquent->nis = $data->nis;
            $eloquent->name = $data->name;
            $eloquent->grade_id = $data->grade_id;
            $eloquent->kelamin = $data->kelamin;
            $eloquent->alamat = $data->alamat;
            $eloquent->tmplahir = $data->tmplahir;
            $eloquent->tgllahir = $data->tgllahir;
            $eloquent->foto = $data->foto;
            $eloquent->kota = $data->kota;
            $eloquent->tahun_masuk = $data->tahun_masuk;
            $eloquent->tahun_keluar = date('Y');
            $eloquent->nohp = $data->nohp;
            $eloquent->nmortu = $data->nmortu;
            $eloquent->nohp_ortu = $data->nohp_ortu;
            $eloquent->nowa = $data->nowa;
            $eloquent->save();
            /*
            * Call FingerprintModel untuk menghapus data siswa pada mesin finger
            */
            // $finger = FingerprintModel::All();
            // foreach($finger as $row)
            // {
            //     $with_array     =   array
            //     (
            //         'ip'            =>  $row->ip_address,
            //         'pin'           =>  $data->id,
            //         'ComKey'        =>  $row->com_key
            //     );
               /*
                * Melakukan penghapusan data siswa pada mesin finger
                */
            //     ssfd_DeleteUser::DeleteUser($with_array);
            // }
            /*
            * Melakukan penghapusan data tabel siswa
            */
            Student::destroy($data->id);
          } else
          {
            Student::where('id', $data->id)->update([
                'grade_id' => $request->t_class,
              ]
            );
          }
        } /* End Loop */
        return array('error' => 0, 'msg' => 'Mutasi siswa berhasil.');
      } else
      {
        return array('error' => 1, 'msg' => 'Tidak ada data siswa.');
      }


    }
}
