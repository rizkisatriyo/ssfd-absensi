<?php

namespace App\Http\Controllers\Siswa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Student;
use Excel;
use File;

class RenderFile extends Controller
{
    public function index(Request $request)
    {

      /* Ambil parameter dari field file */
    	$file = $request->file('file');
      /* Tentukan PATH penyimpnana file */
    	$path = './source/';
      /* Filename */
      $filename = $file->getClientOriginalName();
      /* Upload file dipindahkan ke path */
    	$file->move($path,$filename);
    	/*
    	*	Panggil file dengan Excell::load('PATH FILE YANG AKAN DI LOAD', function(parameter)
    	*	{
		*		// TODO
    	*	});
    	*/
    	Excel::load($path.$filename, function($reader)
    	{

         /* Dump Data */
    		// $reader->dump();

         /* Render Data To Object */
         $data = $reader->toObject()[0];
         /* Path file json */
         $path_file = 'temp_data_siswa.json';
         /* Save Data To File JSON */
         $save = File::put('./'.$path_file, $data);

         if($save)
         {
            $response = array('error' => 0,'msg' => 'Data berhasil diupload.','temp_file' => $path_file);
         } else
         {
            $response = array('error' => 1,'msg' => 'Data gagal diupload.','temp_file' => $path_file);
         }

         echo json_encode($response);

    	});

		/* Hapus kembali file yang telah di upload untuk mengurangi penumpukan file */
    	unlink($path.$filename);
    }

   public function import_to_db(Request $request)
   {

      /* Tentukan vaiabel kelamin */
      $kelamin = 'Perempuan';

      /* Cek jika data kelamin adalah Laki-Laki */
      if($request->kelamin == 'L')
      {
         $kelamin = 'Laki-Laki';
      }

      try
      {

        $counter = Student::all()->count();
        $pin = Student::all()->last();

        if(isset($pin->pin)) {
          $pin = $pin->pin + 1;
        } else {
          $pin = 1;
        }

        if($counter < 10000)
        {
           Student::create([
              'nis' => $request->nis,
              'name' => $request->name,
              'grade_id' => $request->grade_id,
              'pin' => $pin,
              'kelamin' => $kelamin,
              'alamat' => $request->alamat,
              'tmplahir'  => $request->tmplahir,
              'tgllahir'  => $request->tgllahir,
              'kota' => $request->kota,
              'tahun_masuk' => $request->tahun_masuk,
              'nmortu' => $request->nmortu,
              'nohp_ortu' => $request->nohp_ortu,
              'nowa'  => $request->nowa,
           ]);
          return array('error' => 0,'msg' => $request->name.' telah ditambahkan.','data_siswa' => $request->name);
        }

        return array('error' => 1, 'msg' => $request->name.' gagal ditambahkan, karena kapasitas user sudah penuh, pada mesin finger.');

      } catch(\Exception $e)
      {
        return array('error' => 1,'msg' => $request->name.' gagal ditambahkan.','data_siswa' => $request->name, 'error_msg' => $e->getMessage());
      }
   }

}
