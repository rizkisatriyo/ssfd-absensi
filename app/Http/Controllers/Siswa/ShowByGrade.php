<?php

namespace App\Http\Controllers\Siswa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;

class ShowByGrade extends Controller
{
   public function index($grade_id)
   {
   		if($grade_id == 'all')
   		{

   			return Student::all();

   		} else
   		{
   			return Student::where('grade_id', $grade_id)->get();
   		}
   }
}
