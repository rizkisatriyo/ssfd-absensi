<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginCtrl extends Controller
{
	public function checkLogin(Request $request)
	{
    	$username 	= $request->get('username');
    	$password 	= $request->get('password');
    	$check 		= Auth::attempt(['username'=>$username,'password'=>$password]);
    	if($check) {
		    return redirect(url('superadmin'));
		} else
		{
			return redirect()->back();
		}
    }
}