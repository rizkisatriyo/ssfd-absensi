<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Attedances;
use App\Student;
use App\Grade;

class Attedance extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $dataArray = array();

    public function index()
    {
        if(request()->ajax()) {
            $attendances = Attedances::with('student')->orderBy('created_at','desc');
            $row = $attendances->get();
            $results = array();
            if($attendances->count() >0)
            {
                for ($i=0; $i < $attendances->count(); $i++) {

                    if($row[$i]->hadir == 1) { $status = 'Hadir'; }
                    if($row[$i]->terlambat == 1) { $status = 'Terlambat'; }
                    if($row[$i]->sakit == 1) { $status = 'Sakit'; }
                    if($row[$i]->alfa == 1) { $status = 'Alfa'; }
                    if($row[$i]->izin == 1) { $status = 'Izin'; }

                    $results[$i]['id'] = $row[$i]->id;
                    $results[$i]['nama_siswa'] = $row[$i]->student->name;
                    $results[$i]['status'] = $status;
                    $results[$i]['jam_masuk'] = $row[$i]->jam_masuk;
                    $results[$i]['jam_keluar'] = $row[$i]->jam_keluar;
                    $results[$i]['pulang'] = $row[$i]->pulang;
                }
            }
            return datatables($results)->toJson();
        }
        return view('default_theme.absensi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $attendance = Attedances::where('id',$id)->with('student')->first();
        return view('default_theme.absensi.edit',['attendance' => $attendance])
                    ->with(compact('attendance'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $data = Attedances::find($id);

            if(Request()->status == 'terlambat') {
                $param = [
                    'hadir' => 0,
                    'sakit' => 0,
                    'terlambat' => 1,
                    'alfa' => 0,
                    'izin' => 0,
                ];
            }

            if(Request()->status == 'alfa') {
                $param = [
                    'hadir' => 0,
                    'sakit' => 0,
                    'terlambat' => 0,
                    'alfa' => 1,
                    'izin' => 0,
                ];
            }

            if(Request()->status == 'hadir') {
                $param = [
                    'hadir' => 1,
                    'sakit' => 0,
                    'terlambat' => 0,
                    'alfa' => 0,
                    'izin' => 0,
                ];
            }

            if(Request()->status == 'izin') {
                $param = [
                    'hadir' => 0,
                    'sakit' => 0,
                    'terlambat' => 0,
                    'alfa' => 0,
                    'izin' => 1,
                ];
            }

            if(Request()->status == 'sakit') {
                $param = [
                    'hadir' => 0,
                    'sakit' => 1,
                    'terlambat' => 0,
                    'alfa' => 0,
                    'izin' => 0,
                ];
            }
            DB::table('attedances')->where('id', $id)->update($param);
            return ['error' => 0];
        } catch (\Exception $e) {
            return ['error' => 1,'results' => [$e->getMessage()]];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function api()
    {
        //  pendefinisian tanggal awal
        $tgl1 = date('Y-m-d');
        //  operasi penjumlahan tanggal
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        // Query Get Data With Between
        $query = Attedances::whereBetween('created_at',[ $tgl1, $tgl2])->with('student')->orderBy('id','desc');
        // Get Data
        $attendance = $query->get();
        // Counter
        $count = $query->count();

        if($count > 0)
        {
            for($i =0; $i < $count; $i++)
            {

                $tgl = explode(" ", $attendance[$i]['created_at']);

                $grade = Grade::where('id', $attendance[$i]['student']['grade_id'])->first();

                $result[$i] = array(
                    'id_absensi'    => $attendance[$i]['id'],
                    'kode_absensi'  => $attendance[$i]['kode'],
                    'jam_masuk'     => $attendance[$i]['jam_masuk'],
                    'jam_keluar'    => $attendance[$i]['jam_keluar'],
                    'hadir'         => $attendance[$i]['hadir'],
                    'sakit'         => $attendance[$i]['sakit'],
                    'terlambat'     => $attendance[$i]['terlambat'],
                    'alfa'          => $attendance[$i]['alfa'],
                    'izin'          => $attendance[$i]['izin'],
                    'pulang'        => $attendance[$i]['pulang'],
                    'kjm'           => $attendance[$i]['kjm'],
                    'kjp'           => $attendance[$i]['kjp'],
                    'nis'           => $attendance[$i]['student']['nis'],
                    'name'          => $attendance[$i]['student']['name'],
                    'kelas'         => $grade['name'],
                    'foto'          => $attendance[$i]['student']['foto'],
                    'tanggal'       => $tgl[0]
                );
            }
        } else
        {
            $result['data'] = array(
                'id_absensi'    => null,
                'kode_absensi'  => null,
                'jam_masuk'     => null,
                'jam_keluar'    => null,
                'hadir'         => null,
                'sakit'         => null,
                'terlambat'     => null,
                'alfa'          => null,
                'izin'          => null,
                'pulang'        => null,
                'kjm'           => null,
                'kjp'           => null,
                'nis'           => null,
                'name'          => null,
                'kelas'         => null,
                'foto'          => null,
                'tanggal'       => null
            );
        }
        return datatables($result)->toJson();
    }
}
