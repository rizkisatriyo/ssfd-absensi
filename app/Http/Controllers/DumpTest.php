<?php

namespace   App\Http\Controllers;

use App\Helpers\ssfd_GetAttLog;
use App\Helpers\ssfd_AddUser2;
use App\Helpers\ssfd_UserInfo;
use App\Helpers\ssfd_ClearData;
use App\Helpers\ssfd_RestartMachines;
use App\Helpers\ssfd_GetReport;
use App\Helpers\ssfd_CheckStatus;
use App\Helpers\ssfd_FingerTemplates;
use App\Helpers\ssfd_AddFinger;
use App\Helpers\ssfd_DeleteUser;
use App\Attedances;
use App\Helpers\sms_PesanMasuk;
use App\TestModel;

use App\Events\SmsPusherEvent;

class DumpTest extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function GetAttLog()
    {

        $with_array     =   array
        (
            'ip'        => '192.168.137.11',         # Arahkan IP Address fingerprint nya yang akan ditarik datanya
            'ComKey'    =>   0,                      # Default adalah 0
            'status'    =>  'in',                   # In Atau Out
            'pin'       =>  1                   # ALL Untuk Menarik semua data informasi log / bisa menggunakan ID user, siswa dengan type Integer
        );
        try {
            return ssfd_GetAttLog::GetAttLog($with_array);   
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    // public function ClearData()
    // {

    //     $with_array     =   array
    //     (
    //         'ip'        => '192.168.1.201',             # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'    =>  0,                          # Default adalah 0
    //         'pin'       =>  '2'                         # ID User
    //     );
    //     return ssfd_ClearData::ClearData($with_array);
    // }

    // public function RestartMachines()
    // {
 
    //     $with_array     =   array
    //     (
    //         'ip'        => '192.168.1.201',             # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'    =>  0,                          # Default adalah 0
    //     );
    //     return ssfd_RestartMachines::RestartMachines($with_array);
    // }

    // public function GetReport()
    // {

    //     $with_array     =   array
    //     (
    //         'ip'        => '192.168.1.201',             # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'    =>  0,                          # Default adalah 0,
    //         'pin'       =>  1,                          # ID User
    //         'date_from' =>  '2018-01-01',
    //         'date_to'   =>  '2018-04-01',
    //     );
    //     return ssfd_GetReport::GetReport($with_array);

    // }

    // public function AddUser()
    // {

    //     $with_array     =   array
    //     (
    //         'ip'            =>  '192.168.1.201',    # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'        =>  0,                  # Default adalah 0,
    //         'init'          =>  curl_init(),        # Inisialisasi CURL
    //         'username'      =>  '1',                # Username Login For Web App Fingerprint (Administrator)
    //         'userpwd'       =>  '12345',            # Password Login For Web App Fingerprint (Administrator)
    //         'ucard'         =>  '0',                  # Nomor Kartu
    //         'uname'         =>  'Azura Dompu',  # Nama User
    //         'upin'          =>  'NULL',                  # PIN / ID User
    //         'upin2'         =>  7,                  # PIN / ID User
    //         'uprivilege'    =>  '0',                  # 0 = (User) & 14 = (Administrator)
    //         'upwd'          =>  '12345'             # Password login User

    //     );
    //     return ssfd_AddUser::AddUser($with_array);
    // }

    // public function UserInfo()
    // {
    //     $with_array     =   array
    //     (
    //         'ip'        => '192.168.1.201',         # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'    =>  0,                      # Default adalah 0,
    //         'pin'       =>  4,                      # ID User
    //     );
    //     return ssfd_UserInfo::UserInfo($with_array);
    // }

    // public function PesanMasuk()
    // {
    //     return sms_PesanMasuk::PesanMasuk();
    // }

    // public function yajra()
    // {
    //     return datatables(TestModel::orderBy('id','desc')->get())->toJson();
    // }

    // public function cron_test()
    // {
    //     $with_array     =   array
    //     (
    //         'ip'        => '192.168.137.10',         # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //         'keyCom'    =>   0,                      # Default adalah 0
    //         'status'    =>  'in',                   # In Atau Out
    //         'pin'       =>  1                       # ALL Untuk Menarik semua data informasi log / bisa menggunakan ID user, siswa dengan type Integer
    //     );
    //     $test   = ssfd_GetAttLog::GetAttLog($with_array);
    //     $exec   = new SmsPusherEvent("Hola Qudrat");
    //     return event($exec);
    // }

    // public function check()
    // {
    //     $with_array     =   array
    //     (
    //         'init'      =>  curl_init(),
    //         'ip'        => '192.168.137.10',         # Arahkan IP Address fingerprint nya yang akan ditarik datanya
    //     );
    //     return ssfd_CheckStatus::CheckStatus($with_array);
    // }

    public function AddUser()
    {
        $with_array     =   array
        (
            'ip'            =>  '192.168.137.12',    # Arahkan IP Address fingerprint nya yang akan ditarik datanya
            'keyCom'        =>  0,                  # Default adalah 0,
            'init'          =>  curl_init(),        # Inisialisasi CURL
            'username'      =>  '1',                # Username Login For Web App Fingerprint (Administrator)
            'userpwd'       =>  '12345',            # Password Login For Web App Fingerprint (Administrator)
            'ucard'         =>  '0',                  # Nomor Kartu
            'uname'         =>  'Azura Dompu',  # Nama User
            'upin'          =>  'NULL',                  # PIN / ID User
            'upin2'         =>  7,                  # PIN / ID User
            'uprivilege'    =>  '0',                  # 0 = (User) & 14 = (Administrator)
            'upwd'          =>  '12345'             # Password login User
        );
        return ssfd_AddUser2::AddUser2($with_array);
    }

    public function FingerTemplate()
    {
        $with_array     =   array
        (
            'init'          =>  curl_init(),        # Inisialisasi CURL
            'ip'            =>  '192.168.137.10',
            'pin'           =>  10000,
            'fn'            =>  6,
            'ComKey'        =>  0,                  # Default adalah 0,
        );
        return ssfd_FingerTemplates::FingerTemplates($with_array);
    }

    public function AddFinger()
    {
        $with_array     =   array
        (
            'init'          =>  curl_init(),        # Inisialisasi CURL
            'ip'            =>  '192.168.137.10',
            'pin'           =>  10000,
            'fn'            =>  5,
            'ComKey'        =>  0,                  # Default adalah 0,
        );
        return ssfd_AddFinger::AddFinger($with_array);
    }

    public function DeleteUser()
    {
        $with_array     =   array
        (
            'ip'            =>  '192.168.137.10',
            'pin'           =>  3,
            'ComKey'        =>  0
        );
        return ssfd_DeleteUser::DeleteUser($with_array);
    }

    public function between_query_test()
    {
        $tgl1 = date('Y-m-d');// pendefinisian tanggal awal
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1))); //operasi penjumlahan tanggal sebanyak 6 hari
        return Attedances::whereBetween('created_at',[ $tgl1, $tgl2])->with('student')->get();
    }
}
