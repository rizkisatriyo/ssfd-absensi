<?php

namespace App\Http\Requests;

class UpdateStudentRequest extends StoreStudentRequest
{
    public function rules()
    {
        $rules = parent::rules(); 
        $rules['nis'] = 'required|numeric|unique:students,nis,'. $this->route('student');
        return $rules;
    }
}
