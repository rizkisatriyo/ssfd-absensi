<?php

namespace App\Http\Requests;

class UpdateGradeRequest extends StoreGradeRequest
{
    public function rules()
    {
        $rules = parent::rules();
        $rules['name'] = 'required|unique:grades,name,' . $this->route('grade');
        return $rules;
    }
}
