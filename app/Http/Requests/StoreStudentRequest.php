<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nis' => 'required|numeric|unique:students,nis',
            'name' => 'required',
            'tmplahir' => 'required',
            'tgllahir' => 'required',
            'kelamin' => 'required',
            'grade_id' => 'required|exists:grades,id',
            'alamat' => 'required',
            'kota' => 'required',
            'tahun_masuk' => 'required',
            'nohp_ortu' => 'required',
            'foto' => 'required'
        ];
    }
}
